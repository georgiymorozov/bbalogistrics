(function($) {
    $.fn.addTouch = function() {
        return this.each(function(i, el) {
            $(el).bind('touchstart touchmove touchend touchcancel',function() {
                var touches = event['changedTouches'],
                    first = touches[0],
                    type = '';
                switch(event.type) {
                    case 'touchstart':
                        type = 'mousedown';
                        break;
                    case 'touchmove':
                        type = 'mousemove';
                        break;
                    case 'touchend':
                        type = 'mouseup';
                        break;
                    case 'touchcancel':
                        type = 'mouseup';
                        break;
                    default:
                        return;
                }
                var simulatedEvent = document.createEvent('MouseEvent');
                simulatedEvent.initMouseEvent(type, true, true, window, 1, first.screenX, first.screenY, first.clientX, first.clientY, false, false, false, false, 0/*left*/, null);
                first.target.dispatchEvent(simulatedEvent);
            });
        });
    };

    $.fn.iniScrollBlock = function(options) {

        var params = options || {};

        return $(this).each(function() {
            // Базовый блок для скролла ---
            var _this = $(this);

            if (_this.data('scrollObject')) {
                _this.data('scrollObject').update();
                if (options.scrollTo) {
                    _this.data('scrollObject').scrollTo(options.scrollTo);
                }
                if (options.toBegin) {
                    _this.data('scrollObject').toBegin();
                }
                return;
            } else {
                if (options.toBegin) return;
            }

            /* Базовые элементы блока со кроллом  */
            var scrollObject = new (function(options){
                var scrollerLine =
                    $("<div>").addClass("fn-scroller-line");
                var scrollerLineContainer =
                    $("<div>").addClass("scroller-line-container").appendTo(scrollerLine);
                var scrollerRanger =
                    $("<div>").addClass("scroller-ranger").appendTo(scrollerLineContainer);

                var wheeled = false;

                options = $.extend({
                    maxHeight: 200,
                    scrolledContent: '[data-scrolled]'
                }, options);

                // Контент базового блока ---
                var scrolled = $(options['scrolledContent']);

                // Relative блок - вставляется в базовый блок

                var relativeBlock = $('<div>').css({
                    position: 'relative',
                    maxHeight: options['maxHeight'],
                    overflow: 'hidden'
                });

                var fullHeight, percentsHeight, isTouched;

                this.scrollTo = function(pos) {
                    wheeled = Math.min(Math.max(0, wheeled + pos / fullHeight * 100), 100 - percentsHeight);
                    scrollerRanger.css("margin-top", scrollerLine.height() / 100 * wheeled);
                    scrolled.css("margin-top", - fullHeight / 100 * wheeled );
                };
                this.toBegin = function() {
                    wheeled = 0;
                    scrollerRanger.css("margin-top", 0);
                    scrolled.css("margin-top", 0);
                };
                var iniScrollElements = function() {
                    _this.bind("mousewheel DOMMouseScroll", function(e) {
                        e.preventDefault();
                        var event = e.originalEvent;
                        var deltaY = event.deltaY || (event.wheelDelta ? -event.wheelDelta : (event.detail * 3));
                        var percentWheel = deltaY / fullHeight * 100;
                        wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);

                        scrollerRanger.css("margin-top", scrollerLine.height() / 100 * wheeled);

                        scrolled.css("margin-top", - fullHeight / 100 * wheeled );
                        return false;
                    });


                    var startClientY = 0;
                    var downRanger = function(e) {
                        e.preventDefault();
                        startClientY = e.clientY;
                        $(window).bind({
                            mousemove: moveRanger,
                            mouseup: endMove
                        });
                        return false;
                    };

                    var moveRanger = function(e, isTouch) {
                        isTouch = isTouch || false;
                        var rangeMove = e.clientY - startClientY;
                        if (isTouch) {
                            rangeMove = -rangeMove;
                        }
                        startClientY = e.clientY;
                        var percentWheel = rangeMove / options['maxHeight'] * 100;
                        wheeled = Math.min(Math.max(0, wheeled + percentWheel), 100 - percentsHeight);

                        scrollerRanger.css("margin-top", scrollerLine.height() / 100 * wheeled);
                        scrolled.css("margin-top", - fullHeight / 100 * wheeled);
                    };
                    var endMove = function() {
                        $(window).unbind({
                            mousemove: moveRanger,
                            mouseup: endMove
                        });
                    };

                    scrollerRanger.bind({
                        "mousedown": downRanger
                    }).addTouch();

                    var touchMove = function(e) {
                        e.preventDefault();
                        moveRanger(e, true);
                    };
                    var cancelMove = function() {
                        $(window).unbind({
                            mousemove: touchMove,
                            mouseup: cancelMove
                        });
                    };
                };

                var iniHeightScroll = function() {
                    if (scrolled && scrolled.outerHeight() > options['maxHeight']) {
                        relativeBlock.append(scrollerLine, scrolled).appendTo(_this);
                        scrolled.addClass('scrolled-block')
                    } else {
                        scrolled.removeClass('scrolled-block');
                    }
                    fullHeight = scrolled.outerHeight();
                    percentsHeight = Math.min(options['maxHeight'] / fullHeight * 100, 100);
                    scrollerRanger.height(percentsHeight + '%');
                };

                iniScrollElements();
                iniHeightScroll();

                this.update = function() {
                    scrollerLine.detach();
                    iniHeightScroll();
                };
            })(params);

            _this.data('scrollObject', scrollObject);
        });
    };
})(jQuery);