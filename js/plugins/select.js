(function($) {
    var selectNum = 0;
    $.fn.select = function(action, opts) {

        return $(this).each(function() {
            var _this = $(this);

            if (_this.data('selectObject') || false) {
                switch (action) {
                    case 'refresh':
                        _this.data('selectObject').Refresh();
                        return;
                        break;
                    case 'showList':
                        _this.data('selectObject').showList();
                        return;
                        break;
                    case 'hideList':
                        _this.data('selectObject').hideList();
                        return;
                        break;
                    case 'apply':
                        _this.data('selectObject').apply();
                        return;
                        break;
                    default:
                        _this.data('selectObject').Refresh();
                        return;
                }
            }

            opts = opts || {};
            opts.maxSize = 5;
            action = action || false;
            var page = $('body');

            new (function() {
                _this.data('selectObject', this);
                var escapeRegExp = function(str) {
                    return str.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
                };
                this.Refresh = function() {
                    optsContainer.html('');
                    hiddenElements.html('');
                    options = $('option', _this);

                    options.each(function() {
                        var option = $(this);
                        var textItem = option.text(),
                            oneOptionBlock = $('<li>');

                        var isDisabled = (option.val() == 'undefined') || (option.val() == 'undefined:undefined');

                        oneOptionBlock.addClass('select-option').html(
                                $('<span>').text(textItem)
                            ).mousedown(function(e) {
                                e.preventDefault();
                                hideList();
                                _this.val(option.val()).change();
                                if (!_this.is(':focus')) {
                                    _this.focus();
                                }
                                return false;
                            });

                        option.data('option_decor', oneOptionBlock);

                        var replacer = _this.attr('data-replace-selected') || false;
                        var text = option.text();
                        if (replacer) {
                            text = text.replace(new RegExp('^' + escapeRegExp(replacer), 'i'), replacer);
                        }
                        option.data('replacedText', text);

                        isDisabled ? oneOptionBlock.addClass('disabled-value') : false;
                        option.data('disabled', isDisabled);

                        if (option.is(':selected')) {
                            oneOptionBlock.addClass('selected');

                            oldSelectedOption = option;

                            isDisabled ?
                                selectButtonArea.addClass('disabled-value') :
                                selectButtonArea.removeClass('disabled-value');

                            selectButtonArea.text(option.data('replacedText'));
                            oldValue = _this.val();
                        }

                        hiddenElements.append($('<div>').addClass('no-visibility').text(textItem));
                        optsContainer.append(oneOptionBlock);
                    });
                    options.length > 1 ? selectContainer.addClass('more-variants') : selectContainer.removeClass('more-variants');
                };
                selectNum++;
                var oldSelectedOption = false,
                    isVisibleList = false,
                    noShow = false;

                var scrollToSelected = function() {
                    var decor = oldSelectedOption.data('option_decor');
                    var position = decor.offset()['top'],
                        minPosition = listContainer.offset()['top'],
                        maxPosition = minPosition + listContainer.outerHeight() - decor.outerHeight(true);

                    var offset =
                        position < minPosition ? position - minPosition - 1 : position > maxPosition - 1 ? position - maxPosition + 1 : 0;

                    listContainer.iniScrollBlock ? listContainer.iniScrollBlock({scrollTo: offset}) : false;
                };

                var applyChange = this.apply = function() {
                    if (!options.length) return;
                    oldSelectedOption && oldSelectedOption.data('option_decor') ?
                        oldSelectedOption.data('option_decor').removeClass('selected') : false;


                    var selectedOption = options.filter(':selected');

                    selectedOption.data('disabled') ?
                        selectButtonArea.addClass('disabled-value') :
                        selectButtonArea.removeClass('disabled-value');

                    selectedOption.data('option_decor') ?
                        selectedOption.data('option_decor').addClass('selected') :
                        false;

                    oldSelectedOption = selectedOption;
                    selectButtonArea.text(selectedOption.data('replacedText'));
                    selectButtonArea[selectedOption.data('disabled') ? 'addClass' : 'removeClass']('disabled-value');
                    oldValue = _this.val();

                    if (isVisibleList) {
                        scrollToSelected();
                    }
                };

                _this.focus(function() {
                    selectContainer.addClass('focus');
                }).blur(function() {
                    selectContainer.removeClass('focus');
                    hideList();
                }).on('change', applyChange).on('keyup', function() {
                    if (oldValue != _this.val()) {
                        _this.change();
                    }
                }).keydown(function(e) {
                    var key = e.which;
                    if (key == 13 || key == 32 || key == 27) {
                        e.preventDefault();
                        isVisibleList || key == 27 ? hideList() : showList();
                    }
                });

                var oldValue = _this.val(),
                    parentLabel = _this.parents('label:first'),
                    in_label = parentLabel.length,
                    addedClass = opts['class'] || 'select-1',
                    selectContainer = $(in_label ? '<div>' : '<label>').
                        addClass('select ' + addedClass + ' ' + (_this.attr('class') || '')),
                    selectButtonArea = $('<span>').
                        addClass('select-active-area').
                        appendTo(selectContainer).
                        bind({
                            'click': function() {
                                !isVisibleList && !noShow ? showList() : hideList();
                                noShow = false;
                            },
                            'mousedown': function() {
                                noShow = isVisibleList;
                            }
                        }),
                    optsContainer = $('<ul>').addClass('select-list'),
                    listContainer = $('<div>').
                        addClass('select-list-container ' + addedClass + '-container').
                        append(optsContainer).
                        mousedown(function(e) {
                            e.preventDefault();
                        }),
                    hiddenElements = $('<span>').addClass('hidden-items');

                selectContainer.append(hiddenElements);
                var selectObject = this;

                var showList = function() {
                    if (isVisibleList) return;
                    selectObject.Refresh();
                    listContainer.appendTo(page).css({'left': 0, top: 0});

                    //console.log(options.first().data('option_decor').outerHeight(true), opions['maxSize']);
                    if (listContainer.iniScrollBlock && opts['maxSize']) {
                        listContainer.iniScrollBlock({
                            maxHeight: options.first().data('option_decor').outerHeight(true) * opts['maxSize'],
                            scrolledContent: optsContainer
                        });
                    }

                    var pageOffset = page.offset();
                    var pageHeight = page.height();
                    var containerOffset = selectContainer.offset();
                    var containerHeight = selectContainer.outerHeight();
                    var selectWidth = selectContainer.outerWidth();
                    var listContainerHeight = listContainer.outerHeight();

                    listContainer.iniScrollBlock ?
                        listContainer.iniScrollBlock({
                            toBegin: true
                        }) : false;

                    listContainer.width(selectWidth);
                    var containerOffsetTop = containerOffset['top'];
                    (containerOffsetTop + containerHeight + listContainerHeight) < ($(window).height() + page.scrollTop()) ?
                        listContainer.css({
                            'top': containerOffsetTop + containerHeight
                        }).addClass('to-bottom-list') :
                        listContainer.css({
                            'top': containerOffsetTop - listContainerHeight
                        }).addClass('to-top-list');

                    listContainer.css({
                        left: containerOffset['left'] - pageOffset['left']
                    });
                    selectContainer.addClass('active');
                    isVisibleList = true;
                    scrollToSelected();
                };
                this.showList = showList;

                var hideList = function() {
                    if (isVisibleList) {
                        listContainer.detach().removeClass('to-top-list').removeClass('to-bottom-list');
                        selectContainer.removeClass('active');
                        isVisibleList = false;
                    }
                };
                this.hideList = hideList;

                if (in_label) {
                    selectContainer.addClass('label');
                } else {
                    parentLabel = selectContainer;
                }
                var idSelect = _this.attr('id') || 'select-' + (new Date()).getTime() + selectNum;
                parentLabel.attr('for', idSelect);
                _this.after(selectContainer);
                $('<span>').
                    addClass('select-original-container').
                    append(
                    _this.attr('id', idSelect)
                ).appendTo(selectContainer);
                var options;
                this.Refresh();
            })();

        });
    };
})(jQuery);
