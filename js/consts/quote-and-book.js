angular.module('Const').constant('shipmentSelectsConsts', {
    'shipmentStatuses': {
        'new': 'New',
        'unprocessed': 'Unprocessed',
        'inProgress': 'In progress',
        'pending': 'Pending',
        'processed': 'Processed'
    },
    'packagesTypes': [
        'Carton',
        'Skid',
        'Cage',
        'Pallet',
        'Unit Load Device (ULD)',
        'Container',
        'Other'
    ],
    'ReasonForExport': [
        {
            text: 'Purchase',
            value: 'Purchase'
        }, {
            text: 'Gift',
            value: 'Gift'
        },{
            text: 'Sale',
            value: 'Sale'
        },{
            text: 'Sample',
            value: 'Sample'
        },{
            text: 'Return',
            value: 'Return'
        },{
            text: 'Repair',
            value: 'Repair'
        },{
            text: 'Personal Effects',
            value: 'PersonalEffects'
        }
    ],
    'TaxStatus': ['Private', 'Company'],
    'UNITS': {
        weight: [
            {
                value: 'grams',
                full: 'Grams',
                short: 'g'
            },
            {
                value: 'kilograms',
                full: 'Kilograms',
                short: 'kg'
            },
            {
                value: 'ounces',
                full: 'Ounces',
                short: 'oz'
            },
            {
                value: 'pounds',
                full: 'Pounds',
                short: 'lbs'
            }
        ],
        measure: [
            {
                value: 'centimeters',
                full: 'Centimeters',
                short: 'cm'
            },
            {
                value: 'meters',
                full: 'Meters',
                short: 'm'
            },
            {
                value: 'inches',
                full: 'Inches',
                short: 'in'
            },
            {
                value: 'feet',
                full: 'Feet',
                short: 'ft'
            }
        ],
        units: {
            grams: {
                name: 'Grams'
            },
            kilograms: {
                name: 'Kilograms'
            },
            ounces: {
                name: 'Ounces'
            },
            pounds: {
                name: 'Pounds'
            },
            centimeters: {
                name: 'Centimeters'
            },
            meters: {
                name: 'Meters'
            },
            inches: {
                name: 'Inches'
            },
            feet: {
                name: 'Feet'
            }
        },
        combined: [
            {
                values: {
                    weightUnit: 'grams',
                    measureUnit: 'centimeters'
                },
                full: 'Centimeters/Grams',
                short: 'cm/g'
            },
            {
                values: {
                    weightUnit: 'kilograms',
                    measureUnit: 'meters'
                },
                full: 'Meters/Kilograms',
                short: 'm/kg'
            },
            {
                values: {
                    weightUnit: 'pounds',
                    measureUnit: 'inches'
                },
                full: 'Inches/Pounds',
                short: 'in/lbs'
            }
        ]
    }
});
