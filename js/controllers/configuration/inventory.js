'use strict';

var module = angular.module('app');

module.controller('ConfigurationInventoryController', function ($scope, InventoryService, $location, Windows, InventoryTable) {

    $scope.enableInQuote = true;
    var inventoryTable = new InventoryTable();

    $scope.updateTable = function () {
        inventoryTable.setOption('params', {
                enableInQuote: $scope.enableInQuote
            }
        );
        inventoryTable.updateTableData();
    };
    $scope.inventoryTableData = inventoryTable.getTableOptions();

    $scope.exportCSV = function() {
        var columns = ['sku', 'description'];
        InventoryService.exportCSV('inventory.csv', columns, function () {

        }, function() {

        });
    };

});