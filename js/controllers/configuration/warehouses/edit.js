'use strict';

var module = angular.module('app');

module.controller('ConfigurationWarehousesEditController', function ($scope, $route, WarehousesService, $location) {
    $scope.tab = 'general';
    $scope.routeParams = $route.current.params;
    $scope.warehouseId = $scope.routeParams.id;

    $scope.locationParams = {
        onChangeModelCallback: function (model) {
            $scope.warehouseLocation = model;
        }
    };
    $scope.toTab = function(a) {
        $scope.tab = a;
    };
    $scope.toBack = function() {
        $location.path('/configuration/warehouses');
    };
    var toEdit = function (id) {
        $location.path('/configuration/warehouses/edit/' + id);
    };

    $scope.warehouse = {
        unattendedDeliveryEnabled: false,
        poBoxEnabled: false,
        dangerousGoodsEnabled: false,
        ssccEnabled: false
    };

    $scope.editFormErrors = false;
    $scope.addressFormProgress = false;
    $scope.warehouseLocation = false;

    $scope.saveWarehouse = function(generalForm, addressForm, extrasForm) {
        $scope.GeneralFormErrors = false;
        $scope.AddressFormErrors = false;
        $scope.ExtrasFormErrors = false;

        if (generalForm.$valid && addressForm.$valid && extrasForm.$valid) {
            $scope.addressFormProgress = true;
            $scope.warehouse.locationId = $scope.warehouseLocation.id;
            WarehousesService.addToList($scope.warehouse, function (data) {
                $scope.addressFormProgress = false;
                toEdit(data.id);
                if($scope.tab == "general"){
                    $scope.GeneralFormErrors = {
                        'code': 'Success',
                        'message': 'Data successfully updated',
                        'type': 'success'
                    };
                }else{
                    $scope.ExtrasFormErrors = {
                        'code': 'Success',
                        'message': 'Data successfully updated',
                        'type': 'success'
                    };
                }
            }, function(data) {
                // $scope.generalForm = data;
                $scope.addressFormProgress = false;
            });
        } else {
            if (!generalForm.$valid || !addressForm.$valid) {
                $scope.tab = 'general';
                if (!generalForm.$valid) {
                    generalForm.$setSubmitted();
                    $scope.GeneralFormErrors = {
                        'code': $scope['languageLibrary']['error_title_uncorrected'],
                        'message': 'Please ﬁx the below and try again'
                    };
                    return;
                }
                if (!addressForm.$valid) {
                    addressForm.$setSubmitted();
                    $scope.AddressFormErrors = {
                        'code': $scope['languageLibrary']['error_title_uncorrected'],
                        'message': 'Please ﬁx the below and try again'
                    };
                }
            } else {
                $scope.tab = 'extras';
                extrasForm.$setSubmitted();
                $scope.ExtrasFormErrors = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
        }
    };

    $scope.getWarehouse = function() {
        if ($scope.routeParams.id) {
            WarehousesService.getDetails($scope.routeParams.id, function (data) {
                $scope.warehouseLocation = data['location'];
                for (var i in data) {
                    if (i != 'location') {
                        $scope.warehouse[i] = data[i];
                    }
                }
            }, function(err) {
                console.log(err);
            });
        }
    };
});
