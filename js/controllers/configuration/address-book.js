'use strict';

var module = angular.module('app');

module.controller('ConfigurationAddressBookController', function ($scope, AddressBookService, AddressBookTable) {
    var addressBookTable = new AddressBookTable();
    $scope.addressBookTableData = addressBookTable.getTableOptions();

    $scope.exportCSV = function() {
        var columns = ['company', 'firstName', 'lastName'];
        AddressBookService.exportCSV('address-book.csv', columns, function () {
        }, function () {
        });
    };
});