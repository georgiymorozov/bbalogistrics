'use strict';

var module = angular.module('app');

module.controller('ConfigurationPackagingEditController', function ($scope, PackagesService, $route, $location, shipmentSelectsConsts) {
    $scope.params = $route.current.params || {};
    $scope.packageId = $scope.params.id || false;
    $scope.errorsForm = false;
    $scope.units = shipmentSelectsConsts.UNITS;
    $scope.package = {
        measureUnit: $scope.units.measure[0]['value'],
        weightUnit: $scope.units.weight[0]['value'],
        ssccEnabled: true
    };

    $scope.getPackage = function() {
        if (!$scope.packageId) return;
        $scope.addressFormProgress = true;
        PackagesService.getPackage($scope.packageId, function (data) {
            $scope.addressFormProgress = false;
            $scope.package = data;
        });
    };

    $scope.Cancel = function() {
        $location.path('/configuration/packaging/');
    };
    var toEdit = function (id) {
        $location.path('/configuration/packaging/edit/' + id);
    };

    $scope.savePackage = function(packageForm) {
        if (!packageForm.$valid) {
            packageForm.$setSubmitted();
            $scope.errorsForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
            return;
        }
        $scope.addressFormProgress = true;
        PackagesService.savePackage($scope.package, function (data) {
            $scope.addressFormProgress = false;
            toEdit(data.id);
            $scope.errorsForm = {
                'code': 'Success',
                'message': 'Data successfully updated',
                'type': 'success'
            };
        }, function(err) {
            $scope.errorsForm = err;
        });
    };

});