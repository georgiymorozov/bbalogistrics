'use strict';

var module = angular.module('app');

module.controller('TrackingManagerAssignDeviceController', function($scope) {

    var selectOptions = [
        'Look up', 'Mobile Reader', 'Mobile Reader 2', 'Mobile Reader 3', 'Mobile Reader 4'
    ];

    $scope.tableData = {
        'head': [
            {
                name: 'firstName',
                title: 'First Name',
                type: 'field',
                template: 'input.html',
                params: {
                    'class': 'first-of-child'
                }
            },
            {
                name: 'lastName',
                title: 'Last Name',
                type: 'field',
                template: 'input.html'
            },
            {
                name: 'devices',
                title: 'Assigned Devices',
                type: 'field',
                template: 'input.html',
                params: {
                    'class': 'last-of-child'
                }
            },
            {
                title: 'Actions',
                type: 'action'
            },
            {
                title: 'Assign user to device',
                name: 'userDevice',
                template: 'select.html',
                type: 'select'
            }
        ],
        'checked': true,
        'filters': true,
        'data': [],
        'tableClass': ''
    };
    $scope.newDevices = {
        'head': [
            {
                name: 'deviceType',
                title: 'Device Type',
                type: 'field',
                template: 'input.html',
                params: {
                    'class': 'first-of-child last-of-child'
                }
            },
            {
                title: 'Actions',
                type: 'action'
            },
            {
                title: 'Existing Devices',
                name: 'userDevice',
                template: 'select.html',
                type: 'select'
            }
        ],
        'checked': true,
        'filters': true,
        nopaging: true,
        'data': [],
        'tableClass': ''
    };
    $scope.newDevices.data = [
        {
            deviceType: 'ABC Brand 101',
            userDevice: {
                options: selectOptions,
                value: selectOptions[0]
            },
            actions: [
                {
                    type: 'btn-blue',
                    text: 'Edit',
                    icon: 'edit_icon'
                }, {
                    type: 'btn-red',
                    text: 'Delete'
                }
            ]
        }
    ];
    $scope.tableData.data = [
        {
            firstName: 'John',
            lastName: 'Young',
            devices: 'C101 Mobile Reader - 112233',
            userDevice: {
                options: selectOptions,
                value: selectOptions[0]
            },
            actions: [
                {
                    type: 'btn-blue',
                    text: 'Edit',
                    icon: 'edit_icon'
                }, {
                    type: 'btn-red',
                    text: 'Delete'
                }
            ]
        },
        {
            firstName: 'John',
            lastName: 'Young',
            devices: 'C101 Mobile Reader - 112233',
            userDevice: {
                options: selectOptions,
                value: selectOptions[0]
            },
            actions: [
                {
                    type: 'btn-blue',
                    text: 'Edit',
                    icon: 'edit_icon'
                }, {
                    type: 'btn-red',
                    text: 'Delete'
                }
            ],
            params: {
                readonly: true
            }
        },
        {
            firstName: 'John',
            lastName: 'Young',
            devices: 'C101 Mobile Reader - 112233',
            userDevice: {
                options: selectOptions,
                value: selectOptions[0]
            },
            actions: [
                {
                    type: 'btn-blue',
                    text: 'Edit',
                    icon: 'edit_icon'
                }, {
                    type: 'btn-red',
                    text: 'Delete'
                }
            ],
            params: {
                readonly: true
            }
        },
        {
            firstName: 'John',
            lastName: 'Young',
            devices: 'C101 Mobile Reader - 112233',
            userDevice: {
                options: selectOptions,
                value: selectOptions[0]
            },
            actions: [
                {
                    type: 'btn-blue',
                    text: 'Edit',
                    icon: 'edit_icon'
                }, {
                    type: 'btn-red',
                    text: 'Delete'
                }
            ],
            params: {
                readonly: true
            }
        }
    ];
});