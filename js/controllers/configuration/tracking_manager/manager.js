'use strict';

var module = angular.module('app');

module.controller('TrackingManagerController', function($scope) {
    $scope.shipmentsTable = {
        'head': [
            {
                name: 'sku',
                title: 'SKU' + "\u00A0" + '/' + "\u00A0" + 'Part' + "\u00A0" + '#' + "\u00A0" + '(GTIN)',
                template: 'link.html'
            },{
                name: 'description',
                title: 'Description'
            },{
                name: 'length',
                title: 'Length'
            },{
                name: 'width',
                title: 'Width'
            },
            {
                name: 'height',
                title: 'Height'
            },{
                name: 'weight',
                title: 'Weight'
            },{
                name: 'category',
                title: 'Category'
            },{
                title: 'MFG' + "\u00A0" + 'Origin',
                name: 'origin'
            }, {
                name: 'itemType',
                title: 'Item' + "\u00A0" + 'Type'
            }, {
                title: 'Item' + "\u00A0" + 'Value',
                name: 'value'
            }
        ],
        'filters': true,
        'data': []
    };
    $scope.portalTable = {
        'head': [
            {
                name: 'serial',
                title: 'Serial' + "\u00A0" + 'No.' + "\u00A0" + '(sGTIN)'
            },{
                name: 'sscc',
                title: 'Associated SSCC' + "\u00A0" + 'No.'
            },{
                name: 'scanEvent',
                title: 'Scan Event'
            },{
                name: 'location',
                title: 'Location'
            },
            {
                name: 'user',
                title: 'User' + "\u00A0" + 'ID' + "\u00A0" + '/' + "\u00A0" + 'Time'
            }, {
                title: 'Actions',
                type: 'action'
            }
        ],
        'filters': true,
        'data': []
    };


    /* Prototype tables data */
    $scope.shipmentsTable.data = [
        {
            sku: '112233445',
            description: 'Red Dress XL',
            length: 10,
            width: 10,
            height: 10,
            weight: 1,
            category: 'Business',
            origin: 'China',
            itemType: 'Garment',
            value: '$50'
        }
    ];
    $scope.portalTable.data = [
        {
            serial: '12345678',
            sscc: '111222',
            scanEvent: 'Break Bulk Depot 1',
            location: 'Loading Bay 1',
            user: 'John Smith / 15:45 17/11/15',
            actions: [
                {
                    type: 'btn-orange',
                    icon: 'tracking_icon',
                    action: function(data) {

                    },
                    text: 'Tracking Manager'
                }
            ]
        },
        {
            serial: '12345678',
            sscc: '111222',
            scanEvent: 'Break Bulk Depot 1',
            location: 'Loading Bay 1',
            user: 'John Smith / 15:45 17/11/15',
            actions: [
                {
                    type: 'btn-orange',
                    icon: 'tracking_icon',
                    action: function(data) {

                    },
                    text: 'Tracking Manager'
                }
            ]
        },
        {
            serial: '12345678',
            sscc: '111222',
            scanEvent: 'Break Bulk Depot 1',
            location: 'Loading Bay 1',
            user: 'John Smith / 15:45 17/11/15',
            actions: [
                {
                    type: 'btn-orange',
                    icon: 'tracking_icon',
                    action: function(data) {

                    },
                    text: 'Tracking Manager'
                }
            ]
        },
        {
            serial: '12345678',
            sscc: '111222',
            scanEvent: 'Break Bulk Depot 1',
            location: 'Loading Bay 1',
            user: 'John Smith / 15:45 17/11/15',
            actions: [
                {
                    type: 'btn-orange',
                    icon: 'tracking_icon',
                    action: function(data) {

                    },
                    text: 'Tracking Manager'
                }
            ]
        },
        {
            serial: '12345678',
            sscc: '111222',
            scanEvent: 'Break Bulk Depot 1',
            location: 'Loading Bay 1',
            user: 'John Smith / 15:45 17/11/15',
            actions: [
                {
                    type: 'btn-orange',
                    icon: 'tracking_icon',
                    action: function(data) {

                    },
                    text: 'Tracking Manager'
                }
            ]
        }
    ];
});