'use strict';

var module = angular.module('app');

module.controller('ConfigurationInventoryImportController', function ($scope, InventoryService, $location) {

    $scope.file = false;
    $scope.fileError = false;
    $scope.formError = false;
    $scope.onChangeFile = function(data) {
        $scope.fileError = data.error || false;
        $scope.file = data.file;

    };
    $scope.completeImport = function() {
        if ($scope.fileError) return;
        InventoryService.importInventories({
            name: 'file',
            content: $scope.file
        }, function() {
            $location.path('/configuration/inventory/');
        }, function(errs) {
            $scope.formError =  errs;
        });
    };
});