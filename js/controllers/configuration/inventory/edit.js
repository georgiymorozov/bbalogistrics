'use strict';

var module = angular.module('app');

module.controller('ConfigurationInventoryEditController', function ($scope, LocationService, InventoryService, $route, $location) {
    var params = $route.current.params || {};
    $scope.inventorySKU = params.sku || false;
    $scope.inventory = {};
    $scope.inventoryFormProgress = false;

    $scope.getInventory = function() {
        if (!params.sku) return;
        $scope.inventoryFormProgress = true;
        InventoryService.getInventory(params.sku, function (inventory) {
            $scope.inventory = inventory;
            $scope.inventoryFormProgress = false;
            LocationService.Country({
                    code: inventory.origin
                },
                function(response) {
                    $scope.manufacturer = response[0]
                }, function(err) {

                }
            );
        });
    };
    $scope.toBack = function() {
        $location.path('/configuration/inventory');
    };
    var toEdit = function (sku) {
        $location.path('/configuration/inventory/edit/' + sku);
    };
    $scope.manufacturer = false;
    $scope.saveInventory = function (inventoryForm) {
        $scope.errorsForm = false;
        if (inventoryForm.$valid) {
            $scope.inventoryFormProgress = true;
            $scope.inventory.manufacturer =
                $scope.inventory.origin =
                    $scope.manufacturer.code;

            $scope.inventory.manufacturer = $scope.manufacturer.code;

            InventoryService.addToInventory($scope.inventory, function (data) {
                $scope.inventoryFormProgress = false;
                toEdit(data.sku);
                $scope.errorsForm = {
                    'code': 'Success',
                    'message': 'Data successfully updated',
                    'type': 'success'
                };
            }, function(err) {
                $scope.errorsForm = err;
                $scope.inventoryFormProgress = false;
            }, params.sku);
        } else {
            inventoryForm.$setSubmitted();
            $scope.errorsForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
        }
    };

});