'use strict';

var module = angular.module('app');

module.controller('ConfigurationProfileManagerCreatorController', function($scope, shipmentSelectsConsts) {
    $scope.packagesTypes = shipmentSelectsConsts.packagesTypes;
    $scope.packageType = $scope.packagesTypes[0];

    $scope.firstMileAddress = {
        location: false
    };
    $scope.localeSelection = 'Domestic';
    $scope.firstMileLocationOptions = {};

    $scope.linehaul = {};
    $scope.linehaulLocationOptions = {};

    $scope.lastMile = {};
    $scope.lastMileLocationOptions = {};
    
    
    $scope.showMileTab = true;
    $scope.showLinehaulTab = true;
    $scope.showLastMileTab = true;
    $scope.showSummaryTab = true;

});