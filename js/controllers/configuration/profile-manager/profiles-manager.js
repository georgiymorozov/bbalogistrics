'use strict';

var module = angular.module('app');

module.controller('ConfigurationProfileManagerController', function ($scope, $location, Windows, BBAProfilesTable) {
    var profilesTable = new BBAProfilesTable();
    $scope.tableData = profilesTable.getTableOptions();
});