'use strict';

var module = angular.module('app');

module.controller('ConfigurationWarehousesController', function($scope, WarehousesTable) {
    var warehousesTable = new WarehousesTable();
    $scope.warehousesTableData = warehousesTable.getTableOptions();

});