'use strict';

var module = angular.module('app');

module.controller('ConfigurationPackagingController', function ($scope, PackagesListTable) {
    var packagesList = new PackagesListTable();
    $scope.packagesListData = packagesList.getTableOptions();
});