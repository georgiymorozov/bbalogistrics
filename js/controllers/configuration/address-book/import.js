'use strict';

var module = angular.module('app');

module.controller('ConfigurationAddressBookImportController', function ($scope, AddressBookService, $location) {

    $scope.file = false;
    $scope.fileError = false;
    $scope.formError = false;
    $scope.onChangeFile = function(data) {
        $scope.fileError = data.error || false;
        $scope.file = data.file;

    };
    $scope.completeImport = function() {
        if ($scope.fileError) return;
        AddressBookService.importCSV({
            name: 'file',
            content: $scope.file
        }, function() {
            $location.path('/configuration/address-book/');
        }, function(errs) {
            $scope.formError =  errs;
        });
    };
});