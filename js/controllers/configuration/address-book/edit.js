'use strict';

var module = angular.module('app');

module.controller('ConfigurationAddressBookEditController', function ($scope, AddressBookService, $route, $location) {

    $scope.params = $route.current.params || {};
    $scope.address = {};
    $scope.addressId = $scope.params.id || false;
    $scope.errorsForm = false;

    var geocoder, map, marker;
    $scope.toBack = function() {
        $location.path("/configuration/address-book");
    };
    var toEdit = function (id) {
        $location.path("/configuration/address-book/edit/" + id);
    };

    $scope.mapInUpdate = false;
    var inTyping = false;
    $scope.setAddressLine = function () {
        inTyping ? clearTimeout(inTyping) : false;
        inTyping = setTimeout(function () {
            $scope.setNewAddress();
        }, 1000);
    };

    $scope.setNewAddress = function (params) {

        params = params || {};

        var country = params.country;
        var noCode = params.noCode;
        var noLocation = params.noLocation;
        var noRoute = params.noRoute;
        var model = params.model || $scope.address.location;
        var requestData = {};
        var addressData = {
            country: country ? country.code : model['country']['code']
        };
        requestData.componentRestrictions = addressData;
        if (!country) {
            if (model.name && !noLocation) {
                addressData['locality'] = model['name'];
            }
            if (model.code && !noCode) {
                addressData['postalCode'] = model['code'];
            }
            if (model.state) {
                addressData['administrativeArea'] = model['state']['name'];
            }
            if ($scope.address.addressLine1 && !noRoute) {
                requestData.address = $scope.address.addressLine1;
            }
        }

        locationGetter ? clearTimeout(locationGetter) : false;
        $scope.mapInUpdate = true;

        locationGetter = setTimeout(function () {
            geocoder.geocode(requestData,
                function (result, status) {
                    if (result) {
                        if (!result.length) {
                            if (!noCode) {
                                $scope.setNewAddress({
                                    noCode: true,
                                    model: model
                                });
                            } else if (!noRoute) {
                                $scope.setNewAddress({
                                    noRoute: true,
                                    noCode: true,
                                    model: model
                                });
                            } else if (!noLocation) {
                                $scope.setNewAddress({
                                    noRoute: true,
                                    noCode: true,
                                    noLocation: true,
                                    model: model
                                });
                            } else {
                                $scope.mapInUpdate = false;
                                $scope.$apply();
                            }
                        } else {
                            $scope.mapInUpdate = false;
                            $scope.$apply();
                            var res = result[0];
                            var latlng = res['geometry']['location'];
                            var bounds = res['geometry']['viewport'];
                            map.fitBounds(bounds);
                            marker.setPosition({lat: latlng.lat(), lng: latlng.lng()});
                            map.setCenter({lat: latlng.lat(), lng: latlng.lng()});
                        }
                    } else {
                        $scope.setNewAddress({
                            noRoute: noRoute,
                            noCode: noCode,
                            model: model
                        });
                    }
                }
            )

        }, 100);
    };

    var locationGetter = false;
    var lastSelectedCountry = false;
    $scope.locationOptions = {
        onChangeModelCallback: function(n, o) {
            $scope.address.location = n;
            if (n) {
                lastSelectedCountry = n.country;
                $scope.setNewAddress({
                    model: n
                });
            }
        },
        onChangeCountryModelCallback: function (n, o) {
            if (n && !(n && lastSelectedCountry && (lastSelectedCountry.code == n.code))) {
                $scope.setNewAddress({
                    country: n
                })
            }
            lastSelectedCountry = n;
        }
    };

    $scope.saveAddress = function(addressForm) {
        if (addressForm.$valid) {
            $scope.addressFormProgress = true;
            var requestData = angular.copy($scope.address);
            requestData.location = requestData.location.id;
            AddressBookService.addToBook(requestData, function (data) {
                $scope.addressFormProgress = false;
                $scope.errorsForm = {
                    'code': 'Success',
                    'message': 'Data successfully updated',
                    'type': 'success'
                };
                toEdit(data.id);
            }, function(err) {
                $scope.errorsForm = err;
                $scope.addressFormProgress = false;
            });
        } else {
            addressForm.$setSubmitted();
            $scope.errorsForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
        }
    };


    $scope.getAddress = function() {
        if (!$scope.addressId) return;
        $scope.addressFormProgress = true;
        AddressBookService.getAddress($scope.addressId, function (adrs) {
            $scope.address = adrs;
            $scope.locationOptions.onChangeModelCallback(adrs.location);
            $scope.addressFormProgress = false;
        });
    };


    var onChangeMarker = function (event) {
        var latlng = event['latLng'];
        var center = {lat: latlng.lat(), lng: latlng.lng()};
        map.panTo(center);

        var countryCode = false;
        var cityCode = '';
        var cityName = '';


        geocoder.geocode({
            location: center
        }, function (result) {
            console.log(result);
            if (result.length) {
                var request = [];
                result[0]['address_components'].map(function (item) {
                    item.types.map(function (t) {
                        if ((t == 'street_number') || (t == 'route')) {
                            request.push(item['short_name']);
                        }
                    });
                });
                $scope.address.addressLine1 = request.join(', ');
                $scope.$apply();
            }
        });
    };


    /* Google map initialize */
    function initMap() {
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map-container'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 8
        });
        marker = new google.maps.Marker({
            map: map,
            icon: {
                'url': '/images/marker.svg',
                'scaledSize': new google.maps.Size(24, 24)
            },
            draggable: true
        });
        google.maps.event.addListener(marker, 'dragend', onChangeMarker);
    }
    initMap();

});