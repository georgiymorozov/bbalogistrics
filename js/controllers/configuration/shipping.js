'use strict';

var module = angular.module('app');

module.controller('ConfigurationShippingController', function($scope) {
    $scope.tableData = {
        'head': [{
            name: 'name',
            title: 'Name'
        },{
            name: 'channel',
            title: 'Channel'
        },{
            name: 'service',
            title: 'Default Service'
        },{
            name: 'code',
            title: 'Zip / Postcode'
        },{
            name: 'priority',
            title: 'Priority'
        }, {
            title: 'Actions',
            type: 'action'
        }
        ],
        'filters': true,
        'checked': true,
        'data': []
    };
    $scope.tableData.data = [{
        name: 'Sub total between 0 > 1000',
        channel: 'All',
        service: 'DHL Express Satchel',
        priority: '4',
        actions: [{
            type: 'btn-blue',
            action: false,
            text: 'Edit/View',
            icon: 'edit_icon'
        }]
    }, {
        name: 'Sub total between 0 > 1000',
        channel: 'All',
        service: 'DHL Express Satchel',
        priority: '4',
        actions: [{
            type: 'btn-blue',
            action: false,
            text: 'Edit/View',
            icon: 'edit_icon'
        }]
    }, {
        name: 'Sub total between 0 > 1000',
        channel: 'All',
        service: 'DHL Express Satchel',
        priority: '4',
        actions: [{
            type: 'btn-blue',
            action: false,
            text: 'Edit/View',
            icon: 'edit_icon'
        }]
    }, {
        name: 'Sub total between 0 > 1000',
        channel: 'All',
        service: 'DHL Express Satchel',
        priority: '4',
        actions: [{
            type: 'btn-blue',
            action: false,
            text: 'Edit/View',
            icon: 'edit_icon'
        }]
    }, {
        name: 'Sub total between 0 > 1000',
        channel: 'All',
        service: 'DHL Express Satchel',
        priority: '4',
        actions: [{
            type: 'btn-blue',
            action: false,
            text: 'Edit/View',
            icon: 'edit_icon'
        }]
    }];
});