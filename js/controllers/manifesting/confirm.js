'use strict';

var module = angular.module('app');

module.controller('ManifestingConfirmController', function($scope) {
    $scope.rangePickerOptions = {
        'inline': true,
        'container': '#date-ranger',
        'alwaysOpen':true,
        'singleMonth': true,
        'showShortcuts': false,
        'showTopbar': false
    };

    $scope.quotes = {
        'head': [
            {
                name: 'created',
                title: 'Created'
            }, {
                title: 'Company',
                type: 'logo',
                logo: 'logo',
                logoTitle: 'titleLogo',
                container: 'logotype-shipping',
                template: 'logotype.html'
            }, {
                name: 'mode',
                title: 'Mode'
            }, {
                name: 'eta',
                title: 'ETA'
            }, {
                name: 'total',
                title: 'Amount (Ex' + "\u00A0" + 'TAX)'
            }, {
                title: 'Confirm',
                type: 'action'
            }
        ],
        'checked': false,
        'filters': false,
        'nosort': true,
        'nopaging': true,
        'data': []
    };
    $scope.quotes.data = [{
        created: '1/05/15' + "\n" + '2.22pm',
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        mode: 'Road',
        eta: 'Road 1-2 days',
        total: '$180 AUD',
        actions: [
            {
                type: 'btn-red',
                action: function(){},
                text: 'book'
            }
        ]
    }, {
        created: '1/05/15' + "\n" + '2.22pm',
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        mode: 'Road',
        eta: 'Road 1-2 days',
        total: '$180 AUD',
        actions: [
            {
                type: 'btn-red',
                action: function(){},
                text: 'book'
            }
        ]
    }, {
        created: '1/05/15' + "\n" + '2.22pm',
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        mode: 'Road',
        eta: 'Road 1-2 days',
        total: '$180 AUD',
        actions: [
            {
                type: 'btn-red',
                action: function(){},
                text: 'book'
            }
        ]
    }, {
        created: '1/05/15' + "\n" + '2.22pm',
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        mode: 'Road',
        eta: 'Road 1-2 days',
        total: '$180 AUD',
        actions: [
            {
                type: 'btn-red',
                action: function(){},
                text: 'book'
            }
        ]
    }, {
        created: '1/05/15' + "\n" + '2.22pm',
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        mode: 'Road',
        eta: 'Road 1-2 days',
        total: '$180 AUD',
        actions: [
            {
                type: 'btn-red',
                action: function(){},
                text: 'book'
            }
        ]
    }];
});