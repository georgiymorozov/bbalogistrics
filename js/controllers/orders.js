'use strict';

var module = angular.module('app');

module.controller('OrdersController', function ($scope, $location, ShipmentsTable) {

    var data = {};
    $scope.menu = {
        title: 'Print orders',
        items: [{
            name: 'Print Thermal Labels',
            action: function (data) {
            }
        }, {
            name: 'Print Packing Slips',
            action: function (data) {
            }
        }, {
            name: 'Print Pick Slips',
            action: function (data) {
            }
        }
        ],
        data:  data
    };

    
    var orderTable = new ShipmentsTable();
    $scope.ordersTableData = orderTable.getTableOptions({
        order: [
            'date', 'bbaReferences', 'sscc', 'consignment',
            'references', 'bookingStatus', 'total', 'buttons'
        ],
        cellsOptions: {
            buttons: {
                bodyButtons: [
                    {
                        type: 'btn-blue',
                        action: function (data) {
                        },
                        icon: 'view_icon',
                        text: 'Docs'
                    }, {
                        type: 'btn-normal-blue',
                        action: function (data) {
                            $location.path('/orders/details/' + data.id);
                        },
                        icon: 'edit_icon',
                        text: 'View/Edit'
                    }, {
                        type: 'btn-light-grey',
                        action: function (data) {
                        },
                        icon: 'print_icon'
                    }
                ]
            },
            /** FIXME **/
            bbaReferences: {
                urlFormat: function (value) {
                    return '/orders/details/0';
                }
            }
        }
    });
});
