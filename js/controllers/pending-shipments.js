'use strict';

var module = angular.module('app');

module.controller('PendingShipmentsController', function ($scope, $route) {
    $scope.titlesPage = {
        process: {
            title: 'Current Profiles'
        },
        split: {
            title: 'Split Shipments:',
            afterTitle: 'Shipment Summary'
        }
    };
    $scope.transfer = 1;
    $scope.$watch('routeParams', function () {
        $scope.showedTab = '/templates/pending-shipments/' + $scope.routeParams.tab + '.html';
    });
    $scope.toTab = function (a) {
        $route.updateParams({tab: a});
    };
    $route.current.params.tab = $route.current.params.tab || 'process';
    $scope.routeParams = $route.current.params;

    var data = {};
    $scope.menu = {
        title:'Print Shipments ',
        items:[{
            name:'Print Thermal Labels',
            action:function(data){}
        },{
            name:'Print Packing Slips',
            action:function(data){}
        }, {
            name:'Print Pick Slips',
            action:function(data){}
        }
        ],
        data: data
    };

    
});