'use strict';

var module = angular.module('app');

module.controller('ReportingControllerView', function($scope) {
    $scope.itemsTable = {
        'head': [
            {
                title: 'Description',
                type: 'template',
                name: 'description',
                template: 'product-item.html'
            }
        ],
        'filters': false,
        'nosort': true,
        'nopaging': true,
        'data': []
    };
    $scope.itemsTable.data = [{
        description: {
            name: 'Rebel Team Black Small',
            quantity: '1',
            value: '$54.99',
            taxes: 'AUD',
            sku: 'SKU / Part # (GTIN)',
            country: 'Australia (AU)',
            weight: '0.49',
            total: '$54.99',
            type: 'Tee Shirt',
            measure: 'Centemeters',
            length: '10',
            width: '10',
            height: '15',
            category: 'Business',
            code: '00111001D00110011'
        },
        readable: true
    }, {
        description: {
            name: 'Rebel Team Black Small',
            quantity: '1',
            value: '$54.99',
            taxes: 'AUD',
            sku: 'SKU / Part # (GTIN)',
            country: 'Australia (AU)',
            weight: '0.49',
            total: '$54.99',
            type: 'Tee Shirt',
            measure: 'Centemeters',
            length: '10',
            width: '10',
            height: '15',
            category: 'Business',
            code: '00111001D00110011'
        },
        readable: true
    }];
});