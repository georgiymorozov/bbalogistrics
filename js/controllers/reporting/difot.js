'use strict';

var module = angular.module('app');

module.controller('ReportingControllerDifot', function($scope) {
    $scope.tableData = {
        'head': [
            {
                name: 'date',
                title: 'Date',
                type: 'date'
            },
            {
                name: 'status',
                title: 'Status'
            },
            {
                name: 'bbaNo',
                title: 'BBA Ref' + "\u00A0" + 'No.',
                template: 'link.html'
            },
            {
                name: 'consignment',
                title: 'Con' + "\u00A0" + 'No.'
            },
            {
                name: 'sscc',
                title: 'SSCC' + "\u00A0" + 'No.'
            },
            {
                name: 'customer',
                title: 'Customer Ref' + "\u00A0" + 'No.',
                template: 'link.html'
            },
            {
                name: 'tracking',
                title: 'Tracking'
            }, {
                title: 'Carrier',
                type: 'logo',
                logo: 'logo',
                logoTitle: 'titleLogo',
                logoText: 'logoText',
                container: 'logotype-shipping',
                template: 'text-logotype.html'
            }, {
                name: 'etd',
                title: 'ETD'
            }, {
                name: 'sorterStatus',
                title: 'Sorter Status'
            }, {
                title: 'Actions',
                type: 'action'
            }
        ],
        'checked': false,
        'data': []
    };
    $scope.tableData.data = [
        {
            date: '19/06/2015',
            status: 'Completed',
            bbaNo: 'BBA 1234567844',
            consignment: 'CP12345',
            sscc: '1112221',
            customer: '123456890',
            tracking: 'Completed',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            logoText: 'BBA Profile 1',
            etd: '1-5 Days',
            sorterStatus: 'On Schedule',
            actions: [
                {
                    action: function(){},
                    text: 'View',
                    type: 'btn-blue'
                }, {
                    action: function(){},
                    text: 'Reprint',
                    type: 'btn-normal-blue'
                }, {
                    action: function(){},
                    text: 'Tracking',
                    type: 'btn-orange'
                }
            ]
        }, {
            date: '19/06/2015',
            status: 'Completed',
            bbaNo: 'BBA 1234567844',
            consignment: 'CP12345',
            sscc: '1112221',
            customer: '123456890',
            tracking: 'Completed',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            logoText: 'BBA Profile 1',
            etd: '1-5 Days',
            sorterStatus: 'On Schedule',
            actions: [
                {
                    action: function(){},
                    text: 'View',
                    type: 'btn-blue'
                }, {
                    action: function(){},
                    text: 'Reprint',
                    type: 'btn-normal-blue'
                }, {
                    action: function(){},
                    text: 'Tracking',
                    type: 'btn-orange'
                }
            ]
        }, {
            date: '19/06/2015',
            status: 'Completed',
            bbaNo: 'BBA 1234567844',
            consignment: 'CP12345',
            sscc: '1112221',
            customer: '123456890',
            tracking: 'Completed',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            logoText: 'BBA Profile 1',
            etd: '1-5 Days',
            sorterStatus: 'On Schedule',
            actions: [
                {
                    action: function(){},
                    text: 'View',
                    type: 'btn-blue'
                }, {
                    action: function(){},
                    text: 'Reprint',
                    type: 'btn-normal-blue'
                }, {
                    action: function(){},
                    text: 'Tracking',
                    type: 'btn-orange'
                }
            ]
        }, {
            date: '19/06/2015',
            status: 'Completed',
            bbaNo: 'BBA 1234567844',
            consignment: 'CP12345',
            sscc: '1112221',
            customer: '123456890',
            tracking: 'Completed',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            logoText: 'BBA Profile 1',
            etd: '1-5 Days',
            sorterStatus: 'On Schedule',
            actions: [
                {
                    action: function(){},
                    text: 'View',
                    type: 'btn-blue'
                }, {
                    action: function(){},
                    text: 'Reprint',
                    type: 'btn-normal-blue'
                }, {
                    action: function(){},
                    text: 'Tracking',
                    type: 'btn-orange'
                }
            ]
        }, {
            date: '19/06/2015',
            status: 'Completed',
            bbaNo: 'BBA 1234567844',
            consignment: 'CP12345',
            sscc: '1112221',
            customer: '123456890',
            tracking: 'Completed',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            logoText: 'BBA Profile 1',
            etd: '1-5 Days',
            sorterStatus: 'On Schedule',
            actions: [
                {
                    action: function(){},
                    text: 'View',
                    type: 'btn-blue'
                }, {
                    action: function(){},
                    text: 'Reprint',
                    type: 'btn-normal-blue'
                }, {
                    action: function(){},
                    text: 'Tracking',
                    type: 'btn-orange'
                }
            ]
        }
    ];
});