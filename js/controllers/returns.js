'use strict';

var module = angular.module('app');

module.controller('ReturnsController', function ($scope, ShipmentsService, $filter, $rootScope, ShipmentsTable, CarriersTable, ItemsTable, shipmentSelectsConsts) {

    /** For Tabs **/
    $scope.selectOptions = shipmentSelectsConsts;

    $scope.showShipmentsTab = false;
    $scope.showAddressTab = true;
    $scope.showQuoteTab = true;
    $scope.showDetailsTab = true;

    $scope.selectedShipment = false;

    /***************** Forms data *****************/

    /** Plugins options **/
    $scope.times = [];
    var timeRange = 30;
    for (var t = 0; t < 1440; t += timeRange) {
        var h = Math.floor(t / 60);
        var m = t % 60;
        h = (h < 10 ? '0' : '') + h;
        m = (m < 10 ? '0' : '') + m;
        $scope.times.push(h + ':' + m);
    }

    var dateFormat = 'yyyy-MM-dd';

    $scope.rangePickerOptions = {
        'dateFormat': 'yy-mm-dd'
    };


    /** Request Object **/
    $scope.bookingRequest = {
        sourceAddress: {},
        destinationAddress: {},
        pickupDetails: {
            pickupDate: $filter('date')(new Date(), dateFormat),
            pickupTimezone: $filter('date')(new Date(), 'Z'),
            pickupEarliestTime: $scope.times[0],
            pickupLatestTime: $scope.times[0]
        },
        packages: [],
        items: [],
        reasonForShipping: $scope.selectOptions.ReasonForExport[0]['value'],
        taxStatus: $scope.selectOptions.TaxStatus[0],
        customerReferences: []
    };

    /**  Returns Table **/

    var returnsTable = new ShipmentsTable({
        status: 'Unprocessed'
    });
    $scope.returnsTable = returnsTable.getTableOptions({
        cellsOptions: {
            buttons: {
                bodyButtons: [{
                    type: 'btn-blue block width-100',
                    action: function (data) {},
                    icon: 'edit_icon',
                    text: 'View/Edit'
                }]
            }
        }
    });

    /**  Shipments Table **/

    var shipmentsTable = new ShipmentsTable();
    var selectedShipmentButton = false;


    var cancelReturnButton = {
        text: 'Cancel',
        type: 'btn-red'
    };
    var revertButton = function(btn) {
        angular.element('.btn-text', btn).text(returnButton.text);
        btn.
            addClass(returnButton.type).
            removeClass(cancelReturnButton.type);
    };


    var resetSelected = function() {
        $scope.returnObjectRequest = {
            sourceAddress: {},
            destinationAddress: {},
            pickupDetails: {
                pickupDate: $filter('date')(new Date(), 'yyyy-MM-dd'),
                pickupTimezone: $filter('date')(new Date(), 'Z'),
                pickupEarliestTime: $scope.times[0],
                pickupLatestTime: $scope.times[0]
            },
            packages: [{}],
            items: [{}]
        };
        $scope.selectedShipment = false;
    };

    resetSelected();

    var onChangeWarehouse = function (address) {
        if (!address) return;
        $scope.returnObjectRequest.destinationAddress = angular.copy(address);
    };

    $scope.warehouseAutoSelect = {
        textField: 'company',
        searchField: 'company',
        service: 'Warehouse',
        onChange: onChangeWarehouse,
        placeholder: 'Select Pre-Defined Warehouse...'
    };



    var returnButton = {
        text: 'Return',
        type: 'btn-normal-blue',
        action: function (data, btn) {
            btn = angular.element(btn.currentTarget);
            resetSelected();
            if (selectedShipmentButton && selectedShipmentButton.is(btn)) {
                revertButton(btn);
                selectedShipmentButton = false;
            } else {
                selectedShipmentButton ?
                    revertButton(selectedShipmentButton) :
                    false;
                selectedShipmentButton = btn;


                /* FIXME */
                data.items.map(function(item) {
                    for (var k in productsOptionsLists) {
                        if (item[k]) {
                            productsOptionsLists[k].push(item[k]);
                        } else {
                            item[k] = productsOptionsLists[k][0];
                        }
                    }
                });


                for (var key in data) {
                    switch (key) {
                        case 'pickupDetails': break;
                        case 'sourceAddress':
                            $scope.returnObjectRequest.sourceAddress = angular.copy(data.destinationAddress);
                            break;
                        case 'destinationAddress':
                            $scope.returnObjectRequest.destinationAddress = angular.copy(data.sourceAddress);
                            break;
                        default:
                            $scope.returnObjectRequest[key] = angular.copy(data[key]);
                            break;
                    }
                }

                productsTable.updateTableData($scope.returnObjectRequest.items);

                $scope.selectedShipment = true;
                angular.element('.btn-text', btn).text(cancelReturnButton.text);
                btn.
                    addClass(cancelReturnButton.type).
                    removeClass(returnButton.type);
            }
        }
    };


    var viewButton = {
        type: 'btn-blue',
        action: function (data) {},
        text: 'View'
    };


    $scope.shipmentsTable = shipmentsTable.getTableOptions({
        table: {
            onInitTable: false
        },
        cellsOptions: {
            buttons: {
                bodyButtons: [viewButton, returnButton]
            }
        }
    });


    $scope.openTableForReturns = function() {
        $scope.showShipmentsTab = !$scope.showShipmentsTab;
        $scope.showShipmentsTab && !selectedShipmentButton ?
            shipmentsTable.updateTableData() :
            false;
    };

    /** Quotes **/

    var quotesTable = new CarriersTable();

    var selectCarrier = function(data) {
        $scope.returnObjectRequest.carrierId = data.carrierId;
        $scope.returnObjectRequest.serviceId = data.serviceId;
    };

    $scope.quotesTable = quotesTable.getTableOptions({
        table: {
            onSelect: selectCarrier
        }
    });

    $scope.getQuotes = function(packagesForm, addressesForm) {
        addressesForm.$setSubmitted();
        packagesForm.$setSubmitted();
        $scope.errorsPackagesForm =
            $scope.errorsAddressesForm =
                $scope.errorsCarrierForm = false;

        if (!(packagesForm.$valid && addressesForm.$valid)) {
            if (!packagesForm.$valid) {
                $scope.errorsPackagesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
            if (!addressesForm.$valid) {
                $scope.errorsAddressesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
            return;
        }
        var source =        $scope.returnObjectRequest.sourceAddress.location;
        var destination =   $scope.returnObjectRequest.destinationAddress.location;

        var requestData = {
            "source": {
                "country":  source.country.code,
                "city":     source.name,
                "postcode": source.code
            },
            "destination": {
                "country":  destination.country.code,
                "city":     destination.name,
                "postcode": destination.code
            }
        };
        requestData.packages = $scope.returnObjectRequest.packages;
        quotesTable.updateTableData(requestData, function () {
        });
    };

    var removedPackages = [];

    var createPackage = function() {
        var newPackage = {};
        if (removedPackages.length) {
            newPackage = removedPackages.pop();
        }
        $scope.returnObjectRequest.packages.push(newPackage);
    };
    $scope.addPackage = function(packagesForm) {
        if (!packagesForm.$valid) {
            $scope.errorsPackagesForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
            return;
        }
        packagesForm.$setPristine();
        return false;
    };

    $scope.removePackage = function() {
        if ($scope.returnObjectRequest.packages.length > 1) {
            removedPackages.push($scope.returnObjectRequest.packages.pop());
        }
    };


    /** Items Table **/
    var productsTable = new ItemsTable();


    /* FIXME */
    var productsOptionsLists = {
        measures: ['Fix it'],
        taxes: ['Fix it'],
        category: ['Fix it'],
        itemType: ['Fix it']
    };
    $scope.itemsTable = productsTable.getTableOptions({
        cellsOptions: {
            description: {
                selectOptions: productsOptionsLists
            }
        }
    });
    productsTable.updateTableData($scope.returnObjectRequest.items);
    $scope.sentReturnForm = function(addressesForm, packagesForm, carriersForm) {
        addressesForm.$setSubmitted();
        packagesForm.$setSubmitted();
        carriersForm.$setSubmitted();

        $scope.productsFormErrors = false;
        $scope.deliveryFormErrors = false;
        $scope.errorsCarrierForm = false;

        if (!(addressesForm.$valid && packagesForm.$valid && carriersForm.$valid)) {
            if (!addressesForm.$valid) {
                $scope.errorsAddressesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
            if (!packagesForm.$valid) {
                $scope.errorsPackagesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
            if (!carriersForm.$valid) {
                $scope.showQuoteTab = true;
                $scope.errorsCarrierForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
            }
            return;
        }

        ShipmentsService.createShipment($scope.returnObjectRequest, function (data) {
            // Success

        }, function(data) {
            // Error

        });
    };
});