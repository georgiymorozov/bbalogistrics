'use strict';

var module = angular.module('app');

module.controller('QuoteBookController', function ($scope, ShipmentsService, $filter, PackagesService, CarriersTable, shipmentSelectsConsts, $location, $rootScope, LocationService) {

    $scope.selectOptions = shipmentSelectsConsts;
    $scope.isDomestic = false;
    $scope.deliveryDetailsTab = true;

    /** Plugins options **/
    $scope.times = [];
    var timeRange = 30;
    for (var t = 0; t < 1440; t += timeRange) {
        var h = Math.floor(t / 60);
        var m = t % 60;
        h = (h < 10 ? '0' : '') + h;
        m = (m < 10 ? '0' : '') + m;
        $scope.times.push(h + ':' + m);
    }

    var dateFormat = 'yyyy-MM-dd';

    $scope.rangePickerOptions = {
        'dateFormat': 'yy-mm-dd'
    };

    /** Request Object **/
    $scope.bookingRequest = {
        sourceAddress: {},
        destinationAddress: {},
        pickupDetails: {
            pickupDate: $filter('date')(new Date(), dateFormat),
            pickupTimezone: $filter('date')(new Date(), 'Z'),
            pickupEarliestTime: $scope.times[0],
            pickupLatestTime: $scope.times[0]
        },
        packages: [],
        items: [],
        reasonForShipping: $scope.selectOptions.ReasonForExport[0]['value'],
        taxStatus: $scope.selectOptions.TaxStatus[0],
        customerReferences: []
    };

    /** Packages **/

    var removedPackages = [];
    $scope.packagesOriginalList = [
        {
            name: 'Custom'
        }
    ];


    $scope.selectedPackages = [];
    $scope.addPackage = function (packagesForm) {
        if (packagesForm) {
            packagesForm.$setSubmitted();
            if (!packagesForm.$valid) {
                $scope.errorsPackagesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                return;
            }
        }
        if (removedPackages.length) {
            $scope.selectedPackages.push(removedPackages.pop());
            return;
        }
        var newList = angular.copy($scope.packagesOriginalList);

        var pack = {
            pack: newList[0],
            list: newList,
            custom: newList[0]
        };

        shipmentSelectsConsts.UNITS.combined.map(function (unit) {
            if ((unit.measureUnit == $rootScope.GlobalProfile.measureUnit) && (unit.weightUnit == $rootScope.GlobalProfile.weightUnit)) {
                pack['custom']['units'] = unit;
            }
        });

        if (!pack['custom']['units']) {
            pack['custom']['units'] = shipmentSelectsConsts.UNITS.combined[0];
        }
        $scope.selectedPackages.push(pack);
    };

    $scope.removePackage = function () {
        if ($scope.selectedPackages.length > 1) {
            removedPackages.push($scope.selectedPackages.pop());
        }
    };

    PackagesService.getList(function (data) {
        $scope.packagesOriginalList = $scope.packagesOriginalList.concat(data.content);
        $scope.selectedPackages.map(function (item) {
            item.list = item.list.concat(data.content);
        });
    });

    $scope.addPackage();

    /** Quotes **/

    var quotesTable = new CarriersTable();

    var setPickupDateTime = function () {
        var EarliestTime = new Date();
        var LatestTime = new Date();

        var eTime = Math.ceil((EarliestTime.getTime() + Math.round(100000000 * Math.random())) / (timeRange * 60000)) * timeRange * 60000;
        EarliestTime.setTime(eTime);
        var lTime = Math.ceil((EarliestTime.getTime() + Math.round(100000000 * Math.random())) / (timeRange * 60000)) * timeRange * 60000;
        LatestTime.setTime(lTime);
        var he = EarliestTime.getHours();
        var me = EarliestTime.getMinutes();
        var hl = LatestTime.getHours();
        var ml = LatestTime.getMinutes();
        $scope.bookingRequest.pickupDetails = {
            pickupDate: $filter('date')(EarliestTime, dateFormat),
            pickupTimezone: $filter('date')(new Date(), 'Z'),
            pickupEarliestTime: ((he < 10 ? '0' : '') + he) + ':' + ((me < 10 ? '0' : '') + me),
            pickupLatestTime: ((hl < 10 ? '0' : '') + hl) + ':' + ((ml < 10 ? '0' : '') + ml)
        };
    };

    var selectCarrier = function (data) {
        $scope.onSelectCarrier = !$scope.onSelectCarrier;
        $scope.bookingRequest.carrierId = data.carrierId;
        $scope.bookingRequest.serviceId = data.serviceId;
        /** FixMe **/
        setPickupDateTime();
    };

    $scope.quotesTable = quotesTable.getTableOptions({
        table: {
            onSelect: selectCarrier
        }
    });

    $scope.quotesTab = true;
    var resetCarrier = function () {
        $scope.bookingRequest.carrierId = undefined;
        $scope.bookingRequest.serviceId = undefined;
        quotesTable.resetTableData();
    };
    $scope.getQuotes = function (addressesForm, packagesForm) {

        if (!(addressesForm.$valid && packagesForm.$valid)) {
            if (!addressesForm.$valid) {
                $scope.errorsAddressesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                addressesForm.$setSubmitted();
                return;
            } else {
                $scope.errorsAddressesForm = null;
            }
            if (!packagesForm.$valid) {
                $scope.errorsPackagesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                packagesForm.$setSubmitted();
            } else {
                $scope.errorsPackagesForm = null;
            }
            return;
        }

        $scope.errorsCarrierForm = null;
        $scope.inGetQuote = true;

        var source = $scope.sourceAddressLocation.location;
        var destination = $scope.destinationAddressLocation.location;

        var quotesRequestData = {
            "source": {
                "country": source.country.code,
                "city": source.name,
                "postcode": source.code
            },
            "destination": {
                "country": destination.country.code,
                "city": destination.name,
                "postcode": destination.code
            }
        };


        $scope.bookingRequest.packages = [];

        $scope.selectedPackages.map(function (pack) {
            pack.pack.quantity = pack.custom.quantity;

            /** FIXME **/
            pack.pack.measure = pack.custom.units.values.measureUnit;
            $scope.bookingRequest.packages.push(PackagesService.generatePackage(pack.pack));
        });
        quotesRequestData.packages = $scope.bookingRequest.packages;

        resetCarrier();

        quotesTable.updateTableData(quotesRequestData, function () {
            $scope.quotesTab = true;
            $scope.inGetQuote = false;
            $scope.deliveryDetailsTab = true;
            isDomestic();
            $scope.scrollToQuotes = !$scope.scrollToQuotes;
        }, function (error) {
            $scope.inGetQuote = false;
            console.log(error);
        });
    };

    /** Items form control **/
    var removedProducts = [];
    $scope.itemsArray = [];
    $scope.addProduct = function(productsForm) {
        if (productsForm && !productsForm.$valid) {
            productsForm.$setSubmitted();
            return;
        }
        $scope.itemsArray.push(removedProducts.length ? removedProducts.pop() : {
            sku: '',
            description: '',
            quantity: 1,
            value: 0,
            total: ''
        });
    };
    $scope.addProduct();
    $scope.removeLastProduct = function() {
        if ($scope.itemsArray.length > 1) {
            removedProducts.push($scope.itemsArray.pop());
        }
    };
    $scope.correctPrice = function(arr) {
        var n = 1;
        arr.map(function(a) {
            a*= 1;
            n = n * (isNaN(a) ? 0 : a);
        });
        return n.toFixed(2);
    };

    /** Shipment Create Methods **/
    $scope.sentShipment = function (addressesForm, packagesForm, deliveryForm, productsForm, carriersForm, packageDescriptionForm) {

        $scope.productsFormErrors = null;
        $scope.deliveryFormErrors = null;
        $scope.errorsCarrierForm = null;
        $scope.packageDescriptionFormErrors = null;

        if (!(addressesForm.$valid && packagesForm.$valid && carriersForm.$valid && productsForm.$valid && deliveryForm.$valid && packageDescriptionForm.$valid)) {
            if (!addressesForm.$valid) {
                $scope.errorsAddressesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                addressesForm.$setSubmitted();
                return;
            } else {
                $scope.errorsAddressesForm = null;
            }
            if (!packagesForm.$valid) {
                $scope.errorsPackagesForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                packagesForm.$setSubmitted();
                return;
            } else {
                $scope.errorsPackagesForm = null;
            }
            if (!carriersForm.$valid) {
                $scope.errorsCarrierForm = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                carriersForm.$setSubmitted();
                return;
            }
            if (!deliveryForm.$valid) {
                $scope.deliveryFormErrors = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                deliveryForm.$setSubmitted();
                return;
            }
            if (!packageDescriptionForm.$valid) {
                $scope.packageDescriptionFormErrors = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                packageDescriptionForm.$setSubmitted();
                return;
            }
            if (!productsForm.$valid) {
                $scope.productsFormErrors = {
                    'code': $scope['languageLibrary']['error_title_uncorrected'],
                    'message': 'Please ﬁx the below and try again'
                };
                productsForm.$setSubmitted();
                return;
            }

            return;
        }

        $scope.bookingRequest.items = [];

        $scope.itemsArray.map(function(item) {
            $scope.bookingRequest.items.push({
                "description": item.description,
                "quantity": item.quantity,
                "value": item.value,
                "manufacturer": item.country.code
                //"itemType": "Fix it",
                //"category": "Fix it"
            });
        });

        $scope.sentShipmentsInProgress = true;

        ShipmentsService.createShipment($scope.bookingRequest, function (data) {
            $location.path('/orders/details/' + data.id);
            $scope.sentShipmentsInProgress = false;
        }, function (data) {
            $scope.sentShipmentsInProgress = false;
        });
    };

    $scope.sentShipmentsInProgress = false;



    /** Set Addresses from Address Book **/

    var addressBookAutocomplete = {
        textField: 'company',
        searchField: 'searchValue',
        service: 'Warehouse'
    };

    var isDomestic = function (from, to) {

        var s = from || ($scope.sourceAddressLocation.location ? $scope.sourceAddressLocation.location.country : false);
        var d = to || ($scope.destinationAddressLocation.location ? $scope.destinationAddressLocation.location.country : false);

        if (!(s && d)) {
            $scope.isDomestic = false;
            return;
        }
        $scope.isDomestic = ShipmentsService.isDomestic(s.code, d.code);
        if ($scope.isDomestic) {
            $scope.itemsArray = [];
            $scope.bookingRequest.items = [];
        } else if (!$scope.itemsArray.length) {
            $scope.addProduct();
        }
    };

    $scope.sourceAddressLocation = {};
    $scope.destinationAddressLocation = {};

    var onSelectSourceAddress = function (address) {
        if (!address) return;
        $scope.bookingRequest.sourceAddress = address;
        $scope.sourceAddressLocation = angular.copy(address);
    };

    var onSelectDestinationAddress = function (address) {
        if (!address) return;
        $scope.bookingRequest.destinationAddress = address;
        $scope.destinationAddressLocation = angular.copy(address);
    };

    $scope.sourceAddressSelect = angular.copy(addressBookAutocomplete);
    $scope.sourceAddressSelect.onChange = onSelectSourceAddress;
    $scope.sourceAddressSelect.placeholder = 'Warehouses...';
    $scope.sourceAddressSelect.service = 'Warehouse';
    $scope.sourceAddressSelect.textField = 'name';
    $scope.destinationAddressSelect = angular.copy(addressBookAutocomplete);
    $scope.destinationAddressSelect.onChange = onSelectDestinationAddress;
    $scope.destinationAddressSelect.placeholder = 'Address Book...';

    $scope.locationOptionsFrom = {
        onChangeCountryModelCallback: function (country) {
            resetCarrier();
            isDomestic(country);
        },
        onChangeModelCallback: function (location, oldModel) {
            $scope.bookingRequest.sourceAddress.location = location;
            $scope.sourceAddressLocation.location = angular.copy(location);
        }
    };

    $scope.locationOptionsTo = {
        onChangeCountryModelCallback: function (country) {
            resetCarrier();
            isDomestic(false, country);
        },
        onChangeModelCallback: function (location, oldModel) {
            $scope.bookingRequest.destinationAddress.location = location;
            $scope.destinationAddressLocation.location = angular.copy(location);
        }
    };


});