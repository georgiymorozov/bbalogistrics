'use strict';

var module = angular.module('app');

module.controller('CoreController', function($scope, $route) {
    'use strict';

    $scope.$watch('routeParams', function() {
        var template = $scope.routeParams.tab == 'viewactivity' ? 'useractivity' : $scope.routeParams.tab;
        $scope.showedTab = '/templates/core/' + template + '.html';
    });

    $scope.toTab = function(a) {
        $route.updateParams({tab: a});
    };

    $route.current.params.tab = $route.current.params.tab || 'instore';
    $scope.routeParams = $route.current.params;

}).controller('CoreInStoreController', function($scope) {
    $scope.tableData = {
        'head': [{
            name: 'firstName',
            title: 'First Name'
        },{
            name: 'lastName',
            title: 'Last Name'
        },{
            name: 'phone',
            title: 'Phone'
        },{
            name: 'email',
            title: 'Email'
        },{
            name: 'location',
            title: 'Store Location (warehouse link)'
        },{
            type: 'action',
            title: 'Actions'
        }
        ],
        'filters': true,
        'checked': false,
        'data': []
    };
    $scope.tableData.data = [
        {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }, {
            firstName: 'John',
            lastName: 'Smith',
            phone: '0412 345 786',
            email: 'john.smith@email.com',
            location: 'Location A (Myer Chadstone)',
            actions: [
                {
                    text: 'Edit / View',
                    icon: 'edit_icon',
                    type: 'btn-blue'
                }
            ]
        }
    ];
}).controller('CoreUserActivityController', function($scope) {
    $scope.tableData = {
        'head': [{
            name: 'date',
            title: 'Date',
            type: 'date'
        },{
            name: 'firstName',
            title: 'First Name'
        },{
            name: 'lastName',
            title: 'Last Name'
        },{
            name: 'storeName',
            title: 'Store Name'
        },{
            type: 'action',
            title: 'Actions'
        }
        ],
        'filters': true,
        'checked': false,
        'data': []
    };
    $scope.tableData.data = [
        {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }, {
            date: '12/12/15',
            firstName: 'John',
            lastName: 'Smith',
            storeName: 'Myer Chadstone',
            actions: [
                {
                    icon: 'view_icon',
                    text: 'VIEW ACTIVITY',
                    type: 'btn-blue'
                }
            ]
        }
    ];
}).controller('CoreViewUserActivityController', function($scope) {
    $scope.tableData = {
        'head': [{
            name: 'date',
            title: 'Date',
            type: 'date'
        },{
            name: 'firstName',
            title: 'First Name'
        },{
            name: 'lastName',
            title: 'Last Name'
        },{
            name: 'sku',
            title: '#SKU / Part'
        },{
            name: 'description',
            title: 'Description'
        },{
            name: 'phone',
            title: 'Phone'
        },{
            name: 'email',
            title: 'Email'
        },{
            name: 'timeOfCollection',
            title: 'Time of Collection'
        }
        ],
        'filters': true,
        'checked': false,
        'data': []
    };
    $scope.tableData.data = [
        {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }, {
            date: '12/12/15',
            firstName: 'Paul',
            lastName: 'Jones',
            sku: '3942732',
            description: 'Blue Dress',
            phone: '3956 2966',
            email: 'trudy.jones@email.com',
            timeOfCollection: '23/01/16 10:11'
        }
    ];
});




