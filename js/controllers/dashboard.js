'use strict';


var module = angular.module('app');

module.controller('DashboardController', function ($scope, ShipmentsTable, ShipmentsService) {
    var transitTable = new ShipmentsTable();
    $scope.transitTableData = transitTable.getTableOptions({
        order: [
            'date', 'trackingNumber', 'references', 'consignment', 'carrier', 'total', 'buttons'
        ],
        cellsOptions: {
            buttons: {
                bodyButtons: [
                    {
                        type: 'btn-blue',
                        action: function (data) {
                        },
                        text: 'View'
                    }, {
                        type: 'btn-orange',
                        action: function (data) {
                        },
                        icon: 'tracking_icon',
                        text: 'Tracking'
                    }
                ]
            }
        }
    });

    /** Monthly chart configuration **/
    var generateDaysData = function() {
        var dataArray = [];
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear() - 10;
        var currentMonth = currentDate.getMonth();

        var day = 0;
        var startDate = new Date(currentYear, currentMonth, day);
        var date = startDate;
        while (date < currentDate) {
            dataArray.push({
                date: date,
                value: 50000 - Math.round(50000 * Math.random())
            });
            day++;
            date = new Date(startDate.getTime() + day * 24 * 3600000);
        }
        return dataArray;
    };
    var generateMonthlyData = function() {
        var dataArray = generateDaysData();
        var returnData = {};

        dataArray.map(function(cur) {
            var currentDate = cur.date;
            var month = currentDate.getMonth();
            var year = currentDate.getFullYear();
            var index = month + '-' + year;
            if (!returnData[index]) {
                returnData[index] = {
                    dailyData: [],
                    date: new Date(year, month),
                    value: 0
                };
            }
            returnData[index]['dailyData'].push({
                date: currentDate,
                value: cur.value
            });
            returnData[index]['value']+=  cur.value;
        });
        var returnMonthlyArray = [];
        for (var k in returnData) {
            returnMonthlyArray.push(returnData[k]);
        }
        return returnMonthlyArray;
    };

    var fullData = generateMonthlyData();

    $scope.monthlyChart = {
        "type": "serial",
        "dataProvider": fullData,
        "valueAxes": [{
            parseDates: true,
            minPeriod: "MM",
            dashLength: 4,
            minorGridEnabled: false,
            gridAlpha: 0.1,
            fontSize: 13,
            color: '#666666',
            axisColor: '#999999'
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0,
            "animationDuration": 0,
            fadeOutDuration: 0.1
        },
        "graphs": [{
            id: "monthly",
            type: "smoothedLine", // this line makes the graph smoothed line.
            lineColor: "#0085c2",
            bullet: "round",
            bulletSize: 10,
            bulletColor: '#ffb000',
            bulletBorderColor: "#c0dce1",
            bulletBorderAlpha: 1,
            fillColorsField: '#0085c2',
            fillAlphas: 0.8,
            bulletBorderThickness: 1,
            lineThickness:  1,
            valueField: "value",
            balloonText:
                "<span style='font-size: 1.5em; line-height: 1.8em;'>[[category]]</span><br/>" +
                "<b><span style='font-size: 1.75em; line-height: 1.8em;'>$[[value]]</span></b><br/>" +
                "<span style='color: #777'>Click for a detailed view</span>"
        }],
        "categoryAxis": {
            parseDates: true,
            minPeriod: "MM",
            dashLength: 4,
            minorGridEnabled: false,
            gridAlpha: 0.1,
            fontSize: 13,
            color: '#666666',
            axisColor: '#999999'
        },
        "chartScrollbar": {
            oppositeAxis: true,
            offset: 0,
            scrollbarHeight: 50,
            backgroundColor: "#888888",
            graphLineColor: '#0085c2',
            graphFillColor: '#0085c2',
            backgroundAlpha: 0.1,
            graphFillAlpha: 0.2,
            graphLineAlpha: 0.3,

            selectedBackgroundColor: "#888888",
            selectedGraphLineColor: '#0085c2',
            selectedGraphFillColor: '#0085c2',
            selectedBackgroundAlpha: 0.15,
            selectedGraphFillAlpha: 0.4,
            selectedGraphLineAlpha: 0.6,
            autoGridCount: true,
            graph: 'monthly',
            fontSize: 11,
            color: '#666666',
            selectedColor: '#000000'
        },
        marginLeft: 0,
        categoryField: "date",
        dataDateFormat: "MM YYYY",
        balloonDateFormat: 'MMMM, YYYY',
        creditsPosition: "bottom-right",
        fontFamily: '"Ubuntu", sans-serif',
        fontSize: 10,
        color: '#444444',
        "export": {
            "enabled": true
        },
        listeners: [
            {
                "event": "drawn",
                "method": function(event) {
                    event.chart.zoomToIndexes(fullData.length - 15, fullData.length - 1);
                }
            },
            {
                "event": "clickGraphItem",
                "method": function(graphItem) {
                    if (dayChart) {
                        dayChart.dataProvider = graphItem.item.dataContext.dailyData;
                        dayChart.validateData();
                        $scope.dailyDateInfo = graphItem.item.dataContext.dailyData[0].date;
                        $scope.$apply();
                    }
                }
            }
        ]
    };
    var dailyData = fullData[fullData.length - 1].dailyData;
    $scope.dailyDateInfo = dailyData[0].date;

    /** Days chart configuration **/

    var dayChart = false;
    $scope.daysChart = {
        "type": "serial",
        "dataProvider": dailyData,
        "autoDisplay": true,
        "valueAxes": [{
            parseDates: true,
            dashLength: 4,
            minorGridEnabled: false,
            fontSize: 13,
            color: '#666666',
            gridAlpha: 0.1,
            axisColor: '#999999'
        }],
        "balloon": {
            "borderThickness": 1,
            "shadowAlpha": 0,
            "animationDuration": 0,
            fadeOutDuration: 0.1
        },
        "graphs": [{
            id: "daily",
            type: "column", // this line makes the graph smoothed line.
            lineColor: "#ffb000",
            fillColorsField: '#efb000',
            fillAlphas: 0.8,
            lineThickness:  1,
            valueField: "value",
            balloonText:
            "<span style='font-size: 1.5em; line-height: 1.8em;'>[[category]]</span><br/>" +
            "<b><span style='font-size: 1.75em; line-height: 1.8em;'>$[[value]]</span></b><br/>" +
            "<span style='color: #777'>Click for a detailed view</span>"
        }],
        "categoryAxis": {
            parseDates: true,
            minPeriod: "DD",
            dashLength: 4,
            minorGridEnabled: false,
            fontSize: 13,
            color: '#666666',
            gridAlpha: 0.1,
            axisColor: '#999999'
        },
        marginLeft: 0,
        categoryField: "date",
        dataDateFormat: "MM YYYY",
        balloonDateFormat: 'DD MMM, YYYY',
        creditsPosition: "bottom-right",
        fontFamily: '"Ubuntu", sans-serif',
        fontSize: 10,
        color: 'rgba(128, 128, 128, 0)',
        "export": {
            "enabled": true
        },
        listeners: [
            {
                "event": "init",
                "method": function(chart) {
                    dayChart = chart.chart;
                }
            }
        ]
    };

});
