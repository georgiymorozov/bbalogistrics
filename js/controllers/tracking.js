'use strict';

var module = angular.module('app');

module.controller('TrackingController', function ($scope, ShipmentsService, $filter, ShipmentsTable, $route) {
    $scope.search = '';
    $scope.selectedShipment = false;
    $scope.inSearch = false;
    $scope.inLoadShipment = false;
    $scope.toTracking = false;
    var params = $route.current.params || {};
    var shipment = params.id || false;

    var oldButton = false;
    var showedShipment = false;
    var showTracking = function(data, button) {
        if (showedShipment && (showedShipment == data.id)) return;
        showedShipment = data.id;
        if (button) {
            button = angular.element(button.currentTarget);
            if (oldButton) {
                oldButton.removeClass('no-active');
            }
            oldButton = button.addClass('no-active');
        }
        $scope.inLoadShipment = true;
        ShipmentsService.getTracking(data.id, function (tracking) {
            $scope.selectedShipment = data;
            $scope.selectedShipment.tracking = tracking;
            $scope.inLoadShipment = false;
            $scope.toTracking = !$scope.toTracking;
        }, function() {
            $scope.inLoadShipment = false;
            /* console.log(arguments); */
        });
    };

    if (shipment) {
        ShipmentsService.getShipment(shipment, function (ship) {
            $scope.search = ship.trackingNumber;
            showTracking(ship);
        })
    }

    var shipmentsTable = new ShipmentsTable();

    $scope.trackingTableData = shipmentsTable.getTableOptions({
        cellsOptions: {
            buttons: {
                bodyButtons: [{
                    type: 'btn-dark-blue',
                    action: showTracking,
                    text: 'Tracking'
                }]
            }
        },
        table: {
            onInitTable: false
        }
    });


    var geocoder, map;
    $scope.initMap = function() {
        if (!($scope.selectedShipment.tracking && $scope.selectedShipment.tracking.trackingEvents)) return;
        geocoder = new google.maps.Geocoder();
        map = new google.maps.Map(document.getElementById('map-container'), {
            center: {lat: -34.397, lng: 150.644},
            zoom: 8
        });
        var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
        };
        var path = [];
        $scope.selectedShipment.tracking.trackingEvents.map(function(item) {
            path.push(item.locationDetails);
        });
        var polyOptions = {
            strokeColor: '#0070cf',
            strokeOpacity: 0.8,
            strokeWeight: 5,
            geodesic: false,
            path: path
            /*icons: [{
                icon: lineSymbol,
                offset : '10%',
                repeat : '200px'
            }]*/
        };
        var poly = new google.maps.Polyline(polyOptions);
        poly.setMap(map);
    };

    $scope.sentSearchTracking = function() {
        if (!$scope.search || !$scope.search.length) return;
        $scope.selectedShipment = false;
        $scope.inSearch = true;
        showedShipment = false;
        oldButton = false;
        shipmentsTable.updateTableData(false, {item: $scope.search}, function(error) {
            $scope.inSearch = false;
        });
    };
});