'use strict';

var module = angular.module('app');

module.controller('OrderViewController', function($scope) {
    $scope.ordersTable = {
        'head': [
            {
                name: 'date',
                title: 'Date',
                type: 'date',
                sort: 'originalDate'
            },{
                name: 'reciever',
                title: 'Reciever'
            },{
                name: 'consignment',
                title: 'Consignment' + "\u00A0" + 'No.'
            },{
                name: 'customer',
                title: 'Customer Ref.' + "\u00A0" + 'No.',
                template: 'array-links.html'
            },{
                name: 'tracking',
                title: 'Tracking' + "\u00A0" + 'No.',
                template: 'link.html'
            },{
                title: 'Carrier',
                type: 'logo',
                logo: 'logo',
                logoTitle: 'titleLogo',
                container: 'logotype-shipping',
                template: 'logotype.html'
            },{
                name: 'status',
                title: 'Booking Status'
            }, {
                name: 'total',
                title: 'Amount (Ex' + "\u00A0" + 'GST)'
            }, {
                title: 'Actions',
                type: 'action'
            }
        ],
        'filters': true,
        'checked': true,
        'data': []
    };
    $scope.ordersTable.data = [
        {
            date: '19/06/2015',
            reciever: 'Freddy Craigster',
            consignment: '00192',
            customer: ['BBA819928199'],
            tracking: '123456890',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            status: 'Processed',
            total: '$70.00 AUD',
            actions: [
                {
                    type: 'btn-blue',
                    action: function(){},
                    text: 'Docs',
                    icon: 'view_icon'
                },
                {
                    type: 'btn-normal-blue',
                    action: function(){},
                    text: 'Edit/View',
                    icon: 'edit_icon'
                },
                {
                    type: 'btn-light-grey',
                    action: function(){},
                    text: false,
                    icon: 'print_icon'
                }
            ]
        },{
            date: '19/06/2015',
            reciever: 'Freddy Craigster',
            consignment: '00192',
            customer: ['BBA819928199'],
            tracking: '123456890',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            status: 'Processed',
            total: '$70.00 AUD',
            actions: [
                {
                    type: 'btn-blue',
                    action: function(){},
                    text: 'Docs',
                    icon: 'view_icon'
                },
                {
                    type: 'btn-normal-blue',
                    action: function(){},
                    text: 'Edit/View',
                    icon: 'edit_icon'
                },
                {
                    type: 'btn-light-grey',
                    action: function(){},
                    text: false,
                    icon: 'print_icon'
                }
            ]
        },{
            date: '19/06/2015',
            reciever: 'Freddy Craigster',
            consignment: '00192',
            customer: ['BBA819928199'],
            tracking: '123456890',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            status: 'Processed',
            total: '$70.00 AUD',
            actions: [
                {
                    type: 'btn-blue',
                    action: function(){},
                    text: 'Docs',
                    icon: 'view_icon'
                },
                {
                    type: 'btn-normal-blue',
                    action: function(){},
                    text: 'Edit/View',
                    icon: 'edit_icon'
                },
                {
                    type: 'btn-light-grey',
                    action: function(){},
                    text: false,
                    icon: 'print_icon'
                }
            ]
        },{
            date: '19/06/2015',
            reciever: 'Freddy Craigster',
            consignment: '00192',
            customer: ['BBA819928199'],
            tracking: '123456890',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            status: 'Processed',
            total: '$70.00 AUD',
            actions: [
                {
                    type: 'btn-blue',
                    action: function(){},
                    text: 'Docs',
                    icon: 'view_icon'
                },
                {
                    type: 'btn-normal-blue',
                    action: function(){},
                    text: 'Edit/View',
                    icon: 'edit_icon'
                },
                {
                    type: 'btn-light-grey',
                    action: function(){},
                    text: false,
                    icon: 'print_icon'
                }
            ]
        },{
            date: '19/06/2015',
            reciever: 'Freddy Craigster',
            consignment: '00192',
            customer: ['BBA819928199'],
            tracking: '123456890',
            logo: '/images/shipping_company_logos/couriers_please.svg',
            status: 'Processed',
            total: '$70.00 AUD',
            actions: [
                {
                    type: 'btn-blue',
                    action: function(){},
                    text: 'Docs',
                    icon: 'view_icon'
                },
                {
                    type: 'btn-normal-blue',
                    action: function(){},
                    text: 'Edit/View',
                    icon: 'edit_icon'
                },
                {
                    type: 'btn-light-grey',
                    action: function(){},
                    text: false,
                    icon: 'print_icon'
                }
            ]
        }
    ];
});