'use strict';

var module = angular.module('app');

module.controller('DashboardProfileController', function ($scope, $route, UsersTable) {

    var usersTable = new UsersTable();
    $scope.tableData = usersTable.getTableOptions();
    $scope.activeTab = $route.current.params.tab || 'company';
    if ($route.current.params.tab != 'marketplaces') {
        //$route.updateParams({
        //    service: undefined,
        //    id: undefined
        //});
    } else {
        if ($route.current.params.service) {
            $scope.buttonsTemplate = '/templates/dashboard/my-profile/marketplaces/connect-buttons.html';
            $scope.marketplaceTemplate = '/templates/dashboard/my-profile/marketplaces/connect.html';
            $scope.connectTemplate = $route.current.params.service;
        } else {
            $scope.marketplaceTemplate = '/templates/dashboard/my-profile/marketplaces.html';
        }
    }

}).controller('DashboardProfileCompanyController', function ($scope, UserService, $rootScope) {
    $scope.errors = {};
    $scope.saveUser = function(userForm) {
        $scope.errors.UserForm = false;
        if (userForm.$valid) {
            $scope.userFormProgress = true;
            var requestData = {};
            for (var i in $scope.profile) {
                switch (i) {
                    case 'roles':
                        break;
                    case 'location':
                        requestData['locationId'] = $scope.profile['location']['id'];
                        break;
                    default:
                        requestData[i] = $scope.profile[i];
                        break;
                }
            }

            UserService.saveUser(requestData, function (data) {
                $rootScope.GlobalProfile = angular.copy($scope.profile);
                $scope.userFormProgress = false;
                $scope.errors.UserForm = {
                    'code': 'Success',
                    'message': 'Data successfully updated',
                    'type': 'success'
                };
            }, function(errs) {
                $scope.errors.UserForm = errs;
                $scope.userFormProgress = false;
            });
        } else {
            $scope.errors.UserForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
        }
    };

    $scope.userLocationOptions = {};
    $scope.getUser = function () {
        $scope.userFormProgress = true;
        UserService.getUser(function (data) {
            $scope.profile = data;
            //$rootScope.GlobalProfile = data;
            $scope.profileLocation = angular.copy(data.location);
            $scope.userFormProgress = false;
        });
    };

    $scope.getCompany = function () {
        $scope.companyFormProgress = true;
        UserService.getCompany(function (data) {
            $scope.companyLocation = angular.copy(data.location);
            $scope.company = data;
            $scope.companyFormProgress = false;
        }, function (err) {
            console.log(err);
            $scope.companyFormProgress = false;
        });
    };
    $scope.saveCompany = function (companyForm) {
        $scope.errors.CompanyForm = false;
        if (companyForm.$valid) {
            $scope.companyFormProgress = true;
            var requestData = {};
            for (var i in $scope.company) {
                if (i != 'location') {
                    requestData[i] = $scope.company[i];
                } else {
                    requestData['locationId'] = $scope.company['location']['id'];
                }
            }
            UserService.saveCompany(requestData, function (data) {
                $scope.companyFormProgress = false;
                $scope.errors.CompanyForm = {
                    'code': 'Success',
                    'message': 'Data successfully updated',
                    'type': 'success'
                };
                $rootScope.GlobalProfile = $scope.profile;
            }, function (errs) {
                $scope.errors.CompanyForm = errs;
                $scope.companyFormProgress = false;
            });
        } else {
            companyForm.$setSubmitted();
            $scope.errors.CompanyForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
        }
    };
}).controller('DashboardProfilePasswordController', function ($scope, UserService) {
    $scope.password = {};
    $scope.changePassword = function (passwordForm) {
        $scope.errorsPassForm = false;
        if (passwordForm.$valid) {
            $scope.passwordFormProgress = true;
            UserService.setPassword($scope.password, function () {
                $scope.passwordFormProgress = false;
                $scope.errorsPassForm = {
                    'code': 'Success',
                    'message': 'Data successfully updated',
                    'type': 'success'
                };
            }, function (errs) {
                $scope.passwordFormProgress = false;
                $scope.errorsPassForm = errs;
            });
        } else {
            $scope.errorsPassForm = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
            passwordForm.$setSubmitted();
        }
    };
}).controller('DashboardProfileMarketplacesController', function ($scope, $route, $rootScope, $location, MarketplacesService) {

    $scope.activeTab = 'marketplaces';

    var marketplacesList = {
        'ebay': {
            logotype: 'ebay',
            noactive: true
        },
        'shopify': {
            logotype: 'shopify',
            noactive: true
        },
        'magento': {
            logotype: 'magento'
        },
        'bigcommerce': {
            logotype: 'bigcommerce',
            noactive: true
        },
        'rd3party': {
            logotype: '3rd_party',
            noactive: true
        }
    };

    $scope.outputListMarketplaces = [];


    var createMarketplaceList = function (marketplaces) {
        var forRowClass = false;
        var sortedMarketplaces = {};

        marketplaces.map(function (marketplace) {
            sortedMarketplaces[marketplace['marketplaceType']] = sortedMarketplaces[marketplace['marketplaceType']] || [];
            sortedMarketplaces[marketplace['marketplaceType']].push(marketplace);
        });


        for (var k in marketplacesList) {
            marketplacesList[k].marketplace = k;
            marketplacesList[k].rowType = !forRowClass ? 'odd' : 'even';
            forRowClass = !forRowClass;
            if (sortedMarketplaces && sortedMarketplaces[k]) {
                sortedMarketplaces[k].map(function (item, ind) {
                    item.marketplace = k;
                    item.rowType = marketplacesList[k]['rowType'];
                    if (!ind) {
                        item.logotype = marketplacesList[k]['logotype'];
                    }
                    $scope.outputListMarketplaces.push(item);
                });
                $scope.outputListMarketplaces.push({
                    rowType: marketplacesList[k]['rowType'],
                    count: sortedMarketplaces[k].length,
                    marketplace: k
                });
            } else {
                $scope.outputListMarketplaces.push(marketplacesList[k]);
            }
        }
    };

    //createMarketplaceList([]);
    $scope.inProgressLoading = true;
    MarketplacesService.getList(function (data) {
        createMarketplaceList(data.content);
        $scope.inProgressLoading = false;
    }, function () {
        console.log(arguments);
        $scope.inProgressLoading = false;
    });

    $scope.toConnect = function (service, id) {
        if (!service) return;
        var params = {
            service: service
        };
        if (id) {
            params.id = id;
        }
        $route.updateParams(params);
    };
});


