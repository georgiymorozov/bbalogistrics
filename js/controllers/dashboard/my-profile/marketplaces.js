'use strict';

// Marketplace, Name, Verified, Enabled, Actions

var module = angular.module('app');

module.controller('DashboardProfileMarketplacesConnectController', function ($scope, MarketplacesService, Windows, $route, $location) {

    $scope.connectionData = {
        "marketplaceType": $route.current.params.service
    };

    $scope.connectMarketplace = function (connectForm) {
        if (!connectForm.$valid) {
            $scope.connectFormErrors = {
                'code': $scope['languageLibrary']['error_title_uncorrected'],
                'message': 'Please ﬁx the below and try again'
            };
            connectForm.$setSubmitted();
            return;
        }
        $scope.dataTransferProgress = true;
        MarketplacesService[$scope.connectionData.id ? 'updateMarketplace' : 'createMarketplace'](
            $scope.connectionData,
            onCreateMarketplace,
            function (err) {
                $scope.dataTransferProgress = false;
                console.log(err);
            }
        );
    };
    $scope.verifInProgress = false;
    $scope.verificationMarketplace = function (marketplace) {
        $scope.verificationInProgress = true;
        MarketplacesService.verificationMarketplace(marketplace.id,
            function (data) {
                verificationMarketplace(data, marketplace);
            },
            function (err) {
                $scope.verificationInProgress = false;
                console.log(err);
            }
        );
    };


    $scope.enableInProgress = false;
    $scope.disableMarketplace = function (id) {
        $scope.enableInProgress = true;
        MarketplacesService.disableMarketplace(
            id,
            function (data) {
                $scope.enableInProgress = false;
                $scope.connectionData = data;
            },
            function (err) {
                $scope.enableInProgress = false;
            }
        );
    };


    $scope.enableMarketplace = function (id) {
        $scope.enableInProgress = true;
        MarketplacesService.enableMarketplace(
            id,
            function (data) {
                $scope.enableInProgress = false;
                $scope.connectionData = data;
            },
            function (err) {
                $scope.enableInProgress = false;
            }
        );
    };


    $scope.verificationCode = '';
    var verificationMarketplace = function (info, marketplace) {
        info = info || {};
        $scope.verificationInProgress = false;
        Windows.prompt({
            title: 'Marketplace Verification',
            text: "To continue with the a trading platform must be verified.\nFollow the link below for the verification code",
            advanced: info.authorizationUrl ? angular.element(
                '<div class="window-advanced-content">' +
                '<a href="' + info.authorizationUrl + '" class="bold" target="_blank">' + info.authorizationUrl + '</a>' +
                '</div>'
            ) : false,
            field: {
                regexp: /[\S]+/,
                required: true,
                label: 'Verification code'
            },
            onConfirm: function (field, cb) {
                if (!field.regexp || (field.regexp.test(field.input.val()))) {
                    field.input.removeClass('response-error');
                    $scope.verificationInProgress = true;
                    cb();
                    MarketplacesService.completeVerificationMarketplace(
                        marketplace.id,
                        field.input.val(),
                        function (data) {
                            $scope.connectionData = data;
                            $scope.verificationInProgress = false;
                        },
                        function () {
                            $scope.verificationInProgress = false;
                        }
                    );
                } else {
                    field.input.addClass('response-error');
                }
                $scope.$apply();
                return false;
            },
            onClose: function () {
                $scope.verificationInProgress = false;
                $scope.$apply();
            },
            confirmText: 'Verification',
            cancelText: 'Cancel'
        });
    };
    var onCreateMarketplace = function (marketplace) {
        $scope.dataTransferProgress = false;
        if (!$scope.connectionData.id) {
            $route.updateParams({
                id: marketplace.id
            });
        } else {
            $scope.connectionData = marketplace;
        }
    };

    $scope.deleteProgress = false;
    $scope.deleteMarketplace = function (marketplace) {
        Windows.confirm({
            title: 'Marketplace connect',
            text: "You really want to delete marketplace \"" + marketplace.name + "\" ?",
            onConfirm: function () {
                $scope.deleteProgress = true;
                MarketplacesService.deleteMarketplace(
                    marketplace.id,
                    function (response) {
                        $scope.deleteProgress = false;
                        $location.url('/dashboard/my-profile/marketplaces');
                    }, function () {
                        $scope.deleteProgress = false;
                    });
            },
            onClose: function () {

            },
            confirmText: 'Yes, Delete',
            cancelText: 'No, Cancel'
        });
    };

    $scope.dataTransferProgress = false;
    if ($route.current.params.id) {
        $scope.dataTransferProgress = true;
        MarketplacesService.getMarketplace(
            $route.current.params.id,
            function (marketplace) {
                $scope.dataTransferProgress = false;
                $scope.connectionData = marketplace;
            }
        );
    }
}).controller('DashboardProfileMarketplacesMagentoController', function ($scope) {

});