'use strict';

var module = angular.module('app');

module.controller('ReportingController', function ($scope, $location, ShipmentsTable) {

    var shipmentsTableObject = new ShipmentsTable();
    $scope.tableData = shipmentsTableObject.getTableOptions({
        order: [
            'date', 'bookingStatus', 'bbaReferences', 'references', 'consignment', 'sscc', 'trackingNumber', 'carrier', 'buttons'
        ],
        table: {
            noFilters: true
        },
        cellsOptions: {
            buttons: {
                bodyButtons: [
                    {
                        type: 'btn-normal-blue',
                        action: function (data) {
                        },
                        text: 'View',
                        icon: 'view_icon'
                    }, {
                        type: 'btn-orange',
                        action: function (data) {
                        },
                        icon: 'tracking_icon',
                        text: 'Tracking'
                    }, {
                        type: 'btn-light-grey',
                        action: function (data) {
                        },
                        icon: 'print_icon'
                    }
                ]
            }
        }
    });


    
    /** Monthly chart configuration **/

    $scope.monthlyChart = {};
    $scope.monthlyChartData = {
        labels: ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"],
        datasets: [
            {
                label: 'Amount',
                backgroundColor: 'rgba(0, 133, 194, 1)',
                animateScale: true,

                pointRadius: 2,
                pointBackgroundColor: "#c0dce1",
                pointBorderColor: "#ffb000",
                pointBorderWidth: 2,

                pointHoverRadius: 8,
                pointHoverBackgroundColor: "#ffb000",
                pointHoverBorderColor: "#c0dce1",
                pointHoverBorderWidth: 2,

                pointHitRadius: 10,
                data: [65, 59, 80, 81, 56, 55, 40, 65, 23, 65, 98, 23]
            }
        ]
    };


    /** Days chart configuration **/

    $scope.daysChart = {
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        },
        legend: {
            display: true
        }
    };

    $scope.daysChartData = {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
        datasets: [
            {
                label: 'BBA Profile 1',
                backgroundColor: 'rgba(0, 133, 194, 1)',
                data: [15, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 65, 59, 80, 81, 56, 55, 40, 54, 12, 54]
            },
            {
                label: 'BBA Profile 2',
                backgroundColor: 'rgba(2, 99, 181, 1)',
                data: [28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86, 27, 90, 28, 48, 40, 19, 86, 27, 90, 54, 20, 45]
            },
            {
                label: 'DHL',
                backgroundColor: 'rgba(252, 180, 27, 1)',
                data: [34, 21, 54, 76, 12, 65, 23, 76, 98, 23, 45, 23, 56, 98, 23, 65, 25, 42, 76, 23, 87, 23, 65, 87, 23, 98, 23, 52, 23, 64, 65]
            }
        ]
    };


    /** Spend by Carrier Charts configuration **/

    $scope.carriersChartData = {
        labels: [
            "Carrier #1",
            "Carrier #2",
            "Carrier #3",
            "Carrier #4"
        ],
        datasets: [
            {
                data: [300, 50, 100, 45],
                backgroundColor: [
                    "#badbe4",
                    "#0263b5",
                    "#fcb41b",
                    "#ec1c24"
                ]
            }]
    };


    /** Difot Chart configuration **/

    $scope.difotChartData = {
        labels: [
            "Blue",
            "Red"
        ],
        datasets: [
            {
                data: [300, 50],
                backgroundColor: [
                    "#0263b5",
                    "#ec1c24"
                ]
            }]
    };
});
