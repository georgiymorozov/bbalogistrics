'use strict';

var module = angular.module('app');

module.controller('PendingShipmentsSplitController', function($scope) {
    $scope.splitShipmentsTable = {
        'head': [{
                name: 'customer',
                title: 'Customer Ref' + "\u00A0" + 'No.',
                template: 'link.html'
            },{
                name: 'platform',
                title: 'Platform',
                type: 'logo',
                logo: 'logoPlatform',
                sort: 'name',
                logoTitle: 'titleLogo',
                template: 'logotype.html',
                container: 'logotype-company magento'
            },
            {
                name: 'carrier',
                title: 'Carrier',
                type: 'logo',
                logo: 'logoCarrier',
                sort: 'name',
                logoTitle: 'titleLogo',
                container: 'logotype-shipping',
                template: 'logotype.html'
            },{
                name: 'service',
                title: 'Service'
            },{
                name: 'packaging',
                title: 'Packaging'
            },{
                name: 'awaitingBooking',
                title: 'Awaiting Booking'
            }, {
                name: 'total',
                title: 'Amount (Ex' + "\u00A0" + 'Tax)'
            }, {
                title: 'Actions',
                type: 'action'
            }
        ],
        'filters': true,
        'checked': true,
        'data': []
    };
    $scope.splitShipmentsTable.data = [
        {
            customer: 'MAG1234568910',
            logoPlatform: '/images/associated_company_logos/magento.svg',
            logoCarrier: '/images/shipping_company_logos/couriers_please.svg',
            service: 'Road Express',
            packaging: "Package 1 x2\n30 x 40 x 20",
            awaitingBooking: 'Yes',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function(){},
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function(){},
                    text: 'Delete'
                }
            ]
        },
        {
            customer: 'MAG1234568910',
            logoPlatform: '/images/associated_company_logos/magento.svg',
            logoCarrier: '/images/shipping_company_logos/couriers_please.svg',
            service: 'Road Express',
            packaging: "Package 1 x2\n30 x 40 x 20",
            awaitingBooking: 'Yes',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function(){},
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function(){},
                    text: 'Delete'
                }
            ]
        },
        {
            customer: 'MAG1234568910',
            logoPlatform: '/images/associated_company_logos/magento.svg',
            logoCarrier: '/images/shipping_company_logos/couriers_please.svg',
            service: 'Road Express',
            packaging: "Package 1 x2\n30 x 40 x 20",
            awaitingBooking: 'Yes',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function(){},
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function(){},
                    text: 'Delete'
                }
            ]
        }
    ];
});