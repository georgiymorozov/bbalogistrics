'use strict';

var module = angular.module('app');

module.controller('PendingShipmentsProcessController', function($scope, $location) {
    $scope.pendingShipmentsTable = {
        'head': [
            {
                name: 'references',
                title: 'BBA Ref' + "\u00A0" + 'No.',
                template: 'link.html'
            },{
                name: 'customer',
                title: 'Customer Ref' + "\u00A0" + 'No.',
                template: 'array-links.html'
            },{
                name: 'shipmentsNo',
                title: 'No' + "\u00A0" + 'of' + "\u00A0" + 'Shipments'
            },
            {
                name: 'readyStatus',
                title: 'Ready' + "\u00A0" + 'to' + "\u00A0" + 'batch' + "\u00A0" + 'print'
            },{
                name: 'orderStatus',
                title: 'Order Status'
            }, {
                name: 'total',
                title: 'Amount (Ex' + "\u00A0" + 'Tax)'
            }, {
                title: 'Actions',
                type: 'action'
            }
        ],
        'filters': true,
        'checked': true,
        'data': []
    };
    $scope.pendingShipmentsTable.data = [
        {
            references: 'BBA 12348',
            customer: [
                'MAG1234568910',
                'MAG1234568914',
                'MAG1234578910'
            ],
            shipmentsNo: '1/1',
            readyStatus: 'Yes',
            orderStatus: 'Processed',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function() {
                        $location.url("/pending-shipments/booking");
                    },
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function() {

                    },
                    text: 'Delete'
                }
            ]
        },
        {
            references: 'BBA 12348',
            customer: [
                'MAG1234568910',
                'MAG1234568914',
                'MAG1234578910'
            ],
            shipmentsNo: '1/1',
            readyStatus: 'Yes',
            orderStatus: 'Processed',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function() {
                        $location.url("/pending-shipments/booking");
                    },
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function() {

                    },
                    text: 'Delete'
                }
            ]
        },
        {
            references: 'BBA 12348',
            customer: [
                'MAG1234568910',
                'MAG1234568914',
                'MAG1234578910'
            ],
            shipmentsNo: '1/1',
            readyStatus: 'Yes',
            orderStatus: 'Processed',
            total: '$11.00 (multi-currency)',
            actions: [
                {
                    type: 'btn-blue',
                    icon: 'edit_icon',
                    action: function() {
                        $location.url("/pending-shipments/booking");
                    },
                    text: 'Edit/View'
                },
                {
                    type: 'btn-red',
                    action: function() {

                    },
                    text: 'Delete'
                }
            ]
        }
    ];
});