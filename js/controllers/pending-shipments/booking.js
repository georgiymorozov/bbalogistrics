'use strict';

var module = angular.module('app');

module.controller('PendingShipmentsBookingController', function ($scope, PackagesService) {
    $scope.quotes = {
        'head': [
            {
                title: 'Company',
                type: 'logo',
                logo: 'logo',
                logoTitle: 'titleLogo',
                container: 'logotype-shipping',
                template: 'logotype.html'
            }, {
                name: 'service',
                title: 'Service'
            }, {
                name: 'transit',
                title: 'Transit'
            }, {
                name: 'price',
                title: 'Amount Ex' + "\u00A0" + 'Tax'
            }, {
                title: 'Selection',
                type: 'action'
            }
        ],
        'checked': false,
        'filters': false,
        'nosort': true,
        'nopaging': true,
        'data': []
    };
    $scope.quotes.data = [
        {
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        service: 'Simple Service',
        transit: 'Road Express 1-3 days',
        price: '$10 AUD',
        actions: [
            {
                type: 'btn-normal-blue',
                action: function(){},
                text: 'Select'
            }
        ]
    }, {
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        service: 'Simple Service',
        transit: 'Road Express 1-3 days',
        price: '$10 AUD',
        actions: [
            {
                type: 'btn-normal-blue',
                action: function(){},
                text: 'Select'
            }
        ]
    }, {
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        service: 'Simple Service',
        transit: 'Road Express 1-3 days',
        price: '$10 AUD',
        actions: [
            {
                type: 'btn-normal-blue',
                action: function(){},
                text: 'Select'
            }
        ]
    }, {
        logo: '/images/shipping_company_logos/couriers_please.svg',
        titleLogo: 'Simple Carrier',
        service: 'Simple Service',
        transit: 'Road Express 1-3 days',
        price: '$10 AUD',
        actions: [
            {
                type: 'btn-normal-blue',
                action: function(){},
                text: 'Select'
            }
        ]
    }];

    $scope.loadedPackages = false;
    $scope.packagesList = [];
    var removedPackages = [];
    $scope.packages = [];
    $scope.Units = UNITS.combined;
    $scope.Units.map(function(item, i) {
        item.id = i;
    });

    var taxes = ['Tax 1', 'Tax 2', 'Tax 3'];
    var measures = ['Inches', 'Centimeters'];
    var categories = ['Auto', 'Toys', 'Business'];
    var types = ['Normal', 'Big', 'Small'];

    $scope.itemsTable = {
        'head': [
            {
                title: 'Description',
                type: 'template',
                name: 'description',
                template: 'product-item.html',
                measures: measures,
                taxes: taxes,
                categories: categories,
                types: types,
                selectClass: 'select-2'
            }, {
                nofilter: true,
                nosort: true,
                title: 'Actions',
                template: 'buttons-array.html',
                type: 'action',
                actions: [
                    /*{
                     type: 'btn-red',
                     action: deleteInventory,
                     text: 'Delete Selected'
                     }*/
                ],
                buttons: [
                    {
                        type: 'btn-red block width-100',
                        text: 'Delete',
                        action: function(obj) {
                            if ($scope.itemsTable.data.length <= 1) return;
                            obj.description.deleted = true;
                            removedItems.push(obj);

                            $scope.itemsTable.data = $scope.itemsTable.data.filter(function(obj) {
                                return !obj.description.deleted;
                            });
                        }
                    }, {
                        type: 'btn-dark-blue block width-100',
                        text: 'Exclude',
                        action: function(){}
                    }
                ]
            }
        ],
        'checked': true,
        'filters': false,
        'nosort': true,
        'nopaging': true,
        'data': []
    };
    var removedItems = [];
    $scope.addItem = function() {
        var arr = [];
        $scope.itemsTable.data.map(function(i) {
            arr.push(i);
        });

        arr.push(removedItems.length ? removedItems.pop() : {
            description: {
                name: 'Rebel Team Black Small',
                quantity: '1',
                value: '$54.99',
                taxes: taxes[0],
                sku: 'SKU / Part # (GTIN)',
                country: 'Australia (AU)',
                weight: '0.49',
                total: '$54.99',
                type: types[0],
                measure: measures[0],
                length: '10',
                width: '10',
                height: '15',
                category: categories[0],
                code: '00111001D00110011'
            }
        });
        arr.deleted = false;
        $scope.itemsTable.data = arr;

    };
    $scope.addItem();

    $scope.onInitPackages = function() {
        PackagesService.getList(function (data) {
            $scope.packagesList = $scope.packagesList.concat(data);
            $scope.loadedPackages = true;
            createPackage();
        });
    };

    var createPackage = function() {
        var customItem = {
            "measure": $scope.Units[0],
            "name": 'Custom',
            "custom": true,
            "id": 0
        };
        var newPackage = {};
        newPackage.list = [customItem].concat($scope.packagesList);
        if (removedPackages.length) {
            newPackage = removedPackages.pop();
        }
        $scope.packages.push(newPackage);
        newPackage.selected = newPackage.list[0];
    };

    $scope.addPackage = function(packagesForm) {
        if (!packagesForm.$valid) return;
        createPackage();
        packagesForm.$setPristine();
        return false;
    };

    $scope.removePackage = function() {
        if ($scope.packages.length > 1) {
            removedPackages.push($scope.packages.pop());
        }
    };


    $scope.collectionAddress = {};
    $scope.collectionAddressLocationOptions = {};

    $scope.deliveryAddress = {};
    $scope.deliveryAddressLocationOptions = {};
});