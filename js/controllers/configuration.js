'use strict';

var module = angular.module('app');

module.controller('ConfigurationController', function($scope, $location) {
    $scope.toChapter = function(chapter) {
        $location.path('/configuration/' + chapter);
    };
});