angular.module('Directives')
    .directive('ngSortableTable', function() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/table.html',
            replace: true,
            scope: {
                table: '=tableData'
            },
            controller: function ($scope) {

                $scope.templatesPath = '/templates/directives/table-templates/';
                $scope.templateDefault = 'text.html';
                $scope.templateActions = 'actions.html';

                var lastSelectedCount = 10;
                $scope.selectedArray = [];

                var resetSelected = function() {
                    $scope.selectedArray.map(function(item) {
                        item.selected = false;
                    });
                    $scope.selectedArray = [];
                    $scope.checkedRows = false;
                };

                $scope.resetList = function() {
                    $scope.page = 1;
                    $scope.maxPage = 0;
                    $scope.sorting = {};
                    $scope.resetCurrentParams();
                };

                $scope.resetCurrentParams = function() {
                    $scope.filters = {};
                    resetSelected();
                    $scope.updateList();
                    applySort();
                    correctPageNumber();
                };

                $scope.updateList = function() {
                    $scope.originalList = [];
                    $scope.table.data.map(function(item) {
                        $scope.originalList.push(item);
                    });
                    $scope.filteredList = $scope.dataArray = $scope.originalList;
                    correctPageNumber();
                };

                var correctPageNumber = function() {
                    lastSelectedCount = Math.max(lastSelectedCount, 1);
                    $scope.count = Math.min(lastSelectedCount, $scope.filteredList.length);
                    $scope.maxPage = $scope.count ? Math.ceil($scope.filteredList.length / $scope.count) : 1;
                    $scope.page = Math.min(Math.max(1, $scope.page), $scope.maxPage);
                    var k = $scope.page * $scope.count;
                    $scope.dataArray = $scope.filteredList.slice(
                        k - $scope.count,
                        Math.min(k, $scope.filteredList.length));
                };

                $scope.pageUp = function() {
                    $scope.page++;
                    correctPageNumber();
                };
                $scope.pageDown = function() {
                    $scope.page--;
                    correctPageNumber();
                };
                $scope.countUp = function() {
                    $scope.count++;
                    lastSelectedCount = $scope.count;
                    correctPageNumber();
                };
                $scope.countDown = function() {
                    $scope.count--;
                    lastSelectedCount = $scope.count;
                    correctPageNumber();
                };


                $scope.applyFilters = function() {
                    $scope.filteredList = $scope.originalList.filter(function(item) {
                        for (var i in $scope.filters) {
                            var filter = $scope.filters[i];
                            if (filter.length) {
                                var regex = new RegExp('', 'i');
                                if (!regex.test(item[i])) {
                                    return false;
                                }
                            }
                        }
                        return true;
                    });
                    correctPageNumber();
                };
                $scope.resetFilters = function() {
                    $scope.filters = {};
                    $scope.applyFilters();
                };
                var applySort = function(filter) {
                    filter = filter || $scope.sorting['name'];
                    if ($scope.sorting['name']) {
                        $scope.sorting['name'] = filter;
                        $scope.originalList.sort(function(a, b) {
                            return (a[filter] > b[filter] ? 1 : -1) * ($scope.sorting['ASC'] ? -1: 1);
                        });
                    } else {
                        $scope.originalList = [];
                        $scope.table.data.map(function(item) {
                            $scope.originalList.push(item);
                        });
                    }
                };

                $scope.applySort = function(filter) {
                    if (filter != $scope.sorting['name']) {
                        $scope.sorting = {
                            name: filter,
                            ASC: false
                        };
                    } else {
                        if ($scope.sorting['ASC']) {
                            $scope.sorting = {};
                        } else {
                            $scope.sorting['ASC'] = !$scope.sorting['ASC'];
                        }
                    }
                    applySort();
                    $scope.applyFilters();
                };

                $scope.resetList();
                $scope.table = $scope.table || {};
                $scope.table.head = $scope.table.head || [];

                $scope.table.head.map(function(item) {
                    item.sort = item.sort || item.name || false;
                    if (item.type != 'action') {
                        $scope.filters[item.name] = '';
                    }
                });

                $scope.checkedRows = false;

                $scope.checkSelected = function(a) {
                    $scope.checkedRows = !$scope.checkedRows;
                    if ($scope.checkedRows) {
                        $scope.selectedArray = $scope.dataArray;
                        $scope.dataArray.map(function (item) {
                            item.selected = true;
                        });
                    } else {
                        resetSelected();
                    }
                };

                $scope.onCheck = function(item) {
                    item.selected = !(item.selected || false);
                    $scope.selectedArray = $scope.dataArray.filter(function(item) {
                        return item.selected;
                    });
                };

                $scope.$watch('dataArray', function() {
                    $scope.selectedArray.map(function(item) {
                        if ($scope.dataArray.indexOf(item) == -1) {
                            item.selected = false;
                        }
                    });
                    $scope.selectedArray = $scope.dataArray.filter(function(item) {
                        return item.selected;
                    });
                });

                $scope.$watch('selectedArray', function() {
                    var identical = true;
                    for (var k = 0; k < $scope.dataArray.length; k++) {
                        var item = $scope.dataArray[k];
                        if ($scope.selectedArray.indexOf(item) == -1) {
                            identical = false;
                        }
                    }
                    $scope.checkedRows = identical;
                });

            },
            link: function($scope) {
                $scope.$watch('table.data', function() {
                    $scope.updateList();
                });
            }
        }
    });