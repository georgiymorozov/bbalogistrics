var module = angular.module('Directives');
module.directive('tabSection', function ($timeout) {
    return {
        'restrict': 'C',
        'link': function linkingFunction($scope, element, attr) {
            var tabLine = angular.element('.tab-section-line:first', element);
            var topMargin = angular.element('.top-bar:first').outerHeight(true);
            var win = angular.element(window);
            win.on('scroll', function () {
                var offset = element.offset()['top'] - win.scrollTop() - topMargin;
                if (offset < 0) {
                    tabLine.addClass('fixed-page-container');
                    element.addClass('tab-line-scrolled');
                } else {
                    tabLine.removeClass('fixed-page-container');
                    element.removeClass('tab-line-scrolled');
                }
            });
        },
        controller: function ($scope) {

        }
    }
});