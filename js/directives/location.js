angular.module('Directives')
    .directive('ngLocation', function() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/location.html',
            replace: true,
            scope: {
                options: '=?ngParams',
                ngModel: '=?ngModel',
                readonly: '=?ngReadonly'
            },
            controller: function ($scope, LocationService) {
                $scope.citySearchParams = false;
                $scope.countryModel = false;
                $scope.codeModel    = false;
                $scope.cityModel = false;
                $scope.stateModel = false;

                if ($scope.readonly) return;

                var onFocusCity = function () {
                    if (!$scope.countryModel) {
                        $scope.toCountryAutoFocus = !$scope.toCountryAutoFocus;
                    }
                };

                var resetLocation = function () {

                    $scope.stateAutocompleteOptions.localList = false;
                    $scope.codeAutocompleteOptions.localList = false;
                    $scope.cityAutocompleteOptions.localList = false;

                    $scope.codeModel = false;
                    $scope.cityModel = false;

                    $scope.stateAutocompleteOptions.placeholder =
                        !$scope.countryModel ?
                            'Enter the country location...' :
                            'Enter the city name or the postcode';
                };
                // Country
                var onChangeCountry = function(country) {
                    $scope.countryModel = country;
                    if (!(country && $scope.ngModel && (country.code == $scope.ngModel.country.code))) {
                        resetLocation();
                    }
                    $scope.options && $scope.options.onChangeCountryModelCallback ?
                        $scope.options.onChangeCountryModelCallback(country) :
                        false;
                    $scope.citySearchParams = country ? {
                        country: country.code
                    } : false;

                    $scope.codeAutocompleteOptions.disabled =
                        $scope.cityAutocompleteOptions.disabled = country ?
                            false : true;

                    if (country) {
                        $scope.stateAutocompleteOptions.placeholder = 'Enter the city name or the postcode';
                        $scope.cityAutocompleteOptions.placeholder = 'Enter the city location...';
                        $scope.codeAutocompleteOptions.placeholder = 'Enter the code location...';
                    } else {
                        $scope.stateAutocompleteOptions.placeholder =
                            $scope.cityAutocompleteOptions.placeholder =
                                $scope.codeAutocompleteOptions.placeholder = 'Enter the country location...';
                    }

                };


                var onChangeLocation = function (location) {

                    if (!location) {
                        resetLocation();
                        $scope.options && $scope.options.onChangeModelCallback ? $scope.options.onChangeModelCallback(false) : false;
                        return;
                    }

                    location.stateName = location.stateName || (location.state ? location.state.name : 'Unavailable');
                    location.codeName = location.code || 'Unavailable';

                    /** Set baseModel **/
                    if (!($scope.ngModel && ($scope.ngModel.id == location.id))) {
                        $scope.options && $scope.options.onChangeModelCallback ? $scope.options.onChangeModelCallback(LocationService.correctionLocation(location)) : false;
                    }

                    /** Set countryModel & countryList **/
                    if (!($scope.countryAutocompleteOptions.localList && $scope.countryAutocompleteOptions.localList.filter(function (item) {
                            return item.id == location.country.id;
                        }).length)) {
                        $scope.countryAutocompleteOptions.localList = [location.country];
                        $scope.countryModel = location.country;
                    }

                    /** Set stateModel & stateList **/
                    $scope.stateAutocompleteOptions.localList = location['states'] || $scope.stateAutocompleteOptions.localList;

                    /** Set cityModel & cityList **/
                    if (!($scope.cityAutocompleteOptions.localList && $scope.cityAutocompleteOptions.localList.filter(function (item) {
                            if (item.name == location.name) {
                                $scope.cityModel = item;
                                return true;
                            }
                        }).length)) {
                        $scope.cityAutocompleteOptions.localList = location['cities'] || [location];
                        $scope.cityModel = $scope.cityAutocompleteOptions.localList[0];
                    }

                    /** Set codeModel & codeList **/
                    if (!($scope.codeAutocompleteOptions.localList && $scope.codeAutocompleteOptions.localList.filter(function (item) {
                            if (item.code == location.code) {
                                $scope.codeModel = item;
                                return true;
                            }
                        }).length)) {
                        $scope.codeAutocompleteOptions.localList = location['codes'] || [location];
                        $scope.codeModel = $scope.codeAutocompleteOptions.localList[0];
                    }
                };


                $scope.countryAutocompleteOptions = {
                    placeholder: 'Enter the country location',
                    textField: 'name',
                    searchField: 'name',
                    service: 'Country',
                    fieldName: 'Country',
                    required: true,
                    onChange: onChangeCountry
                };
                $scope.stateAutocompleteOptions = {
                    placeholder: 'Enter the country location...',
                    textField: 'stateName',
                    fieldName: 'State',
                    disabled: true
                };
                $scope.cityAutocompleteOptions = {
                    placeholder: 'Enter the country location...',
                    textField: 'name',
                    searchField: 'name',
                    minQueryStringLength: 3,
                    disabled: true,
                    service: 'City',
                    fieldName: 'Suburb',
                    required: true,
                    onChange: onChangeLocation,
                    onFocus: onFocusCity
                };
                $scope.codeAutocompleteOptions = {
                    placeholder: 'Enter the country location...',
                    textField: 'codeName',
                    searchField: 'code',
                    minQueryStringLength: 3,
                    disabled: true,
                    service: 'City',
                    required: true,
                    fieldName: 'Postcode',
                    onChange: onChangeLocation,
                    onFocus: onFocusCity
                };
                $scope.$watch('ngModel', onChangeLocation);
            }
        }
    });