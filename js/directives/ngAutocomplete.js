angular.module('Directives')
    .directive('ngAutocomplete', function() {
        return {
            restrict: 'EA',
            templateUrl: '/templates/directives/autocomplete.html',
            replace: true,
            scope: {
                ngModel: '=?ngModel',
                request: '=ngAutocompleteRequest',
                ngAutocomplete: '=ngAutocomplete',
                autoFocus: '=ngFieldFocus'
            },
            controller: function (LocationService, $scope, $timeout, $q) {

                var aborter, timer;
                $scope.ngModel = false;
                $scope.timeuot = $timeout;

                /** Reset request timer **/
                $scope.cancelTimeout = function() {
                    timer ? $timeout.cancel(timer) : false;
                    timer = false;
                };

                /** Reset http request **/
                $scope.abortRequest = function() {
                    aborter ? aborter.resolve() : false;
                    $scope.inProgress = false;
                };

                /** Send Request function **/

                $scope.getResult = function(cb) {
                    if (!$scope.search || (!$scope.search.length || ($scope.ngAutocomplete.minQueryStringLength &&
                        ($scope.search.length < $scope.ngAutocomplete.minQueryStringLength)))
                    ) return;

                    var data = $scope.request ? angular.copy($scope.request) : {};
                    data[$scope.ngAutocomplete.searchField] = $scope.search;

                    timer = $timeout(function() {
                        timer = false;
                        aborter = $q.defer();
                        $scope.inProgress = true;
                        LocationService[$scope.ngAutocomplete.service](data,
                            function(response) {
                                if (!response.length) {
                                    var defaultItem = {
                                        disabled: true
                                    };
                                    defaultItem[$scope.ngAutocomplete.textField] = 'No search result';
                                    response = [defaultItem];
                                }

                                aborter = false;
                                $scope.inProgress = false;
                                cb(response);

                            }, function(err) {
                                aborter = false;
                                $scope.inProgress = false;
                                var defaultItem = {
                                    disabled: true
                                };
                                if (err.number == 500) {
                                    defaultItem[$scope.ngAutocomplete.textField] = 'Error. Try again';
                                    var response = [defaultItem];
                                    cb(response);
                                }
                            }, aborter
                        );
                    }, 200);
                };
            },

            link: function($scope, element, attrs, ngModelCtrl) {

                var select = angular.element('select:first', element);
                var input = angular.element('input:first', element);

                /** Select initialization **/
                select.select(false, {
                    maxSize: 5,
                    class: $scope.ngAutocomplete.selectClass
                });

                /** Set new select list to autocomplete **/
                var setList = function(list) {
                    if ($scope.ngAutocomplete && (list != $scope.ngAutocomplete.localList)) {
                        $scope.ngAutocomplete.localList = list;
                    }
                    var newItemsList = [];
                    list.map(function(item) {
                        var itemObject = {};
                        itemObject[$scope.ngAutocomplete.textField] = item[$scope.ngAutocomplete.textField];
                        if (!item.disabled) {
                            itemObject.model = item;
                        }
                        newItemsList.push(itemObject);
                    });
                    $scope.optionsList = newItemsList;
                    if (newItemsList.length) {
                        $scope.ngModel = newItemsList[0]['model'];
                    }
                    $scope.timeuot(function() {
                        input.is(':focus') ?
                            select.select('showList') :
                            false;
                        select.select('refresh');
                    });
                };

                /** Search Start Method **/
                $scope.startSearch = function() {

                    $scope.cancelTimeout();
                    $scope.abortRequest();
                    $scope.inProgress = false;

                    select.select('hideList');
                    $scope.optionsList = [];
                    $scope.ngModel = false;
                    select.select('refresh');
                    $scope.getResult(setList);
                };

                /** ngModel watcher **/
                var initialized = false;
                $scope.$watch('ngModel', function(a, b) {
                    if (!initialized) {
                        initialized = true;
                        return;
                    }
                    select.select('refresh');
                    if ($scope.ngModel) {
                        if (!input.is(':focus')) {
                            $scope.search = $scope.ngModel[$scope.ngAutocomplete.textField];
                        } else {
                            $scope.search = $scope.search ?
                                $scope.ngModel[$scope.ngAutocomplete.textField].substr(0, $scope.search.length) :
                                $scope.ngModel[$scope.ngAutocomplete.textField];
                        }
                    } else {
                        if (!input.is(':focus')) {
                            $scope.search = '';
                        }
                    }

                    if ($scope.ngAutocomplete.onChange) {
                        $scope.ngAutocomplete.onChange($scope.ngModel);
                    }
                });

                /** Select and search field Events callbacks **/

                $scope.blurAutoComplete = function () {
                    $scope.abortRequest();
                    if (!$scope.ngModel) {
                        $scope.search = '';
                    }
                    input.val($scope.ngModel ? $scope.ngModel[$scope.ngAutocomplete.textField] : '');
                    select.select('hideList');
                };

                $scope.onFocus = function () {
                    if ($scope.ngModel) {
                        var txt = $scope.ngModel[$scope.ngAutocomplete.textField];
                        input.val($scope.search && input.is(':focus') ? txt.substr(0, $scope.search.length) : txt);
                        if ($scope.optionsList.length) {
                            select.select('refresh');
                        }
                        if ($scope.optionsList.length > 1) {
                            select.select('showList');
                        }
                    }
                    $scope.ngAutocomplete.onFocus ? $scope.ngAutocomplete.onFocus() : false;
                };

                input.on({
                    keydown: function (e) {
                        var key = e.which;
                        var index = 0;
                        $scope.optionsList.map(function (it, ind) {
                            index = it.model == $scope.ngModel ? ind : index;
                        });
                        var oldIndex = index;

                        switch (key) {
                            case 38:
                                e.preventDefault();
                                index--;
                                break;
                            case 40:
                                e.preventDefault();
                                index++;
                                break;
                            case 13:
                                e.preventDefault();
                                select.select('hideList');
                                return;
                                break;
                            default:
                                return;
                                break;
                        }
                        if ($scope.optionsList.length) {
                            index = index > $scope.optionsList.length - 1 ? 0 : index < 0 ? $scope.optionsList.length - 1 : index;
                            var selected = $scope.optionsList[index];
                            $scope.ngModel = selected['model'];
                            if (!$scope.search) {
                                input.val(selected[$scope.ngAutocomplete.textField]);
                            }
                            $scope.$apply();
                            if (oldIndex != index && $scope.optionsList.length > 1) {
                                select.select('apply');
                                select.select('showList');
                            }
                        }
                    }
                });

                $scope.$watch('ngAutocomplete.localList', function() {
                    select.select('hideList');
                    $scope.optionsList = [];
                    $scope.ngModel = false;
                    if (!$scope.ngAutocomplete.localList) {
                        select.select('refresh');
                        return;
                    }
                    setList($scope.ngAutocomplete.localList);
                });
            }
        };
    });

