/*
    noPagination: Boolean
 */


angular.module('Directives')
    .directive('ngIntellectualTable', function() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/intellectual-table.html',
            replace: true,
            scope: {
                table: '=tableData'
            },
            controller: function($scope) {
                $scope.table.data = $scope.table ? $scope.table.data || [] : [];
                $scope.activeSortName = false;
                $scope.activeFilters = {};
                $scope.sortASC = false;
                $scope.checkedItems = [];
                $scope.templatesPath = '/templates/directives/intellectual-table/';

                $scope.onInitTable = function(params) {
                    $scope.table.onInitTable ?
                        $scope.table.onInitTable(params) :
                        false;
                };

                var updatedFiltersFields = false;

                $scope.paginationData = {};

                $scope.resetFilters = function() {
                    $scope.table.head.map(function (item) {
                        if (item.filterType == 'autocomplete') {
                            item.autocompleteParams.localList = false;
                        }
                    });
                    if (!$scope.activeFilters || $scope.activeFilters == {}) return;
                    $scope.activeFilters = {};
                    if (!updatedFiltersFields) {
                        updatedFiltersFields = true;
                        $scope.applyFiltersCallback()
                    }
                };
                $scope.applyFiltersCallback = function () {
                    var sort;
                    if ($scope.activeSortName) {
                        sort = [$scope.activeSortName];
                        if (!$scope.sortASC) {
                            sort.push('desc');
                        }
                        sort = sort.join(',');
                    }
                    updatedFiltersFields && $scope.table.onApplyFilters ?
                        $scope.table.onApplyFilters({
                            sort: sort,
                            page: $scope.paginationData.number - 1,
                            size: $scope.paginationData.size
                        }, $scope.activeFilters, function() {
                            $scope.checkedItems = [];
                        }) : false;
                    updatedFiltersFields = false;
                };
                $scope.applySort = function (sortName) {
                    if ($scope.activeSortName == sortName) {
                        $scope.activeSortName = $scope.sortASC ? sortName : false;
                        $scope.sortASC = !$scope.sortASC;
                    } else {
                        $scope.activeSortName = sortName;
                        $scope.sortASC = true;
                    }
                    updatedFiltersFields = true;
                    $scope.applyFiltersCallback();
                    /* updatedFiltersFields = true;
                    $scope.applyFiltersCallback();*/
                };
                $scope.updatedFilters = function () {
                    updatedFiltersFields = true;
                };

                $scope.toPage = function(page) {
                    if ((page < 1) || (page > $scope.paginationData.totalPages)) return;
                    $scope.paginationData.number = page;
                    updatedFiltersFields = true;
                    $scope.applyFiltersCallback();
                };

                $scope.onCheck = function(item) {
                    item.selected = !(item.selected || false);
                    $scope.checkedItems = $scope.table.info.content.filter(function(item) {
                        return item.selected;
                    });
                };

                $scope.setSize = function(size) {
                    if (
                        ($scope.paginationData.size >= $scope.paginationData.totalElements) &&
                        (size >= $scope.paginationData.totalElements)
                    ) {
                        $scope.paginationData.size = size;
                        return;
                    } else {
                        $scope.paginationData.size = size;
                    }
                    $scope.paginationData.number = Math.min($scope.paginationData.number, Math.ceil($scope.paginationData.totalElements / $scope.paginationData.size));
                    updatedFiltersFields = true;
                    $scope.applyFiltersCallback();
                };

                var viewFormatTemplate = function(item) {
                    return function(data) {
                        var value = data[item.viewName];
                        // value = value || ((value !== null) ? typeof value : '');
                        switch (item.cellType) {
                            case 'array-links':
                                if (!angular.isArray(value)) {
                                    value = [value];
                                }
                                break;
                            default:
                                break;
                        }
                        return value;
                    };
                };

                $scope.checkedItems = [];

                var resetSelected = function() {
                    $scope.checkedItems = [];
                    $scope.checkedRows = false;
                    if (!($scope.table.info && $scope.table.info.content)) return;
                    $scope.table.info.content.map(function (item) {
                        item.selected = false;
                    });
                };

                $scope.checkSelected = function(a) {
                    $scope.checkedRows = !$scope.checkedRows;
                    if ($scope.checkedRows) {
                        $scope.checkedItems = $scope.table.info.content;
                        $scope.table.info.content.map(function (item) {
                            item.selected = true;
                        });
                    } else {
                        resetSelected();
                    }
                };

                $scope.$watch('table.head', function (a) {
                    a.map(function (item) {
                        switch(item.filterType) {
                            case 'select':
                                item.filterOptions = angular.copy(item.filterOptions);
                                var disabledOption = {
                                    text: item.filterPlaceholder
                                };
                                item.filterOptions.unshift(disabledOption);
                                item.model = disabledOption;
                                break;
                            case 'autocomplete':
                                item.autocompleteParams = {
                                    placeholder: 'Enter the country location',
                                    textField: 'name',
                                    searchField: 'name',
                                    service: 'Country',
                                    fieldName: 'Country',
                                    required: false,
                                    selectClass: 'select-2',
                                    onChange: function(newModel) {
                                        item.autocompleteModel = newModel;
                                        $scope.updatedFilters(true);
                                        $scope.activeFilters[item.filterName || item.viewName] = newModel.code;
                                    }
                                };
                                item.onKeyDown = function(e) {
                                    if (e.keyCode == 13) {
                                        $scope.applyFiltersCallback();
                                    }
                                };
                                break;
                            case 'date':
                                item.model = {
                                    from: '',
                                    to: ''
                                };
                                break;
                            default:
                                item.model = '';
                                break;
                        }

                        item.viewFormat = item.viewFormat || viewFormatTemplate(item);

                        if (!($scope.table.noTitles)) {
                            item.titleCellTemplate =
                                $scope.templatesPath + 'header-cells/cell' + ((item.noSort || $scope.table.noSort) ? '' : '-with-sort') + '.html';
                        }
                        if (!$scope.table.noFilters && !item.noFilter) {
                            item.filterCellTemplate =
                                $scope.templatesPath + 'filters-cells/' + (item.filterType || 'text') + '.html';
                        }
                        item.bodyCellTemplate = $scope.templatesPath + 'body-cells/' + (item.cellType || 'text') + '.html';
                    });
                });
                $scope.$watch('table.info', function (a) {
                    resetSelected();
                    if (!a) return;
                    for (var k in $scope.table.info) {
                        var item = $scope.table.info[k];
                        if (k != 'content') {
                            $scope.paginationData[k] = angular.copy(item);
                        }
                    }
                    $scope.paginationData.number = $scope.paginationData.number + 1;
                    $scope.activeSortName = $scope.paginationData.sort ? $scope.paginationData.sort[0].property : false;
                    $scope.sortASC = $scope.paginationData.sort ? $scope.paginationData.sort[0].direction == 'ASC' : false;
                });
            }
        }
    });