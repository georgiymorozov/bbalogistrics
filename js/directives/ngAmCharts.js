var module = angular.module('Directives');


module.directive('ngAmCharts', function () {
    return {
        restrict: 'A',
        scope: {
            ngAmCharts: '=',
            ngAmChartsOptions: '='
        },
        link: function ($scope, element) {
            var chart = AmCharts.makeChart(element.get(0), $scope.ngAmCharts);
            $scope.$watch('ngAmCharts', function() {
            });
        }
    }
});
