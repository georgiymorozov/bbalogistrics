'use strict';

angular.module('Directives').directive('customOnChange', function() {
    return {
        restrict: "A",
        scope: {
            handler: '&',
            size: '@'
        },
        link: function(scope, element, attrs) {
            element.change(function(event){
                var file = event.target.files[0];
                var params = {
                    event: event,
                    el: element,
                    file: file
                };
                var regular = attrs.accept ? new RegExp(attrs.accept) : false;
                var size = scope.size*1 || 0;
                if (regular && !regular.test(file.type)) {
                    params.error = 'MIME-type mismatch';
                } else if (size && (size < file.size)) {
                    params.error = 'High file size';
                }
                scope.$apply(function(){
                    scope.handler({params: params});
                });
            });
        }

    };
});
