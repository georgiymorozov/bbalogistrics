var module = angular.module('Directives');
module.directive('ngDatePicker', function ($filter) {
    return {
        restrict: 'A',
        scope: {
            ngModel: '=?',
            ngParams: '=',
            ngDatePicker: '='
        },
        link: function ($scope, element, attrs) {


            var defaultParams = {
                nextText: '',
                prevText: '',
                showOtherMonths: true,
                showAnim: false,
                dateFormat: 'dd/mm/yy',
                maxDate: new Date(),
                onSelect: function (date) {
                    $scope.$apply();
                    $scope.ngDatePicker && $scope.ngDatePicker.onSelect ?
                        $scope.ngDatePicker.onSelect(element.datepicker('getDate')) :
                        false;
                    $scope.$apply();
                }
            };

            $scope.$watch('ngParams', function () {
                if (!$scope.ngParams) return;
                angular.extend(defaultParams, $scope.ngParams);
                element.datepicker('option', defaultParams);
            });
            $scope.$watch('ngModel', function () {
                if (!$scope.ngModel) return;
                element.datepicker("setDate", $scope.ngModel);
            });

            element.datepicker(defaultParams);

            var isFocus;
            element.on('mousedown', function () {
                isFocus = element.is(':focus');
            }).on('click', function () {
                isFocus ? element.datepicker('show') : false;
            });
        }
    }
});
