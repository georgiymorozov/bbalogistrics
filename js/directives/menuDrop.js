var module = angular.module('app');
module.directive('menuDrop', ['$document', function ($document) {
    return {
        restrict: 'E',
        templateUrl: '/templates/directives/menu_drop.html',
        replace: true,
        transclude: true,
        scope: {
            'item': '=',
            'itemClick': '&',
            menuData: '=?menuData',
            menuTitle: '@'
        },
        link: function(scope, element, attrs) {
            scope.isPopupVisible = false;
            scope.toggleSelect = function(){
                scope.isPopupVisible = !scope.isPopupVisible;
            };

            $document.bind('click', function (event) {
                var isClickedElementChildOfPopup = element.find(event.target).length > 0;
                if (isClickedElementChildOfPopup) return;
                scope.isPopupVisible = false;
                scope.$apply();
            });
        }

    };
}]);
