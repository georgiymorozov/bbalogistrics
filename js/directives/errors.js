'use strict';

angular.module('Directives').directive('formError', function() {
    return {
        restrict: "A",
        scope: {
            error: '=ngErrors'
        },
        replace: false,
        templateUrl: '/templates/directives/form-error.html',
        controller: function($scope) {
            $scope.toError = false;
            $scope.errorClone = false;
        },
        link: function($scope, element) {

            $scope.closeError = function() {
                element.slideUp(300);
            };
            $scope.$watch('error', function(n, o) {
                if (n == o) return;
                if (n) {
                    $scope.errorClone = angular.copy($scope.error);
                    o && o.type ? element.removeClass(o.type) : false;
                    n && n.type ? element.addClass(n.type) : false;
                    element.slideDown(300);
                    $scope.toError = !$scope.toError;
                } else {
                    n === null ? element.hide() : element.slideUp(300);
                }
            });
        }
    };
});
