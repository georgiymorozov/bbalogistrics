angular.module('Directives')
    .directive('ngAddressForm', function() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/address-form.html',
            replace: true,
            scope: {
                ngModel: '=?ngModel',
                options: '=?ngParams',
                readonly: '=ngReadonly',
                ngLocationOptions: '=ngLocationOptions'
            },
            controller: function ($scope) {
            }
        }
    });

