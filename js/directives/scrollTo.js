(function() {
    var module = angular.module('Directives');
    module.directive('ngScrollTo', function() {
        return {
            'restrict': 'A',
            'scope': {
                ngScrollTo: '=ngScrollTo'
            },
            'link': function linkingFunction($scope, element, attr) {
                var container = angular.element(window);
                var topBarHeight = angular.element('.top-bar:first').outerHeight();
                var tabLine = angular.element('.tab-section-line:first', element.parents('.tab-section:first'));
                var isLine = element.is('.tab-section-line');

                $scope.$watch('ngScrollTo', function() {
                    if ($scope.ngScrollTo === undefined) return;
                    var obj = {
                        top: element.offset()["top"] - topBarHeight - (!isLine ? tabLine.outerHeight() : 0)
                    };



                    angular.element({
                        top: container.scrollTop()
                    }).animate({
                        top: obj.top
                    }, {
                        duration: 500,
                        step: function() {
                            container.scrollTop(this.top);
                            //obj.top = element.offset()["top"] - topBarHeight;
                        }
                    });
                });
            },
            controller: function($scope) {}
        }
    });
})();