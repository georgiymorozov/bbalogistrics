'use strict';

angular.module('Directives').directive('ngToFocus', function () {
    return {
        restrict: "A",
        scope: {
            model: '=ngToFocus'
        },
        controller: function ($scope) {
        },
        link: function ($scope, element) {
            $scope.$watch('model', function (n, o) {
                if (n != undefined) {
                    element.focus();
                }
            });
        }
    };
});
