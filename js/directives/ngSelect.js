(function() {
var module = angular.module('Directives');
    module.directive('ngSelect', ['$timeout', function($timeout) {
            return {
                'restrict': 'A',
                'scope': {
                    ngModel: '=ngModel',
                    ngOptionsList: '=?ngModelList',
                    ngSelectClass: '@ngSelectClass'
                },
                'link': function linkingFunction($scope, element, attr) {
                    $scope.select = false;
                    $scope.$watch('ngOptionsList', function() {
                        if ($scope.ngOptionsList) {
                            $scope.select ? element.select('refresh') : $scope.decorate(element);
                        }
                    });
                    $scope.$watch('ngModel', function () {
                        if ($scope.ngModel === undefined) return;
                        $scope.select ? element.select('refresh') : $scope.decorate(element);
                    });
                    return $timeout(function() {
                        $scope.decorate(element);
                    });
                },
                controller: function($scope) {
                    $scope.options = {
                        'class': 'select-1'
                    };
                    $scope.decorate = function(el, attr) {
                        if ($scope.ngSelectClass) {
                            $scope.options.class = $scope.ngSelectClass;
                        }
                        el.select(false, $scope.options);
                        $scope.select = true;
                    };
                }
            }
        }]);
})();