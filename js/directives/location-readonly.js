angular.module('Directives')
    .directive('ngReadonlyLocation', function() {
        return {
            restrict: 'E',
            templateUrl: '/templates/directives/location-readonly.html',
            replace: true,
            scope: {
                ngModel: '=?ngModel'
            },
            controller: function ($scope) {}
        }
    });