var module = angular.module('Directives');

/** Chart Font style default options **/
Chart.defaults.global.defaultFontColor = '#1e252b';
Chart.defaults.global.defaultFontFamily = 'Ubuntu, sans-serif';
Chart.defaults.global.defaultFontStyle = 'bold';
Chart.defaults.global.defaultFontSize = 10;

Chart.defaults.doughnut.cutoutPercentage = 0;

/** Chart grid style default options **/
Chart.defaults.global.responsive = true;
Chart.defaults.global.animation.duration = 500;

Chart.defaults.scale.gridLines.display = true;

/** Legend **/
Chart.defaults.global.legend.display = false;

Chart.defaults.global.tooltips.titleFontSize = 14;
Chart.defaults.global.tooltips.bodyFontSize = 14;

module.directive('ngChart', function () {
    return {
        restrict: 'A',
        scope: {
            ngChart: '=',
            ngChartData: '=',
            ngChartType: '@'
        },
        link: function ($scope, element) {
            var canvas = angular.element('<canvas>');
            if ($scope.ngChartType == 'pie') {
                canvas.attr({
                    width: element.width(),
                    height: element.width() * 0.75
                });
            }

            element.append(canvas);
            new Chart(
                canvas, {
                    type: $scope.ngChartType,
                    data: $scope.ngChartData || {},
                    options: $scope.ngChart || {}
                }
            )
        }
    }
});
