'use strict';

angular.module('Services', []);
angular.module('Const', []);
angular.module('Directives', []);
angular.module('app', ['ngRoute', 'ngCookies', 'base64', 'Services', 'Directives', 'Const'])
    .controller('baseController', function ($rootScope, $timeout, AuthenticationService, $cookies, UserService, $route, $location, $http) {
        $rootScope.showedMenu = false;
        $rootScope.searchFocus = false;
        $rootScope.setSearchFocus = function(fl) {
            $rootScope.searchFocus = fl ? false : true;
        };

                /*object  menus for index.html page */
        
        $rootScope.toggleMenu = function() {
            $rootScope.showedMenu = !$rootScope.showedMenu;
        };

        $rootScope.logOut = function(prev) {
            var url = loginPageURL;
            if (prev) {
                url+= '?go=' + encodeURIComponent(window.location.pathname);
            }
            AuthenticationService.clearCredentials();
            window.location = url;
        };
        var refresh_token = $cookies.get('auth') || false;

        if (!refresh_token) {
            return $rootScope.logOut(true);
        }

        $rootScope.$on('$locationChangeStart', function () {
            if (!refresh_token) {
                return $rootScope.logOut(true);
            }
        });
        var updateToken = function() {
            AuthenticationService.refreshToken(function (response) {
                if (!$rootScope.GlobalProfile) {
                    UserService.getUser(function (data) {
                        $rootScope.GlobalProfile = data;
                        $rootScope.isAdmin = data.roles.indexOf(ROLES.admin) != -1;
                    }, function () {
                        $rootScope.logOut(true);
                    });
                }
                $timeout(updateToken, Math.min(600000, response['data']['expires_in'] * 1000));
            }, function (response) {
                $rootScope.logOut(true);
            }, 'refresh');
        };
        $rootScope.resetForm = function(model) {
            model = {};
        };
        $http({
            url: '/languages/en.json'
        }).success(function (data) {
            $rootScope.languageLibrary = data;
            updateToken();
        }).finally(function (response) {
            options['doneCallback'] ? options['doneCallback'](response) : false;
            $rootScope.sendRequest = false;
        });


        var win = angular.element(window);
        var iniTabletMenu = function() {
            $rootScope.tabletMenu = win.width() < 1170;
        };


        $rootScope.mainMenu = [];
        $rootScope.menuTemplate = '/templates/elements/main-menu.tpl';
        $rootScope.currentRouteParams = {};
        var menuItems = {};

        for (var i in $route.routes) {
            if ($route.routes[i].params && $route.routes[i].params.mainMenu) {
                if ($route.routes[i].params.name) {
                    menuItems[$route.routes[i].params.name] = $route.routes[i];
                }
                $rootScope.mainMenu.push($route.routes[i]);
            }
        }

        var activateItemMenu = function(event, current, old) {
            if (!old || (old.$$route != current.$$route)) {
                //var updateParams = {};
                //for (var i in current['params']) {
                //    updateParams[i] = undefined;
                //}
                //updateParams ? $route.updateParams(updateParams) : false;
            }
            if (current.$$route.params) {
                if (current.$$route.params.mainMenu) {
                    $rootScope.currentRoute = current.$$route;
                } else if (current.$$route.params.parent && menuItems[current.$$route.params.parent]) {
                    $rootScope.currentRoute = menuItems[current.$$route.params.parent];
                }
                $rootScope.currentRouteParams = current.$$route.params;
            } else {
                $rootScope.currentRouteParams = {};
            }
        };

        var originalTitleElement = angular.element('title:first');
        var originalTitleText = originalTitleElement.text();

        $rootScope.$watch('currentRouteParams', function(n) {
            originalTitleElement.text(
                ($rootScope.GlobalProfile && $rootScope.currentRouteParams.htmlTitle) ?
                    $rootScope.currentRouteParams.htmlTitle : originalTitleText);
        });
        $rootScope.$watch('GlobalProfile', function() {
            originalTitleElement.text(
                ($rootScope.GlobalProfile && $rootScope.currentRouteParams.htmlTitle) ?
                    $rootScope.currentRouteParams.htmlTitle : originalTitleText);
        });
        $rootScope.$on('$routeChangeSuccess', activateItemMenu);
        $rootScope.toLocationPage = function(url) {
            return url.split(':')[0];
        };

    })
    .controller('ExitController', function($rootScope) {
        $rootScope.logOut();
    });
