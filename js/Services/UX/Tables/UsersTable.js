'use strict';

angular.module('Services').service('UsersTable', function($filter, TableData) {
    return function () {

        var getUsersList = function() {
            tableOptions.info = {
                size: '5',
                number: 0,
                totalPages: 1,
                totalElements: 2,
                numberOfElements: 2,
                first: true,
                last: true,
                content: [
                    {
                        firstName: 'John',
                        surname: 'Smith',
                        phone1: '03 9565 5555',
                        phone2: '',
                        email: 'john@myer.com.au',
                        password: 'Password',
                        password2: 'Password',
                        userType: 'Primary'
                    },
                    {
                        firstName: 'Paul',
                        surname: 'Young',
                        phone1: '03 9565 5555',
                        phone2: '',
                        email: 'paul@myer.com.au',
                        password: 'Password',
                        password2: 'Password',
                        userType: 'Secondary'
                    }
                ]
            };
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': true,
            'noSort': false,
            'noTitles': false,
            'noFilters': true,
            'onApplyFilters': false,
            'onInitTable': getUsersList,
            'head': [],
            'info': []
        };

        var orderOptions = [
            'firstName', 'surname', 'phone1',
            'phone2', 'email', 'password',
            'password2', 'userType'
        ];

        var cellsOptions = {
            firstName: {
                viewName: 'firstName',
                title: 'First Name'
            },
            surname: {
                viewName: 'surname',
                title: 'Surname'
            },
            phone1: {
                title: 'Phone1',
                viewName: 'phone1'
            },
            phone2: {
                title: 'Phone2',
                viewName: 'phone2'
            },
            email: {
                title: 'Email',
                viewName: 'email'
            },
            password: {
                title: 'Password',
                viewName: 'password'
            },
            password2: {
                title: 'Password',
                viewName: 'password2'
            },
            userType: {
                title: 'User Type',
                viewName: 'userType'
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
    }
});