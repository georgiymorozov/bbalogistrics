'use strict';

angular.module('Services').service('WarehousesTable', function ($filter, TableData, WarehousesService, Windows, $location) {
    return function(options) {
        var oldSearchData = false;

        var getWarehousesList = function(paginationData, searchData, callback, errCallback) {
            paginationData = paginationData || {};
            paginationData.size = paginationData.size || 5;
            tableOptions.inProgress = 'update-data';
            searchData = searchData || oldSearchData;
            if (!paginationData.sort) {
                paginationData.sort = 'id,desc';
            }

            if (options && options.params) {
                if (searchData) {
                    searchData = angular.extend(searchData, options.params);
                } else {
                    searchData = options.params;
                }
            }
            WarehousesService.getList(
                function(data) {
                    tableOptions.info = data;
                    tableOptions.inProgress = false;
                    callback ? callback() : false;
                }, function() {
                    errCallback ? errCallback(err) : false;
                    tableOptions.inProgress = false;
                },  paginationData, searchData
            );
        };

        var deleteWarehouse = function(addresses) {
            if (!Array.isArray(addresses)) {
                addresses = [addresses];
            }
            if (!addresses.length) return;
            var ids = [];
            addresses.map(function(address) {
                ids.push(address['id']);
            });
            Windows.confirm({
                title: 'Warehouses',
                text: 'You really want to remove element from your warehouses?',
                onConfirm: function() {
                    addresses.map(function (address) {
                        address.processing = 'delete';
                    });
                    WarehousesService.delete(ids, function () {
                        getWarehousesList();
                    });
                },
                onClose: function() {
                    //alert('Молодец');
                },
                confirmText: 'Yes, Delete',
                cancelText: 'No, Cancel'
            });
        };

        var toEditWarehouse = function(data) {
            $location.path('/configuration/warehouses/edit/' + data['id']);
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': true,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': getWarehousesList,
            'onInitTable': getWarehousesList,
            'head': [],
            'info': []
        };

        var orderOptions = ['company', 'name', 'street', 'city', 'code', 'state', 'country', 'buttons'];

        var cellsOptions = {
            company: {
                viewName: 'name',
                title: 'Company'
            },
            name: {
                title: 'Name',
                noSort: true,
                viewFormat: function(data) {
                    return data.firstName + ' ' + data.lastName;
                }
            },
            street: {
                title: 'Street',
                noSort: true,
                viewFormat: function(data) {
                    return data.addressLine1 + (data.addressLine2 ? ', ' + data.addressLine2 : '');
                }
            },
            city: {
                title: 'City',
                noSort: true,
                viewFormat: function(data) {
                    return data.location ? data.location.name : '';
                }
            },
            code: {
                title: 'Code',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.code ? data.location.code : '';
                }
            },
            state: {
                title: 'State',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.state ? data.location.state.name : '';
                }
            },
            country: {
                title: 'Country',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.country.name ? data.location.country.name + ' (' + data.location.country.code + ')' : '';
                }
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell nowrap',
                bodyButtons: [{
                    type: 'btn-red',
                    action: deleteWarehouse,
                    text: 'Delete',
                    forProcessing: 'delete'
                }, {
                    type: 'btn-blue',
                    action: toEditWarehouse,
                    icon: 'edit_icon',
                    text: 'View/Edit'
                }]
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.updateTableData = getWarehousesList;
    }
});
