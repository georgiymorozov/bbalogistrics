'use strict';

angular.module('Services').service('TableData', function() {
    return {
        baseData: function (opt, tblOptions, ordOptions, clsOptions) {
            opt = opt || {
                    table: false,
                    order: false,
                    cellsOptions: false
                };
            ordOptions = opt.order || ordOptions;
            if (opt.table) {
                for (var i in opt.table) {
                    tblOptions[i] = opt.table[i];
                }
            }
            tblOptions.head = [];
            ordOptions.map(function (name) {
                if (opt.cellsOptions && opt.cellsOptions[name] === false)
                    return;
                if (opt.cellsOptions && opt.cellsOptions[name]) {
                    clsOptions[name] = clsOptions[name] || {};
                    for (var k in opt.cellsOptions[name]) {
                        clsOptions[name][k] = opt.cellsOptions[name][k];
                    }
                }
                tblOptions.head.push(clsOptions[name]);
            });
        }
    };
});
