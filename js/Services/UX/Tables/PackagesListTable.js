'use strict';

angular.module('Services').service('PackagesListTable', function (PackagesService, $filter, TableData, $location, Windows) {
    return function(options) {

        var oldSearchData = false;

        var toPackagingEdit = function(data) {
            $location.path('/configuration/packaging/edit/' + data['id']);
        };
        var deletePackage = function (packages) {
            if (!Array.isArray(packages)) {
                packages = [packages];
            }
            if (!packages.length) return;
            var ids = [];
            packages.map(function (pack) {
                ids.push(pack['id']);
            });
            Windows.confirm({
                title: 'Packages',
                text: 'You really want to remove element from your packages?',
                onConfirm: function () {
                    packages.map(function (pack) {
                        pack.processing = 'delete';
                    });
                    PackagesService.delete(ids, function () {
                        getPackagesList();
                    });
                },
                onClose: function () {
                    //alert('Молодец');
                },
                confirmText: 'Yes, Delete',
                cancelText: 'No, Cancel'
            });
        };
        var getPackagesList = function(paginationData, searchData, callback, errCallback) {
            paginationData = paginationData || {};
            paginationData.size = paginationData.size || 5;
            tableOptions.inProgress = 'update-data';
            searchData = searchData || oldSearchData;
            if (!paginationData.sort) {
                paginationData.sort = 'id,desc';
            }

            if (options && options.params) {
                if (searchData) {
                    searchData = angular.extend(searchData, options.params);
                } else {
                    searchData = options.params;
                }
            }
            if (!paginationData.sort) {
                paginationData.sort = 'id,desc';
            }
            PackagesService.getList(function (data) {
                tableOptions.info = data;
                tableOptions.inProgress = false;
                callback ? callback() : false;
            }, function(err) {
                errCallback ? errCallback(err) : false;
                tableOptions.inProgress = false;
            },  paginationData, searchData);
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': true,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': getPackagesList,
            'onInitTable': getPackagesList,
            'head': [],
            'info': []
        };

        var orderOptions = ['name', 'length', 'width', 'height', 'measureUnit', 'maxWeight', 'weightUnit', 'buttons'];

        var cellsOptions = {
            name: {
                viewName: 'name',
                title: 'Title'
            },
            length: {
                viewName: 'length',
                title: 'Length'
            },
            width: {
                viewName: 'width',
                title: 'Width'
            },
            height: {
                title: 'Height',
                viewName: 'height'
            },
            measureUnit: {
                title: 'Measure Unit',
                viewName: 'measureUnit'
            },
            maxWeight: {
                title: 'Weight',
                viewName: 'maxWeight'
            },
            weightUnit: {
                title: 'Weight Unit',
                viewName: 'weightUnit'
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell',
                bodyButtons: [
                    {
                        type: 'btn-blue',
                        action: toPackagingEdit,
                        text: 'Edit/View',
                        icon: 'edit_icon'
                    }, {
                        type: 'btn-red',
                        action: deletePackage,
                        text: 'Delete',
                        forProcessing: 'delete'
                    }
                ]
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.setOption = function(key, value) {
            options = options || {};
            options[key] = value;
        };
        this.updateTableData = getPackagesList;
    }
});
