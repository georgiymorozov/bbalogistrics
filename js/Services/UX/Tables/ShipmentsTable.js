'use strict';

angular.module('Services').service('ShipmentsTable', function (ShipmentsService, $filter, TableData, shipmentStatuses) {
    return function (defaultFilters) {
        defaultFilters = defaultFilters || {};

        var oldSearchData = false;
        var getShipments = function(paginationData, searchData, callback) {

            paginationData = paginationData || {};
            paginationData.size = paginationData.size || 5;
            tableOptions.inProgress = 'update-data';
            searchData = searchData || oldSearchData || {};
            angular.extend(searchData, defaultFilters);

            if (!paginationData.sort) {
                paginationData.sort = 'id,desc';
            }
            ShipmentsService.getShipments(function (data) {
                tableOptions.info = data;
                tableOptions.inProgress = false;
                callback ? callback() : false;
            }, function(err) {
                callback ? callback(err) : false;
                tableOptions.inProgress = false;
            },  paginationData, searchData);

            oldSearchData = searchData;
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': true,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': getShipments,
            'onInitTable': getShipments,
            'head': [],
            'info': []
        };

        var orderOptions = [
            'date', 'bbaReferences', 'consignment',
            'sscc', 'trackingNumber', 'returnAuthorityNumber',
            'bookingStatus', 'carrier', 'total', 'buttons'
        ];

        var cellsOptions = {
            date: {
                viewName: 'pickupDetails',
                sortName: 'originalDate',
                filterName: 'originalDate',
                filterType: 'date',
                title: 'Date',
                typeCellClass: 'min-width',
                viewFormat: function (data) {
                    return $filter('date')(new Date(data.pickupDetails.pickupDate), 'dd/MM/yyyy');
                }
            },
            references: {
                viewName: 'customerReferences',
                title: 'Customer Ref' + "\u00A0" + 'No.',
                cellType: 'array-links',
                urlFormat: function(value) {
                    return '#';
                }
            },
            bbaReferences: {
                viewName: 'bbaReferences',
                title: 'BBA Ref' + "\u00A0" + 'No.',
                cellType: 'array-links',
                urlFormat: function(value) {
                    return '#';
                }
            },
            consignment: {
                title: 'Consignment' + "\u00A0" + 'No.',
                viewName: 'consignmentId'
            },
            sscc: {
                title: 'SSCC' + "\u00A0" + 'No.',
                viewName: 'sscc'
            },
            trackingNumber: {
                title: 'Tracking' + "\u00A0" + 'No.',
                viewName: 'trackingNumber',
                cellType: 'link',
                urlFormat: function(value) {
                    return '/tracking/' + value.id;
                }
            },
            returnAuthorityNumber: {
                title: 'Return Authority' + "\u00A0" + 'No.',
                viewName: 'returnAuthorityNumber'
            },
            bookingStatus: {
                title: 'Booking Status',
                viewName: 'bookingStatus',
                viewFormat: function (shipment) {
                    return shipmentStatuses[shipment['bookingStatus']]
                }
            },
            carrier: {
                title: 'Carrier',
                titleName: 'titleLogo',
                sortName: 'carrierId',
                filterType: 'select',
                cellType: 'logotype',
                container: 'logotype-shipping',
                typeCellClass: 'logo-cell',
                srcFormat: function(item) {
                    return item['serviceDetails'] ?
                        item['serviceDetails']['logo'] : false;
                },
                filterPlaceholder: 'Select Carrier',
                filterOptions: [
                    {
                        text: 'Carrier 1',
                        value: 1
                    }, {
                        text: 'Carrier 2',
                        value: 2
                    }, {
                        text: 'Carrier 3',
                        value: 3
                    }, {
                        text: 'Carrier 4',
                        value: 4
                    }, {
                        text: 'Carrier 5',
                        value: 5
                    }
                ]
            },
            total: {
                name: 'total',
                title: 'Amount (Ex' + "\u00A0" + 'Tax)',
                viewName: 'total'
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell'
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.updateTableData = getShipments;
    }
});
