'use strict';

angular.module('Services').service('BBAProfilesTable', function (Windows, TableData, $location, $timeout) {
    return function () {
        var deleteProfile = function (profiles) {
            if (!Array.isArray(profiles)) {
                profiles = [profiles];
            }
            if (!profiles.length) return;

            Windows.confirm({
                title: 'BBA Profile Manager',
                text: 'You really want to remove element from your BBA Profiles?',
                onConfirm: function () {

                    /** FixMe **/
                    var oldInfo = tableOptions.info;
                    var newArray = [];
                    oldInfo.content.map(function (item, index) {
                        if (profiles.indexOf(item) == -1) {
                            newArray.push(item);
                        }
                    });
                    oldInfo.content = newArray;
                    tableOptions.info = angular.copy(oldInfo);
                },
                onClose: function () {
                    //alert('Молодец');
                },
                confirmText: 'Yes, Delete',
                cancelText: 'No, Cancel'
            });
        };


        var getUsersList = function (content) {
            tableOptions.info = {
                size: '5',
                number: 0,
                totalPages: 1,
                totalElements: 2,
                numberOfElements: 2,
                first: true,
                last: true,
                content: [
                    {
                        name: 'China to Melbourne IKA Air',
                        id: 1
                    },
                    {
                        name: 'China to Melbourne IKA Air',
                        id: 2
                    },
                    {
                        name: 'China to Melbourne IKA Air',
                        id: 3
                    },
                    {
                        name: 'China to Melbourne IKA Air',
                        id: 4
                    },
                    {
                        name: 'China to Melbourne IKA Air',
                        id: 5
                    }
                ]
            };
        };

        var toProfileEdit = function () {
            $location.url('/configuration/profile-manager/edit');
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': false,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': false,
            'onInitTable': getUsersList,
            'head': [],
            'info': []
        };

        var orderOptions = [
            'name', 'buttons'
        ];

        var cellsOptions = {
            name: {
                viewName: 'name',
                title: 'BBA Profile Name',
                cellType: 'array-links',
                urlFormat: function (value) {
                    return '#';
                }
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell',
                bodyButtons: [
                    {
                        type: 'btn-blue',
                        action: toProfileEdit,
                        text: 'Edit/View',
                        icon: 'edit_icon'
                    }, {
                        type: 'btn-red',
                        action: deleteProfile,
                        text: 'Delete',
                        forProcessing: 'delete'
                    }
                ],
                filterButtons: [{
                    type: 'btn-red',
                    action: deleteProfile,
                    text: 'Delete selected',
                    forProcessing: 'delete'
                }]
            }
        };

        this.getTableOptions = function (options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
    }
});