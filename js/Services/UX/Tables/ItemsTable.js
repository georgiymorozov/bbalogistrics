'use strict';

angular.module('Services').service('ItemsTable', function ($filter, TableData) {

    return function() {
        var tableOptions = {
            'noPagination': true,
            'noHead': false,
            'noCheck': true,
            'noSort': true,
            'noTitles': false,
            'noFilters': true,
            'onApplyFilters': false,
            'onInitTable': false,
            'head': [],
            'info': []
        };

        var orderOptions = ['description', 'buttons'];
        var cellsOptions = {
            description: {
                title: 'Description',
                cellType: 'product-item',
                typeCellClass: 'template'
            },
            buttons: {
                title: 'Actions',
                cellType: 'buttons',
                typeCellClass: 'btn-cell',
                bodyButtons: [
                    {
                        type: 'btn-normal-blue width-100 block',
                        action: function (data, button) {},
                        text: 'Exclude'
                    }
                ]
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.updateTableData = function(data) {
            tableOptions.info.content = data;
        };
    }
});
