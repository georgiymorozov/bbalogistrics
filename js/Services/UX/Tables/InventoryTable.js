'use strict';

angular.module('Services').service('InventoryTable', function (InventoryService, $filter, TableData, $location, Windows) {
    return function(options) {
        var oldSearchData = false;

        var getInventories = function(paginationData, searchData, callback, errCallback) {

            paginationData = paginationData || {};
            paginationData.size = paginationData.size || 5;
            tableOptions.inProgress = 'update-data';
            searchData = searchData || oldSearchData;
            if (options && options.params) {
                if (searchData) {
                    searchData = angular.extend(searchData, options.params);
                } else {
                    searchData = options.params;
                }
            }

            InventoryService.getInventoriesList(
                function(data) {
                    tableOptions.info = data;
                    tableOptions.inProgress = false;
                    callback ? callback() : false;
                }, function(err) {
                    errCallback ? errCallback(err) : false;
                    tableOptions.inProgress = false;
                },  paginationData, searchData
            );
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': false,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': getInventories,
            'onInitTable': getInventories,
            'head': [],
            'info': {}
        };

        var orderOptions = [
            'sku', 'description', 'length',
            'width', 'height', 'weight',
            'category', 'origin', 'itemType', 'value', 'actions'
        ];

        var deleteInventory = function(inventories) {
            if (!Array.isArray(inventories)) {
                inventories = [inventories];
            }
            if (!inventories.length) return;
            var skus = [];
            inventories.map(function(inventory) {
                skus.push(inventory.sku);
            });
            Windows.confirm({
                title: 'Inventory',
                text: 'You really want to remove Inventory element' + (skus.length > 1 ? 's' : '') + ' from your inventories?',
                onConfirm: function() {
                    inventories.map(function (inventory) {
                        inventory.processing = 'delete';
                    });
                    InventoryService.deleteInventory(skus, function () {
                        getInventories();
                    }, function() {
                        console.error('Inventory DELETE method failed');
                    });
                },
                onClose: function() {
                    //alert('Молодец');
                },
                confirmText: 'Yes, Delete',
                cancelText: 'No, Cancel'
            });
        };

        var cellsOptions = {
            sku: {
                viewName: 'sku',
                title: 'SKU' + "\u00A0" + '/' + "\u00A0" + 'Part' + "\u00A0" + '# (GTIN)'
            },
            description: {
                viewName: 'description',
                title: 'Description'
            },
            length: {
                viewName: 'length',
                title: 'Length'
            },
            width: {
                viewName: 'width',
                title: 'Width'
            },
            height: {
                viewName: 'height',
                title: 'Height'
            },
            weight: {
                viewName: 'weight',
                title: 'Weight'
            },
            category: {
                viewName: 'category',
                title: 'Category'
            },
            origin: {
                viewName: 'origin',
                title: 'MFG Origin',
                filterType: 'autocomplete'
            },
            itemType: {
                viewName: 'itemType',
                title: 'Item Type'
            },
            value: {
                viewName: 'value',
                title: 'Item Value'
            },
            actions: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell nowrap',
                bodyButtons: [{
                    type: 'btn-red',
                    action: deleteInventory,
                    text: 'Delete',
                    forProcessing: 'delete'
                }, {
                    type: 'btn-blue',
                    action: function (data) {
                        $location.path('/configuration/inventory/edit/' + data.sku);
                    },
                    icon: 'edit_icon',
                    text: 'View/Edit'
                }],
                filterButtons: [{
                    type: 'btn-red',
                    action: deleteInventory,
                    text: 'Delete selected',
                    forProcessing: 'delete'
                }]
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.setOption = function(key, value) {
            options = options || {};
            options[key] = value;
        };
        this.updateTableData = getInventories;
    }
});
