'use strict';

angular.module('Services').service('CarriersTable', function (CarriersService, $filter, TableData) {

    return function() {
        var tableOptions = {
            'noPagination': true,
            'noHead': false,
            'noCheck': true,
            'noSort': true,
            'noTitles': false,
            'noFilters': true,
            'onApplyFilters': false,
            'onInitTable': false,
            'head': [],
            'info': []
        };

        var orderOptions = ['carrier', 'service', 'transit', 'price', 'buttons'];
        var cellsOptions = {
            carrier: {
                title: 'Company',
                titleName: 'carrier',
                viewName: 'logo',
                sortName: 'carrier',
                cellType: 'logotype',
                typeCellClass: 'logo-cell',
                container: 'logotype-shipping',
                srcFormat: false,
                filterPlaceholder: 'Select Company'
            },
            service: {
                title: 'Service',
                viewName: 'service'
            },
            transit: {
                title: 'Transit',
                viewName: 'eta'
            },
            price: {
                title: 'Price',
                viewName: 'amount'
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                typeCellClass: 'btn-cell',
                bodyButtons: [{
                    type: 'btn-normal-blue width-100',
                    action: function (data, button) {
                        button = angular.element(button.currentTarget);

                        if (selectedItemButton) {
                            if (selectedItemButton.is(button)) return;
                            selectedItemButton.removeClass('no-active');
                            angular.element('.btn-text', selectedItemButton).text('Select');
                        }
                        selectedItemButton = button.addClass('no-active');
                        angular.element('.btn-text', selectedItemButton).text('Selected');

                        tableOptions.onSelect ? tableOptions.onSelect(data) : false;
                    },
                    text: 'Select'
                }]
            }
        };





        var selectedItemButton = false;

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };

        var createQuotesTableData = function(data) {
            var quotesList = [];
            (data && data.carriers) ? data.carriers.map(function (carrier) {
                carrier.quotes.map(function(quote) {
                    quote.carrier = carrier.carrier;
                    quote.carrierId = carrier.carrierId;
                    quotesList.push(quote);
                })
            }) : false;
            tableOptions.info.content = quotesList;
        };

        this.updateTableData = function(request, callback, errorCallback) {
            tableOptions.inProgress = 'update-data';
            CarriersService.getQuote(request, function (data) {
                tableOptions.info = data;
                tableOptions.inProgress = false;
                createQuotesTableData(data);
                callback ? callback(data) : false;
            }, function(err) {
                tableOptions.inProgress = false;
                errorCallback ? errorCallback(err) : false;
            });
        };
        this.resetTableData = function () {
            tableOptions.info.content = [];
        };
    }
});
