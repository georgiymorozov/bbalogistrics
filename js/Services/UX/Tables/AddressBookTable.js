'use strict';

angular.module('Services').service('AddressBookTable', function ($filter, TableData, AddressBookService, Windows, $location) {
    return function(options) {
        var oldSearchData = false;

        var getAddressBookList = function(paginationData, searchData, callback, errCallback) {
            searchData = {};
            paginationData = paginationData || {};
            paginationData.size = paginationData.size || 5;
            tableOptions.inProgress = 'update-data';
            searchData = searchData || oldSearchData;
            if (!paginationData.sort) {
                paginationData.sort = 'id,desc';
            }

            if (options && options.params) {
                if (searchData) {
                    searchData = angular.extend(searchData, options.params);
                } else {
                    searchData = options.params;
                }
            }
            AddressBookService.getBook(
                function(data) {
                    tableOptions.info = data;
                    tableOptions.inProgress = false;
                    callback ? callback() : false;
                }, function() {
                    errCallback ? errCallback(err) : false;
                    tableOptions.inProgress = false;
                },  paginationData, searchData
            );
        };

        var deleteAddress = function(addresses) {
            if (!Array.isArray(addresses)) {
                addresses = [addresses];
            }
            if (!addresses.length) return;
            var ids = [];
            addresses.map(function(address) {
                ids.push(address['id']);
            });
            Windows.confirm({
                title: 'Address Book',
                text: 'You really want to remove ' + ids.length + ' element' + (ids.length > 1 ? 's' : '') + ' from your address book?',
                onConfirm: function() {
                    addresses.map(function (address) {
                        address.processing = 'delete';
                    });
                    AddressBookService.deleteAddress(ids, function () {
                        getAddressBookList();
                    });
                },
                onClose: function() {
                    //alert('Молодец');
                },
                confirmText: 'Yes, Delete',
                cancelText: 'No, Cancel'
            });
        };
        var toAddressEdit = function(data) {
            $location.path('/configuration/address-book/edit/' + data['id']);
        };

        var tableOptions = {
            'noPagination': false,
            'noHead': false,
            'noCheck': false,
            'noSort': false,
            'noTitles': false,
            'noFilters': false,
            'onApplyFilters': getAddressBookList,
            'onInitTable': getAddressBookList,
            'head': [],
            'info': []
        };

        var orderOptions = ['company', 'name', 'email', 'phone', 'location', 'state', 'country', 'buttons'];

        var cellsOptions = {
            company: {
                viewName: 'company',
                title: 'Company'
            },
            name: {
                title: 'Name',
                noSort: true,
                viewFormat: function(data) {
                    return data.firstName + ' ' + data.lastName;
                }
            },
            email: {
                title: 'Email',
                viewName: 'email'
            },
            phone: {
                title: 'Phone',
                viewName: 'phone'
            },
            location: {
                title: 'Suburb / Postcode',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.name ? data.location.name + '/' + data.location.code : false;
                }
            },
            state: {
                title: 'State',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.state ? data.location.state.name : '';
                }
            },
            country: {
                title: 'Country',
                noSort: true,
                viewFormat: function(data) {
                    return data.location && data.location.country.name ? data.location.country.name + ' (' + data.location.country.code + ')' : '';
                }
            },
            buttons: {
                title: 'Actions',
                cellName: 'actions',
                cellType: 'buttons',
                noSort: true,
                filterType: 'buttons',
                typeCellClass: 'btn-cell nowrap',
                bodyButtons: [{
                    type: 'btn-red',
                    action: deleteAddress,
                    text: 'Delete',
                    forProcessing: 'delete'
                }, {
                    type: 'btn-blue',
                    action: toAddressEdit,
                    icon: 'edit_icon',
                    text: 'View/Edit'
                }],
                filterButtons: [{
                    type: 'btn-red',
                    action: deleteAddress,
                    text: 'Delete selected',
                    forProcessing: 'delete'
                }]
            }
        };

        this.getTableOptions = function(options) {
            TableData.baseData(options, tableOptions, orderOptions, cellsOptions);
            return tableOptions;
        };
        this.updateTableData = getAddressBookList;
    }
});
