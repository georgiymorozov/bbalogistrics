

angular.module('Services')
    .service('RequestService', function ($http, $rootScope, Error, Notice) {

        /*var error*/

        var errorsCallback = function(data, err, callback) {
            var response = angular.extend(data || {}, {
                code: err,
                message: 'Contact administrator if this error will occur again',
                response: data
            });
            switch(err) {
                case 400:
                    response['fieldErrors'] = data['fieldErrors'] ? Error.errorConverter(data['fieldErrors']) : false;
                    break;
                case 401:
                    return $rootScope.logOut();
                    break;
                case 500:
                    var noticeObject = {
                        code: 'Unknown error #' + err,
                        message: 'Contact administrator if this error will occur again',
                        number: 500
                    };
                    noticeObject.icon = 'fa-warning';
                    Notice.show(noticeObject);
                    break;
            }
            callback ? callback(response) : false;
        };

        $rootScope.sendRequest = false;

        /**
         *
         * @constructor
         * @author Dmitry Kovalev
         */
        var RequestAPIService = function () {

            /**
             *
             * @param {Object} options
             */
            this.get = function (options) {
                var params = options['params'] || {};
                params['url'] = options['url'];
                params['method'] = 'GET';
                var data = [];
                for (var i in options['data']) {
                    data.push(i + '=' + options['data'][i]);
                }
                params['url']+= data.length ? '?' + data.join('&') : '';
                $rootScope.sendRequest = true;
                $http(params).success(function(data) {
                    options['successCallback'] ? options['successCallback'](data) : false;
                }).error(function(data, err) {
                    errorsCallback(data, err, options['errorCallback']);
                }).finally(function (response) {
                    options['doneCallback'] ? options['doneCallback'](response) : false;
                    $rootScope.sendRequest = false;
                });
            };

            /**
             *
             * @param {Object} options
             * @param {Object} requestData
             */
            this.post = function (options, requestData) {
                $rootScope.sendRequest = true;
                $http({
                    url: options['url'],
                    method: options['method'] || 'PUT',
                    data: requestData
                }).success(function(data) {
                    options['successCallback'] ? options['successCallback'](data) : false;
                }).error(function(data, err) {
                    errorsCallback(data, err, options['errorCallback']);
                }).finally(function () {
                    $rootScope.sendRequest = false;
                });
            };

            /**
             *
             * @param {Object} options
             * @param {Object} requestData
             */
            this.delete = function (options, requestData) {
                $rootScope.sendRequest = true;
                $http({
                    url: options['url'],
                    method: opts['method'] || 'DELETE',
                    data: requestData,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }).success(function(data) {
                    options['successCallback'] ? options['successCallback'](data) : false;
                    Error.successMessage();
                }).error(function(data, err) {
                    errorsCallback(data, err, options['errorCallback']);
                }).finally(function () {
                    $rootScope.sendRequest = false;
                });
            };

            /**
             *
             * @param {Object} options
             * @param {String} fileName
             * @param {File} file
             */
            this.uploadFile = function (options, fileName, file) {
                $rootScope.sendRequest = true;
                var formData = new FormData();
                formData.append(fileName, file);
                $http({
                    url: options['url'],
                    method: 'POST',
                    data: formData,
                    headers: {'Content-Type': undefined }
                }).success(function(data) {
                    options['successCallback'] ? options['successCallback'](data) : false;
                    Error.successMessage();
                }).error(function(data, err) {
                    data = data || {};
                    errorsCallback(data, err, options['errorCallback']);
                }).finally(function () {
                    $rootScope.sendRequest = false;
                });
            };

            /**
             *
             * @param {Object} options
             */
            this.getFile = function (options) {
                var params = {
                    url: opts['url'] + '?' + opts['data'],
                    method: 'GET',
                    responseType: 'blob'
                };
                $rootScope.sendRequest = true;
                $http(params).success(function(data) {
                    var url = (window.URL || window.webkitURL).createObjectURL(data);
                    opts['successCallback'] ? opts['successCallback'](url) : false;
                }).error(function(data, err) {
                    errorsCallback(data, err, opts['errorCallback']);
                }).finally(function () {
                    $rootScope.sendRequest = false;
                });
            };

            /**
             *
             * @param params
             */
            this.exportFile = function (params) {
                params['successCallback'] = function(data) {
                    var blob = new Blob([data], {type: (params['fileType'] || 'text/plain') + ';charset=utf-8'});
                    saveAs(blob, params['fileName'] || 'file.txt');
                };
                params['errorCallback'] = function(data) {
                    params['errorCallback'] ? params['errorCallback'](data) : false;
                };
                this.get(params);
            }
        };
        return new RequestAPIService();
    })
    .service('Error', function () {
        return {
            errorConverter: function(errors) {
                errors = errors || [];
                var newErrors = {};
                for (var i = 0; i < errors.length; i++) {
                    newErrors[errors[i]['field']] = errors[i]['message'];
                }
                return newErrors;
            }
        };
    });
