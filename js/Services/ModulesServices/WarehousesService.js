angular.module('Services')
    .service('WarehousesService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             */
            getList: function (successCallback, errorCallback, pagination, filters) {
                RequestService.get({
                    url: ApiURLs.warehouses.get,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: angular.element.extend({}, pagination || {}, filters || {})
                });
            },

            /**
             * @param WarehouseData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            addToList: function (WarehouseData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.warehouses.get + (WarehouseData.id ? WarehouseData.id + '/' : ''),
                    method: WarehouseData.id ? 'PUT' : 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, WarehouseData);
            },

            /**
             * @param id - id of Warehouse in the Warehouses list
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getDetails: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.warehouses.get + id + '/',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             * @param ids - ids of Warehouse in the Warehouses list to be deleted
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            delete: function (ids, successCallback, errorCallback) {
                var id = ids[0];
                RequestService.post({
                    url: ApiURLs.warehouses.get + id + '/',
                    method: 'DELETE',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             * @param {Object} file - information about the import file
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            importInventories: function (file, successCallback, errorCallback) {
                file.name = 'file';
                RequestService.uploadFile({
                    method: 'POST',
                    url: ApiURLs.inventory.inventory,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, file.name, file.content);
            }
        }
    });