angular.module('Services')
    .service('CarriersService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {
            /**
             * @param {Object} QuoteParams - options for a list of carriers
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getQuote: function (QuoteParams, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.carrier.quote,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, QuoteParams);
            }
        };
    });