
angular.module('Services')
    .service('AddressBookService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @description Сервис для взаимодействия с "Address and Warehouse management API"
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             * @description Получение списка адресов
             */
            getBook: function (successCallback, errorCallback, pagination, filters) {
                console.log(arguments);
                RequestService.get({
                    url: ApiURLs.address.list,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: angular.element.extend({}, pagination || {}, filters || {})
                });
            },

            /**
             * @param {!AddressModel} AddressBookData - for saving data
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @description Добавление адреса в адресную книгу
             */
            addToBook: function (AddressBookData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.address.list,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, AddressBookData);
            },

            /**
             * @param {number} id - ID of Address in Address Book
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @description Получение подробной информации об адресе
             */
            getAddress: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.address.list + id + '/',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             * @param ids {number[]} - ID's of Addresses in Address Book
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @description Удаление адресов из адресной книги
             */
            deleteAddress: function (ids, successCallback, errorCallback) {
                if (!(ids && ids.length)) return;
                RequestService.post({
                    url: ApiURLs.address.delete,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, ids);
            },

            /**
             * @param {importFileObject} file - information about the import file
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @description Импорт CSV-файла со списком адресов
             */
            importCSV: function (file, successCallback, errorCallback) {
                RequestService.uploadFile({
                    method: 'POST',
                    url: ApiURLs.address.import,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, file.name, file.content);
            },

            /**
             * @param {!String} fileName - file name to save on the computer
             * @param {array} columns - information about the data fields
             * @param {successCallback} successCallback - is performed after a successful request
             * @param {errorCallback} errorCallback - is performed after the request with error
             * @description Экспорт адресной книги в виде CSV-файла
             */
            exportCSV: function (fileName, columns, successCallback, errorCallback) {
                RequestService.exportFile({
                    fileName: fileName,
                    url: ApiURLs.address.export,
                    data: {columns: columns},
                    fileType: 'text/csv',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            }
        };
    });
