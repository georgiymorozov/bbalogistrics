angular.module('Services')
    .service('MarketplacesService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             *
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getList: function (successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.marketplaces.root,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param MarketplaceData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            createMarketplace: function (MarketplaceData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.marketplaces.root,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, MarketplaceData);
            },

            /**
             *
             * @param MarketplaceData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            updateMarketplace: function (MarketplaceData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.marketplaces.marketplace.replace(/\{\{id\}\}/, MarketplaceData.id),
                    method: 'PUT',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, MarketplaceData);
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            verificationMarketplace: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.marketplaces.verification.replace(/\{\{id\}\}/, id),
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getMarketplace: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.marketplaces.marketplace.replace(/\{\{id\}\}/, id),
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            deleteMarketplace: function (id, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.marketplaces.marketplace.replace(/\{\{id\}\}/, id),
                    method: 'DELETE',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {string} verificationCode - code of marketplace for full verification
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            completeVerificationMarketplace: function (id, verificationCode, successCallback, errorCallback) {
                if (!(id && verificationCode)) return;
                RequestService.post({
                    method: 'PUT',
                    url: ApiURLs.marketplaces.verification.replace(/\{\{id\}\}/, id) + '?code=' + verificationCode,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            enableMarketplace: function (id, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.marketplaces.enable.replace(/\{\{id\}\}/, id),
                    method: 'PUT',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param id - id of Marketplace
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            disableMarketplace: function (id, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.marketplaces.disable.replace(/\{\{id\}\}/, id),
                    method: 'PUT',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            }
        }
    });
