angular.module('Services')
    .service('ShipmentsService', function (RequestService, shipmentStatuses) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             *
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             */
            getShipments: function (successCallback, errorCallback, pagination, filters) {
                var url = ApiURLs.shipment.data;
                if (filters && filters.item) {
                    url = ApiURLs.shipment.search;
                }
                RequestService.get({
                    url: url,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: angular.element.extend({}, pagination || {}, filters || {})
                });

            },

            /**
             *
             * @param ShipmentData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            createShipment: function (ShipmentData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.shipment.data,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, ShipmentData);
            },

            /**
             *
             * @param id - id of Shipment
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getShipment: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.shipment.data + id + '/',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },


            /**
             *
             * @param id - id of Shipment
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getTracking: function (id, successCallback, errorCallback) {
                var url = ApiURLs.shipment.tracking.replace(/\{\{id\}\}/i, id);
                RequestService.get({
                    url: url,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param {string} sourceCountry - Code of country (Example: 'AU')
             * @param {string} destinationCountry - Code of country (Example: 'AU')
             * @returns {boolean} - If is domestic, return {true}, else {false}
             */
            isDomestic: function (sourceCountry, destinationCountry) {
                return (sourceCountry == 'AU') && (destinationCountry == 'AU');
            },

            /**
             *
             * @param {object} params - {startDate: "YYYY-MM-DD", endDate: "YYYY-MM-DD"}
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getStatistics: function (params, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.shipment.statistics,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: params
                });
            }
        }
    }).constant('shipmentStatuses', {
        'new': 'New',
        'unprocessed': 'Unprocessed',
        'inProgress': 'In progress',
        'pending': 'Pending',
        'processed': 'Processed'
    });
