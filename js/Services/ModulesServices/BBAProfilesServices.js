angular.module('Services')
    .service('BBAProfilesService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             *
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             */
            getList: function (successCallback, errorCallback, pagination, filters) {

            },

            /**
             *
             * @param ids - list of ids to be deleted
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            delete: function (ids, successCallback, errorCallback) {
                var id = ids[0];
                RequestService.post({
                    url: ApiURLs.packaging + id + '/',
                    method: 'DELETE',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            }
        }
    });