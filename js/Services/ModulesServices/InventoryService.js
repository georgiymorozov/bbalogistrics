angular.module('Services')
    .service('InventoryService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */

        return {

            /**
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             */
            getInventoriesList: function (successCallback, errorCallback, pagination, filters) {
                var requestPagination = angular.element.extend({}, pagination || {}, filters || {});
                RequestService.get({
                    url: ApiURLs.inventory.inventory,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: requestPagination
                });
            },

            /**
             *
             * @param {Object} InventoryData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {string|false} sku - sku of Inventory
             */
            addToInventory: function (InventoryData, successCallback, errorCallback, sku) {
                RequestService.post({
                    url: ApiURLs.inventory.inventory + (sku ? encodeURIComponent(sku) + '/' : ''),
                    method: sku ? 'PUT' : 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, InventoryData);
            },

            /**
             *
             * @param sku - sku of Inventory
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getInventory: function (sku, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.inventory.inventory + encodeURIComponent(sku) + '/',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param {array} skus - list of skus to be deleted
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            deleteInventory: function (skus, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.inventory.delete,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, skus);
            },

            /**
             *
             * @param {Object} file - information about the import file
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            importInventories: function (file, successCallback, errorCallback) {
                file.name = 'file';
                RequestService.uploadFile({
                    method: 'POST',
                    url: ApiURLs.inventory.import,
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, file.name, file.content);
            },

            /**
             *
             * @param {String} fileName - file name to save on the computer
             * @param {Array} columns - information about the data fields
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            exportCSV: function (fileName, columns, successCallback, errorCallback) {
                RequestService.exportFile({
                    fileName: fileName,
                    data: {columns: columns},
                    url: ApiURLs.inventory.export,
                    fileType: 'text/csv',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            }
        }
    });