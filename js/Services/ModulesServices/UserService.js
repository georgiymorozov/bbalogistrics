'use strict';

angular.module('Services')
    .service('UserService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */

        var service = {};

        /**
         *
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.getUser = function (successCallback, errorCallback) {
            RequestService.get({
                url: ApiURLs.user.account,
                successCallback: successCallback,
                errorCallback: errorCallback
            });
        };

        /**
         *
         * @param AccountData - user data for saving
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.saveUser = function (AccountData, successCallback, errorCallback) {
            RequestService.post({
                method: 'PUT',
                url: ApiURLs.user.account,
                successCallback: successCallback,
                errorCallback: errorCallback
            }, AccountData);
        };

        /**
         *
         * @param PasswordData - password data for change
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.setPassword = function (PasswordData, successCallback, errorCallback) {
            RequestService.post({
                method: 'POST',
                url: ApiURLs.user.password,
                successCallback: successCallback,
                errorCallback: errorCallback
            }, PasswordData);
        };

        /**
         *
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.getCompany = function (successCallback, errorCallback) {
            RequestService.get({
                url: ApiURLs.user.company,
                successCallback: successCallback,
                errorCallback: errorCallback
            });
        };

        /**
         *
         * @param CompanyData - company data for saving
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.saveCompany = function (CompanyData, successCallback, errorCallback) {
            RequestService.post({
                method: 'PUT',
                url: ApiURLs.user.company,
                successCallback: successCallback,
                errorCallback: errorCallback,
                roles: [ROLES.admin]
            }, CompanyData);
        };

        /**
         *
         * @param {Object} file - information about the import file
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.savePhoto = function (file, successCallback, errorCallback) {
            RequestService.uploadFile({
                method: 'POST',
                url: ApiURLs.user.photo,
                successCallback: successCallback,
                errorCallback: errorCallback
            }, file.name, file.content);
        };

        /**
         *
         * @param params
         * @param {function} successCallback - is performed after a successful request
         * @param {function} errorCallback - is performed after the request with error
         */
        service.getPhoto = function (params, successCallback, errorCallback) {
            RequestService.getFile({
                url: ApiURLs.user.photo,
                data: params,
                successCallback: successCallback,
                errorCallback: errorCallback
            });
        };

        return service;
    }
);