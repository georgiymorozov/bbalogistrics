'use strict';

angular.module('Services')
    .service('LocationService', function (RequestService) {


        var sendRequest = function(params, url, callback, errorCallback, aborter) {
            RequestService.get({
                url: url,
                successCallback: callback,
                errorCallback: errorCallback,
                data: params || {},
                params: {
                    cache: true,
                    timeout: aborter ? aborter.promise : false
                }
            });
        };

        var groupLocations = function (locationsList, groupBy) {
            var returnData = [];
            var groupsNames = (groupBy == 'name' ? ['codes', 'states'] : ['cities', 'states']);
            locationsList.map(function (item) {
                var searchedName = false;
                item.stateName = item.state ? item.state.name : 'Unavailable';
                item.codeName = item.code || 'Unavailable';
                returnData.map(function (savedItem) {
                    groupsNames.map(function (groupName) {
                        if (savedItem[groupBy] == item[groupBy]) {
                            searchedName = true;
                            item[groupName] = savedItem[groupName];
                            if (groupName != 'states') {
                                savedItem[groupName].push(item);
                            } else {
                                if (
                                    !savedItem['states'].filter(function (stateItem) {
                                        return stateItem.stateName == item.stateName
                                    }).length
                                ) {
                                    if ((groupBy == 'name' && (savedItem.name == item.name)) ||
                                        (groupBy == 'codeName' && (savedItem.codeName == item.codeName))
                                    ) {
                                        savedItem[groupName].push(item);
                                    }
                                }
                            }
                        }
                    });
                });

                if (!searchedName) {
                    groupsNames.map(function (groupName) {
                        item[groupName] = item[groupName] || [item];
                    });
                    returnData.push(item);
                }
            });
            return returnData;
        };

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {
            /**
             *
             * @param params - search params
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             */
            Country: function (params, successCallback, errorCallback, aborter) {
                sendRequest(params, ApiURLs.location.country, successCallback, errorCallback, aborter);
            },

            /**
             *
             * @param params - search params
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             */
            City: function (params, successCallback, errorCallback, aborter) {
                sendRequest(params, ApiURLs.location.location, function(data) {
                    successCallback(groupLocations(data.content, params.name ? 'name' : 'codeName'));
                }, errorCallback, aborter);
            },

            /**
             *
             * @param countryId - id of country
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             * @param params
             */
            State: function (countryId, successCallback, errorCallback, aborter, params) {
                var url = ApiURLs.location.state.replace('{{country}}', countryId);
                sendRequest(params, url, function(data) {
                    successCallback(data.content);
                }, errorCallback, aborter);
            },

            /**
             *
             * @param id - id of location
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             */
            Address: function (id, successCallback, errorCallback, aborter) {
                var url = ApiURLs.location.location + id + '/';
                sendRequest(false, url, function(data) {
                    successCallback(data.content);
                }, errorCallback, aborter);
            },

            /**
             *
             * @param search - search string
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             */
            AddressBook: function (search, successCallback, errorCallback, aborter) {
                var url = ApiURLs.address.search + search.searchValue;
                sendRequest(false, url + '?_t=' + (new Date()).getTime(), function (data) {
                    successCallback(data.content);
                }, errorCallback, aborter);
            },
            /**
             *
             * @param search - search string
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param aborter - angular aborter for $http
             */
            Warehouse: function (search, successCallback, errorCallback, aborter) {
                var url = ApiURLs.warehouses.search + search.searchValue;
                sendRequest(false, url + '?_t=' + (new Date()).getTime(), function (data) {
                    successCallback(data.content);
                }, errorCallback, aborter);
            },
            correctionLocation: function (location) {
                var newLocation = {};
                for (var k in location) {
                    switch (k) {
                        case 'name':
                        case 'code':
                        case 'country':
                        case 'state':
                        case 'id':
                            newLocation[k] = location[k];
                            break;
                    }
                }
                return newLocation;
            }
        }
    });

