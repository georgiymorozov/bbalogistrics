angular.module('Services')
    .service('PackagesService', function (RequestService) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @constructor
         */
        return {

            /**
             *
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {Object} pagination - to display the selected page in the table
             * @param {Object} filters - data filters
             */
            getList: function (successCallback, errorCallback, pagination, filters) {
                RequestService.get({
                    url: ApiURLs.packaging,
                    successCallback: successCallback,
                    errorCallback: errorCallback,
                    data: angular.element.extend({}, pagination || {}, filters || {})
                });
            },

            /**
             *
             * @param {number} id - id of Package
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            getPackage: function (id, successCallback, errorCallback) {
                RequestService.get({
                    url: ApiURLs.packaging + id + '/',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            },

            /**
             *
             * @param {object} PackageData - for saving data
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            savePackage: function (PackageData, successCallback, errorCallback) {
                RequestService.post({
                    url: ApiURLs.packaging,
                    method: 'POST',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                }, PackageData);
            },

            /**
             *
             * @param {object} data
             * @returns {{}}
             */
            generatePackage: function (data) {
                var returnPackage = {};
                for (var i in data) {
                    switch (i) {
                        case 'length':
                        case 'width':
                        case 'weight':
                        case 'height':
                        case 'quantity':
                        case 'measure':
                        case 'packagingId':
                            returnPackage[i] = data[i];
                            break;
                        case 'maxWeight':
                            returnPackage['weight'] = data[i];
                            break;
                        case 'id':
                            returnPackage['packagingId'] = data[i];
                            break;
                        case 'units':
                            returnPackage['measure'] = data[i]['short'];
                            break;
                    }
                }
                return returnPackage;
            },

            /**
             *
             * @param ids - list of ids to be deleted
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             */
            delete: function (ids, successCallback, errorCallback) {
                var id = ids[0];
                RequestService.post({
                    url: ApiURLs.packaging + id + '/',
                    method: 'DELETE',
                    successCallback: successCallback,
                    errorCallback: errorCallback
                });
            }
        }
    });