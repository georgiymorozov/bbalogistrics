'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.reloadOnSearch = true;
    $routeProvider.
        when(
        '/dashboard/my-profile/:tab?/:service?/:id?', {
            templateUrl: function (params) {
                return '/templates/dashboard/my-profile.html';
            },
            controller: 'DashboardProfileController',
            params: {
                parent: 'dashboard',
                htmlTitle: 'Dashboard::My Profile',
                pageTitle: 'My Profile'
            }
        });//.
    //when(
    //'/dashboard/my-profile/password', {
    //    templateUrl: function (params) {
    //        return '/templates/dashboard/my-profile.html';
    //    },
    //    controller: 'DashboardProfileController',
    //    params: {
    //        parent: 'dashboard',
    //        htmlTitle: 'Dashboard:My Profile',
    //        pageTitle: 'My Profile'
    //    }
    //}).
    //when(
    //'/dashboard/my-profile/marketplaces', {
    //    templateUrl: function (params) {
    //        return '/templates/dashboard/my-profile.html';
    //    },
    //    controller: 'DashboardProfileMarketplacesController',
    //    params: {
    //        parent: 'dashboard',
    //        htmlTitle: 'Dashboard::Marketplaces',
    //        pageTitle: 'My Profile',
    //        pageTitleSpan: ' - Marketplaces'
    //    }
    //}).
    //when(
    //'/dashboard/my-profile/marketplaces/magento/:id?', {
    //    templateUrl: function (params) {
    //        return '/templates/dashboard/my-profile.html';
    //    },
    //    controller: 'DashboardProfileMarketplacesMagentoController',
    //    params: {
    //        parent: 'dashboard',
    //        htmlTitle: 'Dashboard::Marketplaces',
    //        pageTitle: 'My Profile',
    //        pageTitleSpan: ' - Marketplaces'
    //    }
    //});
});



