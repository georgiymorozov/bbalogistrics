'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/orders/details/:id', {
            templateUrl: '/templates/orders/details.html',
            controller: 'OrderViewController',
            params: {
                parent: 'orders',
                htmlTitle: 'Orders|Details',
                pageTitle: 'Orders',
                headerTemplate: '/templates/header/order-drop.html'
            }
        });
});