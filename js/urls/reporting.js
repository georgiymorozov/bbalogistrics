'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/reporting/difot', {
            templateUrl: '/templates/reporting/difot.html',
            controller: 'ReportingControllerDifot',
            params: {
                parent: 'reporting',
                htmlTitle: 'Reporting|Difot',
                pageTitle: 'Reporting'
            }
        }).
        when(
        '/reporting/view', {
            templateUrl: '/templates/reporting/view.html',
            controller: 'ReportingControllerView',
            params: {
                parent: 'reporting',
                htmlTitle: 'Reporting|Shipment Details',
                pageTitle: 'Reporting'
            }
        });
});