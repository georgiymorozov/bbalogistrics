'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/reconciliations/summary', {
            templateUrl: '/templates/reconciliations/summary.html',
            controller: 'ReconciliationsSummaryController',
            params: {
                parent: 'reconciliations',
                htmlTitle: 'reconciliations|Summary',
                pageTitle: 'Reconciliations'
            }
        });
});