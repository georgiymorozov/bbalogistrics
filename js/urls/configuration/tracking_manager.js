'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/tracking_manager', {
            templateUrl: '/templates/configuration/tracking_manager/manager.html',
            controller: 'TrackingManagerController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Tracking Manager',
                pageTitle: 'Tracking Manager'
            }
        }).
        when(
        '/configuration/tracking_manager/configuration', {
            templateUrl: '/templates/configuration/tracking_manager/configuration.html',
            controller: 'TrackingManagerConfigurationController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Tracking Manager|Configuration',
                pageTitle: 'Tracking Manager',
                pageTitleSpan: ' - Configuration'
            }
        }).
        when(
        '/configuration/tracking_manager/assign_device', {
            templateUrl: '/templates/configuration/tracking_manager/assign_device.html',
            controller: 'TrackingManagerAssignDeviceController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Tracking Manager|Assign Device',
                pageTitle: 'Tracking Manager',
                pageTitleSpan: ' - Assign Device'
            }
        });
});