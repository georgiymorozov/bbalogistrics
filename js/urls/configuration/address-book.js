'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/address-book/edit/:id?', {
            templateUrl: '/templates/configuration/address-book/edit.html',
            controller: 'ConfigurationAddressBookEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Address Book|Edit',
                pageTitle: 'Address Book',
                headerTemplate: '/templates/header/profile-manager-create.html'
            }
        }).
        when(
        '/configuration/address-book/import', {
            templateUrl: '/templates/configuration/address-book/import.html',
            controller: 'ConfigurationAddressBookImportController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Address Book|Import',
                pageTitle: 'Address Book'
            }
        });
});