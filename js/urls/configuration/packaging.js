'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/packaging/edit/:id?', {
            templateUrl: '/templates/configuration/packaging/edit.html',
            controller: 'ConfigurationPackagingEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Packaging|Edit',
                pageTitle: 'Packaging and Cartonisation Logic'
            }
        });
});