'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/inventory/edit', {
            templateUrl: '/templates/configuration/inventory/edit.html',
            controller: 'ConfigurationInventoryEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Inventory|Add new',
                pageTitleSpan: ' - Add Inventory',
                pageTitle: 'Inventory'
            }
        }).
        when(
        '/configuration/inventory/edit/:sku', {
            templateUrl: '/templates/configuration/inventory/edit.html',
            controller: 'ConfigurationInventoryEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Inventory|Edit',
                pageTitleSpan: ' - Edit Inventory',
                pageTitle: 'Inventory'
            }
        }).
        when(
        '/configuration/inventory/import', {
            templateUrl: '/templates/configuration/inventory/import.html',
            controller: 'ConfigurationInventoryImportController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Inventory|Import',
                pageTitle: 'Inventory Management'
            }
        });
});