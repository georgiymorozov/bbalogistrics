'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/warehouses/edit/:id?', {
            templateUrl: '/templates/configuration/warehouses/edit.html',
            controller: 'ConfigurationWarehousesEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Warehouses|Edit',
                pageTitle: 'Manage Warehouses',
                headerTemplate: '/templates/header/profile-manager-create.html'
            }
        });
});