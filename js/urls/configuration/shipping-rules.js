'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/shipping-rules/edit', {
            templateUrl: '/templates/configuration/shipping-rules/edit.html',
            controller: 'ConfigurationShippingRulesEditController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Shipping Rules|Edit',
                pageTitle: 'Shipping Rules',
                headerTemplate: '/templates/header/profile-manager-create.html'
            }
        });
});