'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/configuration/profile-manager', {
            templateUrl: '/templates/configuration/profile-manager.html',
            controller: 'ConfigurationProfileManagerController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Profile Manager',
                pageTitle: 'BBA Profile Manager'
            }
        }).
        when(
        '/configuration/profile-manager/edit', {
            templateUrl: '/templates/configuration/profile-manager-create.html',
            controller: 'ConfigurationProfileManagerCreatorController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Profile Manager Creator',
                pageTitle: 'BBA Profile Creator',
                headerTemplate: '/templates/header/profile-manager-create.html'
            }
        }).
        when(
        '/configuration/warehouses', {
            templateUrl: '/templates/configuration/warehouses.html',
            controller: 'ConfigurationWarehousesController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Warehouses',
                pageTitle: 'Manage Warehouses'
            }
        }).
        when(
        '/configuration/packaging', {
            templateUrl: '/templates/configuration/packaging.html',
            controller: 'ConfigurationPackagingController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Packaging',
                pageTitle: 'Packaging and Cartonisation Logic'
            }
        }).
        when(
        '/configuration/inventory', {
            templateUrl: '/templates/configuration/inventory-management.html',
            controller: 'ConfigurationInventoryController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Inventory Management',
                pageTitle: 'Inventory Management',
                pageTitleSpan: ' - Duties and Tax’s configuration'
            }
        }).
        when(
        '/configuration/shipping-rules', {
            templateUrl: '/templates/configuration/shipping-rules.html',
            controller: 'ConfigurationShippingController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Shipping Rules',
                pageTitle: 'Shipping Rules'
            }
        }).
        when(
        '/configuration/address-book', {
            templateUrl: '/templates/configuration/address-book.html',
            controller: 'ConfigurationAddressBookController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Address Book',
                pageTitle: 'Address Book'
            }
        }).
        when(
        '/configuration/currency-converter', {
            templateUrl: '/templates/configuration/currency-converter.html',
            controller: 'ConfigurationCurrencyController',
            params: {
                parent: 'configuration',
                htmlTitle: 'Configuration|Currency Converter',
                pageTitle: 'Currency Converter'
            }
        });
});