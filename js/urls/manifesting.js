'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/manifesting/confirm', {
            templateUrl: '/templates/manifesting/confirm.html',
            controller: 'ManifestingConfirmController',
            params: {
                parent: 'manifesting',
                htmlTitle: 'Manifesting|Confirm',
                pageTitle: 'Manifesting'
            }
        });
});