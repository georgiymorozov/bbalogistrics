'use strict';

var module = angular.module('app');

module.config(function($routeProvider, $locationProvider) {
    $routeProvider.
        when(
        '/', {
            templateUrl: '/templates/dashboard.html',
            controller: 'DashboardController',
            params: {
                name: 'dashboard',
                itemText: 'Dashboard',
                htmlTitle: 'Dashboard',
                pageTitle: 'Dashboard',
                icon: 'dashboard',
                mainMenu: true
            }
        }).
        when(
        '/configuration', {
            templateUrl: '/templates/configuration.html',
            controller: 'ConfigurationController',
            params: {
                name: 'configuration',
                itemText: 'Configuration',
                htmlTitle: 'Configuration',
                pageTitle: 'Configuration',
                icon: 'configuration',
                mainMenu: true
            }
        }).
        when(
        '/quote-and-book', {
            templateUrl: '/templates/quote-and-book.html',
            controller: 'QuoteBookController',
            params: {
                itemText: 'Quote & Book',
                htmlTitle: 'Quote & Book',
                pageTitle: 'Quote & Book',
                icon: 'quotebook',
                mainMenu: true
            }
        }).
        when(
        '/pending-shipments/:tab?', {
            templateUrl: '/templates/pending-shipments.html',
            controller: 'PendingShipmentsController',
            params: {
                name: 'pendingshipments',
                itemText: 'Pending Shipments',
                htmlTitle: 'Pending Shipments',
                pageTitle: 'Pending Shipments',
                icon: 'pendingshipments',
                mainMenu: true,
                headerTemplate: '/templates/header/pending-shipments.html'
            }
        }).
        when(
        '/orders', {
            templateUrl: '/templates/orders.html',
            controller: 'OrdersController',
            params: {
                itemText: 'Orders',
                htmlTitle: 'Orders',
                pageTitle: 'Orders',
                icon: 'orders',
                mainMenu: true,
                name: 'orders',
                headerTemplate: '/templates/header/order-drop.html'
            }
        }).
        when(
        '/manifesting', {
            templateUrl: '/templates/manifesting.html',
            controller: 'ManifestingController',
            params: {
                itemText: 'Manifesting',
                htmlTitle: 'Manifesting Manager',
                pageTitle: 'Manifesting Manager',
                icon: 'manifesting',
                mainMenu: true,
                name: 'manifesting'
            }
        }).
        when(
        '/reporting', {
            templateUrl: '/templates/reporting.html',
            controller: 'ReportingController',
            params: {
                name: 'reporting',
                itemText: 'Reporting',
                htmlTitle: 'Reporting',
                pageTitle: 'Reporting',
                icon: 'configuration',
                mainMenu: true
            }
        }).
        when(
        '/tracking/:id?', {
            templateUrl: '/templates/tracking.html',
            controller: 'TrackingController',
            params: {
                itemText: 'Tracking',
                htmlTitle: 'Tracking',
                pageTitle: 'Tracking',
                icon: 'tracking',
                mainMenu: true
            }
        }).
        when(
        '/returns', {
            templateUrl: '/templates/returns.html',
            controller: 'ReturnsController',
            params: {
                itemText: 'Returns',
                htmlTitle: 'Returns',
                pageTitle: 'Returns',
                icon: 'returns',
                mainMenu: true
            }
        }).
        when(
        '/reconciliations', {
            templateUrl: '/templates/reconciliations.html',
            controller: 'ReconciliationsController',
            params: {
                itemText: 'Reconciliations',
                htmlTitle: 'Reconciliations',
                pageTitle: 'Reconciliations',
                icon: 'rec',
                mainMenu: true,
                name: 'reconciliations'
            }
        }
    ).
        when(
        '/core/:tab?', {
            templateUrl: '/templates/core.html',
            controller: 'CoreController',
            params: {
                itemText: 'In Store',
                htmlTitle: 'In Store',
                pageTitle: 'In Store',
                icon: 'final',
                mainMenu: true
            }
        }
    ).
        when(
        '/logout', {
            template: false,
            controller: 'ExitController',
            params: {}
        }
    ).
        otherwise({
            redirectTo: '/'
        });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    })

});