'use strict';

var module = angular.module('app');

module.config(function($routeProvider) {
    $routeProvider.
        when(
        '/pending-shipments/details', {
            templateUrl: '/templates/pending-shipments/details.html',
            controller: 'PendingShipmentsDetailsController',
            params: {
                itemText: 'Pending Shipments',
                htmlTitle: 'Pending Shipments',
                pageTitle: 'Pending Shipments',
                parent: 'pendingshipments',
                headerTemplate: '/templates/header/pending-shipments.html'
            }
        }).
        when(
        '/pending-shipments/booking', {
            templateUrl: '/templates/pending-shipments/booking.html',
            controller: 'PendingShipmentsBookingController',
            params: {
                itemText: 'Pending Shipments',
                htmlTitle: 'Pending Shipments',
                pageTitle: 'Pending Shipments',
                parent: 'pendingshipments',
                headerTemplate: '/templates/header/pending-shipments.html'
            }
        });
});