var ApiURL = 'http://api.bbalogistics.com.au/';
var LocationApiURL = 'http://api.bbalogistics.com.au/';

var loginPageURL = '/auth';


var userUrl = ApiURL + 'auth/user/';
var addressUrl = LocationApiURL + 'address/';

var ApiURLs = {
    token: ApiURL + 'oauth/token',
    user: {
        account: userUrl,
        password: userUrl + 'password/',
        company: userUrl + 'company/',
        photo: userUrl + 'photo/'
    },
    location: {
        country: addressUrl + 'country/',   //?name=r, ?code=RU
        location: addressUrl + 'location/', //?name=kras&country=174
        state: addressUrl + 'country/{{country}}/state/'
    },
    address: {
        search: addressUrl + 'address/search/', // + {search}
        list: addressUrl + 'address/', // /id?
        delete: addressUrl + 'address/delete/',
        import: addressUrl + 'address/import/',
        export: addressUrl + 'address/export/'
    },
    warehouses: {
        search: addressUrl + 'warehouse/search/', // + {search}
        get: addressUrl + 'warehouse/'
    },
    shipment: {
        data: LocationApiURL + 'shipment/shipment/', // /id?
        search: LocationApiURL + 'shipment/shipment/search/', // /id?
        tracking: LocationApiURL + 'shipment/shipment/{{id}}/tracking/',
        statistics: LocationApiURL + 'shipment/shipment/statistics/'
    },
    inventory: {
        inventory: LocationApiURL + 'inventory/', // sku
        import: LocationApiURL + 'inventory/import/',
        delete: LocationApiURL + 'inventory/delete/',
        export: LocationApiURL + 'inventory/export/'
    },
    carrier: {
        quote: LocationApiURL + 'carrier/quote/'
    },
    packaging: LocationApiURL + 'packaging/', // /id?
    marketplaces: {
        root: LocationApiURL + 'marketplace/',
        verification: LocationApiURL + 'marketplace/{{id}}/verification/',
        marketplace: LocationApiURL + 'marketplace/{{id}}',
        enable: LocationApiURL + 'marketplace/{{id}}/enable',
        disable: LocationApiURL + 'marketplace/{{id}}/disable'
    }
};






