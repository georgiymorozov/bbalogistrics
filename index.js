require('./bower_components/angular/angular.min');
require('./bower_components/angular-base64/angular-base64');
require('./bower_components/angular-route/angular-route.min');
require('./bower_components/angular-resource/angular-resource.min');
require('./bower_components/angular-cookies/angular-cookies.min');

require('./js/app');


var requireContext = require('webpack-require_context');

var replaceFrom = /\.\/(\w+)\.js/;
var replaceWith = '$1';

var filesContext = require.context('./js/directives');
requireContext(filesContext, replaceFrom, replaceWith);

filesContext = require.context('./js/Services');
requireContext(filesContext, replaceFrom, replaceWith);

filesContext = require.context('./js/controllers');
requireContext(filesContext, replaceFrom, replaceWith);

filesContext = require.context('./js/urls');
requireContext(filesContext, replaceFrom, replaceWith);

filesContext = require.context('./js/consts');
requireContext(filesContext, replaceFrom, replaceWith);