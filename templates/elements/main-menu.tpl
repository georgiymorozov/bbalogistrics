<ul class="main-menu-list">
    <li ng-repeat="itemMenu in mainMenu" class="main-menu-item" ng-class="{active:currentRoute==itemMenu}" data-title="{{itemMenu.params.itemText}}">
        <a class="menu-pseudo-link" ng-href="{{toLocationPage(itemMenu.originalPath)}}">
            <img ng-src="/images/icons/menu/{{itemMenu.params.icon}}_off.svg" class="item-icon off-item-icon"/>
            <img ng-src="/images/icons/menu/{{itemMenu.params.icon}}_on.svg" class="item-icon on-item-icon"/>
            <span class="menu-item-text">{{itemMenu.params.itemText}}</span>
        </a>
    </li>
</ul>