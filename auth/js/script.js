'use strict';

angular.module('Login', ['ngCookies', 'ngRoute', 'base64'])
    .controller('LoginController',
    function ($scope, $rootScope, $location, $cookies, AuthenticationService) {
            var authCookie = $cookies.get('auth') || false;
            var getString = {};
            var search = window.location.search ? window.location.search.split('?')[1].split('&') : false;
            search ? search.map(function(item) {
                var keyValue = item.split('=');
                getString[keyValue[0]] = keyValue[1];
            }) : false;
            if (authCookie) {
                window.location = getString.go ? decodeURIComponent(getString.go) : '/';
                return;
            }

        AuthenticationService.clearCredentials();
            $scope.dataLoading = false;


        $scope.login = function(form) {
                if ($scope.dataLoading) return;
                if (!form.$valid) {
                    $scope.authError = true;
                    return;
                }
                $scope.authError = false;
                $scope.dataLoading = true;
            AuthenticationService.login($scope.username, $scope.password, function (response) {
                window.location = getString.go ? decodeURIComponent(getString.go) : '/';
            }, function () {
                $scope.authError = true;
                $scope.dataLoading = false;
            });
                return false;
            };
        }
    );

(function($) {
    $(function() {
        var form = $('#form');
        form.on('submit', function(e) {
            e.preventDefault();
        });
    });
})(jQuery);