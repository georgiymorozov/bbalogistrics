'use strict';

angular.module('Login')
    .factory('AuthenticationService', function ($base64, $http, $cookies, $httpParamSerializerJQLike) {

        /**
         * @author Dmitry Kovalev
         * @see RequestAPIService
         * @description Сервис для взаимодействия с "Address and Warehouse management API"
         * @constructor
         */
        return {

            /**
             *
             * @param {string} username - user name
             * @param {string} password - user password
             * @param {function} successCallback - is performed after a successful request
             * @param {function} errorCallback - is performed after the request with error
             * @param {string} requestMethod - communication method
             */
            login: function (username, password, successCallback, errorCallback, requestMethod) {
                var data = {
                    'client_id': authParams['client_id']
                };
                switch (requestMethod || 'auth') {
                    case 'auth':
                        data['username'] = username;
                        data['password'] = password;
                        data['grant_type'] = 'password';
                        data['client_secret'] = authParams['client_secret'];
                        break;
                    case 'refresh':
                        var refresh_token = $cookies.get('auth');
                        refresh_token = refresh_token || false;
                        data['grant_type'] = 'refresh_token';
                        data['refresh_token'] = refresh_token;
                        break;
                }
                $http({
                    url: authURL,
                    traditional: true,
                    method: 'POST',
                    data: data,
                    headers: {
                        'Authorization': 'Basic ' + $base64.encode(authParams['client_id'] + ":" + authParams['client_secret']),
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
                    },
                    transformRequest: $httpParamSerializerJQLike
                }).then(function (response) {
                    $cookies.put('auth', response['data']['refresh_token'], {
                        expires: new Date((new Date()).getTime() + response['data']['expires_in'] * 1000),
                        path: '/'
                    });
                    $http.defaults.headers.common.Authorization = 'Bearer ' + response['data']['access_token'];
                    successCallback(response);
                }, function(response) {
                    errorCallback(response);
                });
            },

            /**
             *
             */
            clearCredentials: function () {
                $cookies.remove('auth', {
                    path: '/'
                });
                $http.defaults.headers.common.Authorization = '';
            }
        };
    });

